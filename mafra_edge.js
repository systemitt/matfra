/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'lapiz',
                            type: 'image',
                            rect: ['62px', '-5px', '75px', '70px', 'auto', 'auto'],
                            overflow: 'visible',
                            fill: ["rgba(0,0,0,0)",im+"lapiz.gif",'0px','0px']
                        },
                        {
                            id: 'Imagen12',
                            type: 'image',
                            rect: ['47px', '-30px', '105px', '119px', 'auto', 'auto'],
                            overflow: 'visible',
                            fill: ["rgba(0,0,0,0)",im+"Imagen12.png",'0px','0px'],
                            transform: [[],['90']]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '0px', '0px'],
                            overflow: 'visible',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid29",
                            "left",
                            6000,
                            0,
                            "linear",
                            "${Imagen12}",
                            '47px',
                            '47px'
                        ],
                        [
                            "eid2",
                            "rotateZ",
                            0,
                            2000,
                            "linear",
                            "${Imagen12}",
                            '90deg',
                            '-90deg'
                        ],
                        [
                            "eid3",
                            "rotateZ",
                            2000,
                            2000,
                            "linear",
                            "${Imagen12}",
                            '-90deg',
                            '90deg'
                        ],
                        [
                            "eid4",
                            "rotateZ",
                            4000,
                            2000,
                            "linear",
                            "${Imagen12}",
                            '90deg',
                            '0deg'
                        ],
                        [
                            "eid30",
                            "top",
                            6000,
                            0,
                            "linear",
                            "${Imagen12}",
                            '-30px',
                            '-30px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("mafra_edgeActions.js");
})("EDGE-1414379");
