(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



// stage content:
(lib.cambiaformas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FAFFFF","#0000FF","#000077"],[0.247,0.588,1],-0.9,-28,0,-0.8,0,31.2).s().p("AjUDUQhXhXAAh9QAAh8BXhXQBZhYB7AAQB8AABYBYQBYBXAAB8QAAB9hYBXQhYBYh8AAQh7AAhZhYg");
	this.shape.setTransform(84.1,201.8,1.668,1.668);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#FAFFFF","#0B0BF4","#0A0A72","#060472"],[0.235,0.576,0.98,1],0.9,-42.5,0,-0.6,-0.3,46.9).s().p("AihHkQhtgohZhdQiPiXgEjQQAKjPCViTQBZhXBugmQBLgUBTADQDRAHCVCSQAcAbAWAeQBbCEgDCqQgDCnhbCBQgZAfggAdQiXCNjSAAQhSAAhJgVg");
	this.shape_1.setTransform(91.4,196.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FAFFFF","#1515EA","#14146D","#0D086D"],[0.227,0.565,0.957,1],2.7,-38.1,0,-0.3,-0.5,41.9).s().p("AiqHqQhrgthXhgQiNibgHjTQATjMCZiUQBbhYBvgoQBLgRBTAGQDSANCYCUQAcAbAYAdQBUCIgGCsQgFCphbCEQgbAeggAcQidCGjUAAQhTAAhLgUg");
	this.shape_2.setTransform(98.6,192);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FBFFFF","#2020DF","#1F1E68","#130D68"],[0.216,0.553,0.937,1],4.4,-33.6,0,-0.1,-0.6,37).s().p("AizHxQhpgyhVhjQiKiggLjVQAcjLCciUQBdhYBwgrQBMgNBUAIQDSAVCbCUQAdAbAZAeQBNCLgJCuQgICqhaCIQgcAcghAbQiiCAjXAAQhVAAhMgTg");
	this.shape_3.setTransform(105.9,187);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#FBFFFF","#2B2BD4","#292863","#1A1163"],[0.204,0.541,0.918,1],6.2,-29.2,0,0.1,-0.8,32.4).s().p("Ai9H4Qhmg4hUhmQiHikgOjXQAljJCfiVQBfhZBxgtQBMgJBUALQDTAbCfCVQAdAbAaAeQBGCOgMCwQgLCshYCMQgeAbgjAZQinB6jaAAQhVAAhOgSg");
	this.shape_4.setTransform(113.2,182);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#FBFFFF","#3535CA","#33325E","#20155E"],[0.196,0.529,0.898,1],7.8,-24.7,0,0.3,-0.8,27.8).s().p("AjHH/Qhjg9hShoQiFiqgSjZQAvjHCiiWQBhhYBygwQBNgGBUAOQDTAiCiCVQAfAcAaAeQA/CSgOCxQgPCuhXCPQgfAZgkAYQisB1jdAAQhXAAhPgSg");
	this.shape_5.setTransform(120.5,176.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FBFFFF","#4040BF","#3D3C59","#261A59"],[0.184,0.518,0.875,1],9.6,-20.2,0,0.5,-0.9,23.7).s().p("AjQIHQhhhChRhrQiCiugVjcQA4jECliXQBjhZBzgyQBOgCBUAQQDUApClCWQAfAcAbAdQA4CWgRCzQgRCvhWCTQggAYglAXQiyBujgAAQhYAAhQgRg");
	this.shape_6.setTransform(127.8,171.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FBFFFF","#4A4AB5","#474654","#2D1E54"],[0.176,0.506,0.855,1],11.3,-15.6,0,0.7,-0.8,20.2).s().p("AjaIQQhehIhPhuQiAizgZjdQBCjDCoiXQBlhaB0g1QBOACBVAUQDUAvCpCXQAfAcAcAdQAxCZgUC1QgTCyhVCVQgiAXgmAVQi3BpjjgBQhZAAhSgPg");
	this.shape_7.setTransform(135.1,166.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FCFFFF","#5555AA","#52504F","#33224F"],[0.165,0.494,0.835,1],12.9,-10.9,0,0.9,-0.8,17.6).s().p("AjlIYQhchMhNhxQh9i4gcjgQBKjACsiYQBnhaB1g3QBPAFBVAWQDVA2CsCXQAgAdAdAeQAqCcgXC3QgXCyhUCaQgjAVgnAUQi9BijlAAQhaABhUgQg");
	this.shape_8.setTransform(142.5,161.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#FCFFFF","#60609F","#5C5A4A","#39264A"],[0.153,0.482,0.812,1],14.6,-6.3,0,1,-0.6,16.4).s().p("AjvIiQhZhShMh0Qh7i8gfjiQBUi/CviZQBohaB2g6QBPAJBWAZQDWA9CvCYQAhAcAdAeQAkCggaC4QgZC1hUCdQgkAUgoATQjCBbjoAAQhbAAhWgNg");
	this.shape_9.setTransform(149.9,156.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FCFFFF","#6A6A95","#666445","#402B45"],[0.145,0.471,0.792,1],16.2,-1.6,0,1.1,-0.6,16.8).s().p("Aj6IqQhXhWhKh3Qh4jBgjjkQBdi9CziaQBqhbB3g8QBPANBWAbQDXBECyCZQAiAdAeAdQAcCkgcC5QgcC3hSCgQgmATgqASQjHBVjqAAQhdAAhXgNg");
	this.shape_10.setTransform(157.3,151);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FCFFFF","#75758A","#706E40","#462F40"],[0.133,0.459,0.773,1],17.8,3,0,1.2,-0.5,18.9).s().p("AkFIzQhUhchJh6Qh2jGgmjmQBni7C1iaQBshbB4g/QBQARBXAeQDXBKC1CZQAiAdAgAfQAVCmgfC7QgfC5hRCkQgoARgqARQjNBPjtAAQhdAAhZgMg");
	this.shape_11.setTransform(164.8,145.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FDFFFF","#808080","#7B783C","#4D333C"],[0.125,0.447,0.753,1],19.2,7.6,0,1.1,-0.5,22.1).s().p("AkRI8QhShhhHh9QhzjLgqjoQBwi5C5ibQBuhcB5hBQBQAVBXAgQDYBRC5CbQAiAdAhAeQAOCqgiC9QgiC7hQCmQgpARgrAOQjSBKjwAAQhfAAhagLg");
	this.shape_12.setTransform(172.3,140.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FDFFFF","#8A8A75","#858237","#533737"],[0.114,0.435,0.729,1],20.6,12.1,0,1.1,-0.4,26).s().p("AkeJFQhPhnhGh/QhwjQgtjqQB5i3C8icQBwhcB5hEQBSAZBXAjQDYBYC8CbQAkAdAhAeQAHCuglC/QgkC8hPCqQgqAPgtANQjXBEjzAAQhgAAhcgKg");
	this.shape_13.setTransform(179.9,135.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#FDFFFF","#95956A","#8F8C32","#593B32"],[0.102,0.424,0.71,1],22,16.7,0,1,-0.4,30.4).s().p("AkrJOQhNhthEiCQhtjUgxjtQCCi1DAicQByhcB6hGQBSAbBXAmQDZBfC/CcQAkAdAiAeQABCxgoDBQgnC+hOCtQgsANguANQjcA+j2AAQhhgBhdgIg");
	this.shape_14.setTransform(187.6,130.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#FDFFFF","#9F9F60","#99962D","#60402D"],[0.094,0.412,0.69,1],23.3,21.3,0,0.7,-0.3,35).s().p("Ak5JWQhKhwhDiGQhrjZg0juQCMi0DCidQB0hdB7hJQBTAgBYAoQDZBnDDCcIBHA7QgGC1grDCQgqDAhNCxQgtAMgvALQjiA3j4ABQhiAAhfgJg");
	this.shape_15.setTransform(195.3,125);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#FDFFFF","#AAAA55","#A3A028","#664428"],[0.082,0.4,0.667,1],24.6,26,0,0.5,-0.2,39.8).s().p("AlHJfQhHh2hCiIQhojeg4jwQCViyDHieQB1hdB8hLQBTAjBYArQDaBtDGCdQAlAeAkAeQgNC4gtDEQguDBhMC1IheAUQjnAxj7AAQhkAAhggHg");
	this.shape_16.setTransform(203.1,119.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#FEFFFF","#B5B54A","#AEAA23","#6C4823"],[0.071,0.388,0.647,1],25.8,30.6,0,0.3,-0.1,44.7).s().p("AlVJoQhFh7hAiMQhmjig7jzQCeivDKifQB3hdB9hOQBVAnBXAuQDcBzDICeQAmAeAlAeQgUC7gwDGQgwDDhMC4IhhASQjsArj9AAQhlAAhigGg");
	this.shape_17.setTransform(210.8,114.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.rf(["#FEFFFF","#BFBF40","#B8B41E","#734D1E"],[0.063,0.376,0.627,1],27.2,35.1,0,0.1,-0.1,49.7).s().p("AljJwQhDiAg9iOQhkjng/j1QCoiuDNifQB5heB+hQQBVArBYAwQDcB6DMCfQAnAeAlAeQgbC/gzDIQgzDEhKC7IhkAQQjxAlkBAAQhlAAhkgGg");
	this.shape_18.setTransform(218.5,109.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#FEFFFF","#CACA35","#C2BE19","#795119"],[0.051,0.365,0.604,1],28.5,39.8,0,-0.1,0,54.8).s().p("AlxJ5QhAiFg8iRQhhjshDj3QCyisDQifQB7hfB/hTQBVAvBYAzQDdCBDPCfIBOA9QgiDBg3DKQg1DHhJC+IhmANQj3AfkDAAQhnAAhlgFg");
	this.shape_19.setTransform(226.3,104.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#FEFFFF","#D4D42B","#CCC814","#7F5514"],[0.043,0.353,0.584,1],29.7,44.4,0,-0.3,0,60).s().p("Al+KCQg/iLg6iTQhfjxhGj5QC8iqDSigQB+hfCAhWQBVAyBZA3QDdCHDSCgIBQA9QgpDFg5DMQg4DIhIDCIhpAKQj9AZkFAAQhpAAhlgEg");
	this.shape_20.setTransform(234,99);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.rf(["#FEFFFF","#DFDF20","#D6D20F","#86590F"],[0.031,0.341,0.565,1],31.1,49,0,-0.5,0.1,65.1).s().p("AmNKLQg7iQg5iXQhcj1hJj7QDEioDWihQB/hfCBhZQBWA2BZA5QDeCPDWCgIBQA9QgwDIg8DOQg7DKhGDGIhsAHQkBATkJAAQhpAAhogDg");
	this.shape_21.setTransform(241.8,93.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.rf(["#FFFFFF","#EAEA15","#E1DC0A","#8C5E0A"],[0.02,0.329,0.545,1],32.3,53.5,0,-0.8,0.1,70.3).s().p("AmbKUQg5iVg3iaQhZj6hNj9QDNinDZihQCChgCChbICwB2QDeCVDZChIBSA+Qg2DLhADPQg9DMhGDJIhuAFQkGANkMAAQhrAAhpgCg");
	this.shape_22.setTransform(249.5,88.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.rf(["#FFFFFF","#F4F40B","#EBE605","#936205"],[0.012,0.318,0.522,1],33.6,58.1,0,-1,0.2,75.5).s().p("AmpKcQg2iag2icQhWj/hRkAQDWikDdiiQCDhgCDhdICxB7QDfCcDcCiIBUA+Qg+DPhBDRQhBDOhEDMIhxACQkNAGkOAAIjWgBg");
	this.shape_23.setTransform(257.3,83.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.rf(["#FFFFFF","#FFFF00","#F5F000","#996600"],[0,0.306,0.502,1],12.6,23.7,0,-1,0.1,30.4).s().p("AilD/Ihmk6IELjDIEMDDIhmE6g");
	this.shape_24.setTransform(265,78.2,2.653,2.653);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.rf(["#FFFFFF","#FFF400","#F5E600","#9D6200"],[0.008,0.318,0.506,0.98],36.7,57.6,0,-0.8,0.2,76.4).s().p("AmuKPIkZtEILSnwIHgFLIDdCxIkYNDIkfAMg");
	this.shape_25.setTransform(272.1,83.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.rf(["#FFFFFF","#FFEA00","#F6DC00","#A15E00"],[0.012,0.329,0.51,0.965],38.5,52.5,0,-0.5,0.2,72.3).s().p("AmmJ4IkhtCILbnbIHnE7IDNC2IkhNDIkZAXg");
	this.shape_26.setTransform(279.2,88.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.rf(["#FFFFFF","#FFDF00","#F6D200","#A65900"],[0.02,0.341,0.51,0.949],40.3,47.3,0,-0.1,0.2,68.5).s().p("AmdJiIkqtCILmnFIHtEtIC8C6IkpNCIkUAig");
	this.shape_27.setTransform(286.3,93.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.rf(["#FFFFFF","#FFD400","#F7C800","#AA5500"],[0.027,0.353,0.514,0.929],42.2,42.1,0,0.2,0.2,65).s().p("AmVJLIkytCILvmvIH0EfICsC+IkyNCIkOAug");
	this.shape_28.setTransform(293.5,98.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.rf(["#FFFFFF","#FFCA00","#F7BE00","#AE5100"],[0.031,0.361,0.518,0.914],44,36.8,0,0.5,0.2,62).s().p("AmNI1Ik6tBIL5maIH7ERICbDCIk6NBIkJA5g");
	this.shape_29.setTransform(300.6,103.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.rf(["#FFFFFF","#FFBF00","#F8B400","#B34D00"],[0.039,0.373,0.522,0.894],45.8,31.4,0,0.8,0.1,59.4).s().p("AmEIeIlDtAIMDmEIIBECICLDHIlCNAIkDBFg");
	this.shape_30.setTransform(307.7,108.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#FFFFFF","#FFB500","#F8AA00","#B74800"],[0.047,0.384,0.525,0.875],47.7,26,0,1.2,0.2,57.5).s().p("Al8IIIlLtAIMNlvIIID0IB6DLIlLNAIj9BQg");
	this.shape_31.setTransform(314.8,113.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#FFFFFF","#FFAA00","#F8A000","#BB4400"],[0.051,0.396,0.525,0.859],49.5,20.5,0,1.5,0.1,56.1).s().p("Al0HxIlTs/IMXlZIIODlIBqDPIlUNAIj3Bbg");
	this.shape_32.setTransform(322,118.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFFFFF","#FF9F00","#F99600","#BF4000"],[0.059,0.408,0.529,0.843],51.4,14.9,0,1.9,0.1,55.4).s().p("AlrHbIlcs/IMhlEIIVDYIBZDTIlcM/IjyBng");
	this.shape_33.setTransform(329.1,123.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#FFFFFF","#FF9500","#F98C00","#C43B00"],[0.067,0.42,0.533,0.824],53.2,9.3,0,2.2,0.1,55.3).s().p("AliHEIlls+IMrkuIIbDJIBJDYIlkM+IjtByg");
	this.shape_34.setTransform(336.2,128.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FFFFFF","#FF8A00","#FA8200","#C83700"],[0.071,0.431,0.537,0.808],55.1,3.7,0,2.6,0.1,56).s().p("AlaGuIlts+IM1kYIIiC6IA4DcIltM+IjmB9g");
	this.shape_35.setTransform(343.3,133.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.rf(["#FFFFFF","#FF8000","#FA7800","#CC3300"],[0.078,0.443,0.541,0.788],57.2,-2,0,2.9,0,57.3).s().p("AlRGXIl2s9IM/kDIIoCtIAoDfIl2M+IjhCJg");
	this.shape_36.setTransform(350.5,138.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FFFFFF","#FF7500","#FA6E00","#D02F00"],[0.086,0.451,0.541,0.769],59.1,-7.7,0,3.3,0,59.2).s().p("AlJGAIl+s8INJjtIIvCeIAXDkIl9M8IjcCVg");
	this.shape_37.setTransform(357.6,143.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#FFFFFF","#FF6A00","#FB6400","#D42B00"],[0.09,0.463,0.545,0.753],61.1,-13.4,0,3.6,0,61.7).s().p("AlBFqImGs8INTjYII2CQIAGDoImGM8IjWCgg");
	this.shape_38.setTransform(364.7,148.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#FFFFFF","#FF6000","#FB5A00","#D92600"],[0.098,0.475,0.549,0.737],62.5,-19.3,0,3.5,0,64.6).s().p("Ak9FUImPs8INcjCII9CBIgKDtImPM7IjQCsg");
	this.shape_39.setTransform(372.3,153.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.rf(["#FFFFFF","#FF5500","#FC5000","#DD2200"],[0.106,0.486,0.553,0.718],63.6,-25.2,0,3,0,68.1).s().p("Ak9E9ImXs7INmisIJDBzIgbDxImWM6IjLC3g");
	this.shape_40.setTransform(380.3,158.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.rf(["#FFFFFF","#FF4A00","#FC4600","#E11E00"],[0.11,0.498,0.553,0.702],64.7,-31.1,0,2.5,0,71.8).s().p("Ak9EmImgs6INxiWIJKBkIgrD1ImgM6IjFDCg");
	this.shape_41.setTransform(388.2,163.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.rf(["#FFFFFF","#FF4000","#FD3C00","#E61A00"],[0.118,0.51,0.557,0.682],65.9,-37.1,0,2,0,75.9).s().p("Ak9EQImos6IN7iBIJQBXIg8D5ImnM5IjADOg");
	this.shape_42.setTransform(396.2,168.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.rf(["#FFFFFF","#FF3500","#FD3200","#EA1500"],[0.125,0.522,0.561,0.663],67,-43.2,0,1.5,-0.1,80.3).s().p("Ak8D5Imxs4IOEhsIJXBIIhMD+ImwM5Ii6DYg");
	this.shape_43.setTransform(404.1,173.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.rf(["#FFFFFF","#FF2B00","#FD2800","#EE1100"],[0.129,0.529,0.565,0.647],68.2,-49.2,0,1,-0.1,84.9).s().p("Ak8DjIm6s4IOPhXIJeA6IhdECIm5M5Ii0Djg");
	this.shape_44.setTransform(412,178.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.rf(["#FFFFFF","#FF2000","#FE1E00","#F20D00"],[0.137,0.541,0.569,0.631],69.6,-55.5,0,0.6,-0.1,89.6).s().p("Ak8DNInCs4IOYhBIJlArIhtEGInCM4IiuDwg");
	this.shape_45.setTransform(420,183.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#FFFFFF","#FF1500","#FE1400","#F70800"],[0.145,0.553,0.569,0.612],70.8,-61.8,0,0.1,-0.1,94.6).s().p("Ak8C2InKs3IOigrIJrAdIh+EKInJM3IiqD7g");
	this.shape_46.setTransform(427.9,188.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.rf(["#FFFFFF","#FF0B00","#FF0A00","#FB0400"],[0.149,0.565,0.573,0.596],72.1,-68,0,-0.3,-0.1,99.6).s().p("Ak8CfInSs2IOsgWIJxAPIiOEPInSM2IikEHg");
	this.shape_47.setTransform(435.9,193.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.rf(["#FFFFFF","#FF0000"],[0.157,0.576],45.5,-46.5,0,-0.9,-0.1,65.7).s().p("AnvmtIPfAAInwNbg");
	this.shape_48.setTransform(443.8,198.4,1.596,1.596);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.rf(["#FFFFFF","#FA0906","#F40600"],[0.157,0.569,0.576],76.2,-68.5,0,-0.8,-0.2,102.9).s().p("Am6ATIl1q/IAAgBIZfAAIgEALIl2K+Im1KSg");
	this.shape_49.setTransform(437.1,201.9);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.rf(["#FFFFFF","#F6110C","#EA0D00"],[0.157,0.565,0.576],79.2,-62.6,0,-0.8,-0.1,101.5).s().p("AniAvIlmraIAAgBIaRAAIgEAKIllLbIneJ1g");
	this.shape_50.setTransform(430.5,205.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.rf(["#FFFFFF","#F11A12","#DF1300"],[0.157,0.557,0.576],82.3,-56.7,0,-0.7,-0.1,100.4).s().p("AoLBMIlVr4IAAAAIbBAAIgCAJIlVL4IoHJYg");
	this.shape_51.setTransform(423.8,208.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.rf(["#FFFFFF","#EC2318","#D41A00"],[0.153,0.549,0.576],85.2,-50.9,0,-0.6,-0.1,99.8).s().p("Ao0BpIlFsUIAAgBIbzAAIgBAJIlFMUIowI8g");
	this.shape_52.setTransform(417.1,212.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.rf(["#FFFFFF","#E82C1E","#CA2000"],[0.153,0.545,0.576],88.2,-45.1,0,-0.6,-0.1,99.5).s().p("ApdCGIk1sxIAAgBIclAAIgBAJIk0MxIpZIfg");
	this.shape_53.setTransform(410.4,215.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.rf(["#FFFFFF","#E33524","#BF2600"],[0.153,0.537,0.576],91.1,-39.2,0,-0.6,-0.1,99.7).s().p("AqGCjIkltOIAAgBIdWAAIABAJIkkNNIqDIDg");
	this.shape_54.setTransform(403.7,219.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.rf(["#FFFFFF","#DE3D2A","#B52D00"],[0.153,0.529,0.573],94.1,-33.4,0,-0.6,-0.1,100.4).s().p("AqwC/IkUtqIAAAAIeHAAIACAHIkUNqIqsHmg");
	this.shape_55.setTransform(397.1,222.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.rf(["#FFFFFF","#DA4630","#AA3300"],[0.153,0.525,0.573],97,-27.6,0,-0.6,-0.1,101.4).s().p("ArZDcIkEuHIAAAAIe4AAIADAHIkDOHIrVHJg");
	this.shape_56.setTransform(390.5,226.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.rf(["#FFFFFF","#D54F36","#9F3900"],[0.153,0.518,0.573],99.9,-21.7,0,-0.7,-0.1,102.8).s().p("AsDD5IjzujIAAgBIfpAAIAEAHIjzOjIr+Gtg");
	this.shape_57.setTransform(383.8,229.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#FFFFFF","#D0583C","#954000"],[0.153,0.51,0.573],102.8,-15.8,0,-0.7,0,104.7).s().p("AssEWIjkvAIAAgBMAgbAAAIAFAHIjjPAIsnGQg");
	this.shape_58.setTransform(377.2,233.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFFFFF","#CC6042","#8A4600"],[0.153,0.506,0.573],105.7,-10,0,-0.7,0,106.9).s().p("AtVEzIjUvcIAAgBMAhMAAAIAGAFIjSPdItQF0g");
	this.shape_59.setTransform(370.6,236.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.rf(["#FFFFFF","#C76948","#804D00"],[0.153,0.498,0.573],108.7,-4.1,0,-0.7,0,109.4).s().p("At/FPIjDv5IAAAAMAh9AAAIAIAFIjDP5It5FXg");
	this.shape_60.setTransform(363.9,240.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FFFFFF","#C2724D","#755300"],[0.149,0.49,0.573],111.6,1.7,0,-0.7,0,112.3).s().p("AuoFsIizwWMAiuAAAIAJAFIiyQVIujE7g");
	this.shape_61.setTransform(357.3,244);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.rf(["#FFFFFF","#BE7A53","#6A5900"],[0.149,0.486,0.573],114.6,7.6,0,-0.7,0,115.5).s().p("AvSGJIiiwyIAAgBMAjfAAAIAKAFIiiQyIvLEeg");
	this.shape_62.setTransform(350.6,247.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FFFFFF","#B98359","#606000"],[0.149,0.478,0.573],117.4,13.3,0,-0.7,0,118.9).s().p("Av7GmIiTxQMAkRAAAIALAEIiSRPIv0ECg");
	this.shape_63.setTransform(344,251);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.rf(["#FFFFFF","#B48C5F","#556600"],[0.149,0.471,0.573],120.4,19.2,0,-0.7,0,122.6).s().p("AwlHCIiCxrIAAAAMAlCAAAIANADIiCRsIweDkg");
	this.shape_64.setTransform(337.4,254.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.rf(["#FFFFFF","#B09565","#4A6C00"],[0.149,0.467,0.573],123.3,25.1,0,-0.7,0,126.6).s().p("AxOHfIhyyIIAAAAMAlzAAAIAOADIhySIIxGDIg");
	this.shape_65.setTransform(330.7,257.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.rf(["#FFFFFF","#AB9E6B","#407300"],[0.149,0.459,0.573],126.3,30.9,0,-0.7,0,130.7).s().p("Ax4H8IhhykIAAgBMAmkAAAIAPADIhhSkIxwCsg");
	this.shape_66.setTransform(324.1,261.5);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.rf(["#FFFFFF","#A6A671","#357900"],[0.149,0.451,0.569],129.1,36.7,0,-0.8,0,135).s().p("AyhIZIhRzBMAnVAAAIAQACIhRTAIyZCPg");
	this.shape_67.setTransform(317.5,265);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.rf(["#FFFFFF","#A2AF77","#2B7F00"],[0.149,0.447,0.569],132,42.5,0,-0.8,0,139.5).s().p("AzKI1IhBzdMAoGAAAIARACIhATdIzCBzg");
	this.shape_68.setTransform(310.8,268.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.rf(["#FFFFFF","#9DB87D","#208600"],[0.145,0.439,0.569],135,48.4,0,-0.8,0.1,144.2).s().p("AzzJSIgyz6MApLAABIgxT6IzrBWg");
	this.shape_69.setTransform(304.2,272);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.rf(["#FFFFFF","#98C183","#158C00"],[0.145,0.431,0.569],137.8,54.2,0,-0.8,0.1,148.9).s().p("A0dJvIgh0XMAp9AABIghUXI0UA5g");
	this.shape_70.setTransform(297.6,275.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.rf(["#FFFFFF","#94C989","#0B9300"],[0.145,0.427,0.569],140.8,60.1,0,-0.8,0.1,153.8).s().p("A1GKMIgR00MAqvAABIgQUzI09Adg");
	this.shape_71.setTransform(290.9,278.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.rf(["#FFFFFF","#8FD28F","#009900"],[0.145,0.42,0.569],84.4,38.8,0,-0.8,0.1,93.6).s().p("AszGRIAAshIZnAAIAAMhg");
	this.shape_72.setTransform(284.3,282.4,1.698,1.698);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.rf(["#FFFFFF","#89CA93","#009305"],[0.149,0.427,0.584],137.4,60.9,0,-0.8,0,151.7).s().p("A1CKfIgElEQgGnyAAn1QH1gJH7gFQH1gGH4AAQFaAAFZACIAEFEQAFHyAAH1IgGAAQnzAJn5AFQn1AGn3AAQlZAAlYgCg");
	this.shape_73.setTransform(276.3,279.2);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.rf(["#FFFFFF","#84C198","#008D0A"],[0.153,0.431,0.604],131.2,56.1,0,-0.8,0.1,144.5).s().p("A0VKVIgIk6QgMnjAAnpQHhgRHsgMQHmgLHrAAQFSAAFPAGIAIE5QAMHjAAHpIgHAAQneAQnqANQnmALnrAAQlRAAlOgFg");
	this.shape_74.setTransform(268.3,276);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.rf(["#FEFFFF","#7EB99C","#00870E"],[0.157,0.439,0.62],124.9,51.1,0,-0.8,0,137.4).s().p("AznKLIgMkwQgSnUAAndQHLgZHdgRQHYgSHfAAQFJAAFFAIIAMEwQASHUAAHcIgGABQnKAZnbARQnXASnfAAQlIAAlEgIg");
	this.shape_75.setTransform(260.3,272.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.rf(["#FEFFFF","#78B0A1","#008113"],[0.161,0.447,0.639],118.7,46.3,0,-0.8,0.1,130.2).s().p("Ay6KBIgQkmQgXnFAAnRQG2ghHOgXQHIgYHTAAQFBAAE8ALIAQElQAXHGAAHQIgGAAQm1AinMAXQnIAXnSABQlAAAk7gLg");
	this.shape_76.setTransform(252.3,269.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.rf(["#FEFFFF","#72A8A5","#007A18"],[0.165,0.455,0.655],112.5,41.5,0,-0.8,0.1,123.1).s().p("AyMJ3QgLiMgJiPQgem3AAnEQGigqG/gdQG5geHHABQE4gBEzAOIATEbQAdG3AAHEIgFABQmgApm9AeQm6AcnGAAQk3AAkxgNg");
	this.shape_77.setTransform(244.3,266.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.rf(["#FEFFFF","#6DA0AA","#00741D"],[0.169,0.459,0.671],106.5,36.8,0,-0.8,0.1,116.2).s().p("AxfJtQgNiGgLiLQgjmoAAm4QGNgyGwgjQGqgjG7AAQEvAAEpAQQAMCHAMCKQAiGoABG4IgFAAQmLAymvAjQmrAjm5AAQkvAAkogQg");
	this.shape_78.setTransform(236.3,263.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.rf(["#FEFFFF","#6797AE","#006E21"],[0.173,0.467,0.69],100.4,31.9,0,-0.8,0,109.2).s().p("AwyJjIgckHQgomZAAmsIAAAAQF4g6GhgpQGbgpGuAAQEoAAEeATQAPCBANCFQApGaAAGrIgFABQl2A6mgApQmbApmuAAQkmAAkfgTg");
	this.shape_79.setTransform(228.3,259.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.rf(["#FDFFFF","#618FB3","#006826"],[0.176,0.475,0.706],94.4,27.3,0,-0.8,0,102.3).s().p("AwEJZQgRh8gPiAQgumLAAmfIAAgBQFjhCGRgwQGOguGhAAQEfAAEVAWQARB7APCBQAuGLAAGeIgEACQliBCmQAvQmNAvmhAAQkeAAkVgWg");
	this.shape_80.setTransform(220.2,256.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.rf(["#FDFFFF","#5C86B7","#00622B"],[0.18,0.478,0.725],88.4,22.6,0,-0.8,0,95.5).s().p("AvWJPQgTh2gRh8Qg1l8AAmTIAAgBQFOhKGDg1QF/g1GVAAQEWAAELAZQATB2ARB8QA0F8ABGSIgFABQlNBLmBA1Ql+A1mVAAQkVAAkLgZg");
	this.shape_81.setTransform(212.2,253.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.rf(["#FDFFFF","#567EBC","#005C30"],[0.184,0.486,0.741],82.5,18,0,-0.8,0,88.7).s().p("AupJFQgUhxgTh3Qg7ltAAmHIAAgBQE5hTF0g6QFvg6GKAAQENAAECAbQAVBwASB3QA7FtAAGHIgEABQk4BTlzA7QluA6mJAAQkNABkCgcg");
	this.shape_82.setTransform(204.2,250.2);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.rf(["#FDFFFF","#5076C0","#005634"],[0.188,0.494,0.757],76.7,13.4,0,-0.8,0,82.2).s().p("At7I7QgYhrgUhzQhAleAAl7IAAAAQEkhcFkhAQFhhAF9AAQEFAAD4AeQAXBrAVByQBAFeAAF7IgEABQkjBblkBBQlgBAl8AAQkFAAj3geg");
	this.shape_83.setTransform(196.2,247);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.rf(["#FDFFFF","#4A6DC5","#005039"],[0.192,0.502,0.776],70.8,8.9,0,-0.8,0,75.8).s().p("AtNIxQgahmgXhtQhGlRAAltIAAgBQEQhkFWhGQFThGFuAAQD9AADvAgQAZBnAWBtQBGFRABFtIgEAAQkPBllUBGQlTBGluAAQj9AAjtghg");
	this.shape_84.setTransform(188.2,243.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.rf(["#FCFFFF","#4565C9","#00493E"],[0.2,0.506,0.792],64.7,4.3,0,-0.9,0,69.6).s().p("AsgInQgchggYhoQhMlDAAlhIAAgBQD7hsFGhMQFFhMFiAAQD0AADlAjQAcBhAXBpQBMFCABFgIgDACQj6BslGBLQlEBNljAAQjzAAjkgkg");
	this.shape_85.setTransform(180.2,240.5);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.rf(["#FCFFFF","#3F5CCE","#004343"],[0.204,0.514,0.812],59,-0.1,0,-0.8,0,63.6).s().p("AryIeQgehcgahjQhSk0AAlVIAAAAQDmh1E3hSQE2hSFWAAQDsAADbAmQAdBcAaBjQBSE0AAFUIgDACQjlB0k2BRQk1BTlXAAQjqAAjbgmg");
	this.shape_86.setTransform(172.2,237.3);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.rf(["#FCFFFF","#3954D2","#003D47"],[0.208,0.522,0.827],53.3,-4.5,0,-0.9,0,58.1).s().p("ArFIUQgghWgchfQhYklABlIIAAgBQDQh9EphYQEnhYFJAAQDkAADRApQAgBWAbBfQBYEkAAFIIgCACQjRB9knBYQkmBXlLABQjiAAjRgpg");
	this.shape_87.setTransform(164.2,234.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.rf(["#FCFFFF","#334CD7","#00374C"],[0.212,0.529,0.843],47.7,-8.9,0,-0.8,0,52.9).s().p("AqXIKQgihRgehaQhekWAAk8IAAgBQC8iFEZheQEYhdE+AAQDbAADIArQAhBRAeBZQBdEWABE8IgDACQi7CFkZBdQkXBek+AAQjaAAjHgrg");
	this.shape_88.setTransform(156.2,230.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.rf(["#FCFFFF","#2E43DB","#003151"],[0.216,0.533,0.863],42.1,-13.3,0,-0.9,0,48.4).s().p("ApqIAQgkhLgghWQhjkIAAkvIAAgBQCniNEKhkQEJhjEyAAQDSAAC+AuQAkBLAfBVQBjEHABEwIgCACQinCNkKBjQkIBkkyAAQjRAAi+gug");
	this.shape_89.setTransform(148.2,227.6);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.rf(["#FBFFFF","#283BE0","#002B56"],[0.22,0.541,0.878],36.6,-17.5,0,-0.8,0,44.7).s().p("Ao8H2QgnhGghhQQhqj5AAkkIAAAAQCTiWD7hqQD7hpElAAQDJAAC0AxQAmBGAiBQQBpD4AAEjIgBADQiSCVj7BpQj6BqklAAQjJAAi0gxg");
	this.shape_90.setTransform(140.2,224.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.rf(["#FBFFFF","#2232E4","#00255A"],[0.224,0.549,0.898],31.1,-21.9,0,-0.8,-0.1,42).s().p("AoPHsQgohBgkhLQhvjrAAkXIAAgBQB+ieDshvQDshvEYAAQDBAACrA0QAoBAAkBLQBuDqAAEXIgBACQh9CejsBvQjrBwkYAAQjBAAirg0g");
	this.shape_91.setTransform(132.2,221.2);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.rf(["#FBFFFF","#1D2AE9","#001F5F"],[0.227,0.553,0.914],25.7,-26.1,0,-0.8,0,40.6).s().p("AniHiQgqg7gmhHQh0jbAAkMIAAAAQBoimDeh2QDch1EMABQC5AACiA2QAqA6AkBHQB1DbABELIgCACQhnCmjeB1QjbB1kNABQi4gBiig2g");
	this.shape_92.setTransform(124.2,218);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.rf(["#FBFFFF","#1722ED","#001864"],[0.231,0.561,0.929],20.3,-30.3,0,-0.9,0,40.4).s().p("Am0HYQgsg2gohCQh6jNAAj+IAAgBQBTiuDOh7QDPh7EAgBQCwABCXA5QAsA1AnBCQB7DMAAD/IgBACQhTCujOB7QjNB8kBAAQivgBiYg5g");
	this.shape_93.setTransform(116.1,214.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.rf(["#FBFFFF","#1119F2","#001269"],[0.235,0.569,0.949],14.9,-34.4,0,-0.9,0,41.6).s().p("AmGHOQgvgwgpg9QiBi+AAjzIAAgBQA/i3C/iBQC/iAD0AAQCoAACOA8QAuAwApA8QCAC+ABDyIgBADQg/C2i/CBQi+CBj0AAQinAAiOg8g");
	this.shape_94.setTransform(108.1,211.5);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.rf(["#FAFFFF","#0B11F6","#000C6D"],[0.239,0.576,0.965],9.6,-38.5,0,-0.9,0,44).s().p("AlZHFQgwgsgsg4QiGivAAjnIAAgBQAqi/CwiHQCwiGDoAAQCfAACEA+QAwArArA4QCGCvABDlIgBADQgpDAixCGQivCHjoAAQieAAiFg+g");
	this.shape_95.setTransform(100.1,208.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.rf(["#FAFFFF","#0608FB","#000672"],[0.243,0.58,0.984],4.3,-42.7,0,-0.9,0,47.6).s().p("AkrG6Qgzglgtg0QiMigAAjaIAAgBQAUjICiiNQChiMDbAAQCXAAB6BCQAzAlAsAzQCMCgABDZIAAADQgVDHiiCNQigCNjcAAQiWAAh6hCg");
	this.shape_96.setTransform(92.1,205);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(309.1,351.8,100.1,100.1);
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 16,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;