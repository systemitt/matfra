(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
var rect; // used to reference frame bounds
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



// stage content:
(lib.interpolaciondeforma = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660033").s().p("AlgFhQiTiSAAjPQAAjOCTiTQCSiSDOAAQDPAACSCSQCTCTAADOQAADPiTCSQiSCTjPAAQjOAAiSiTg");
	this.shape.setTransform(86.4,85.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#670034").s().p("Aj8G5Qg3gigvgyQgugygfg4QhHhvgFiMQAFiJBFhvQAgg5AvgyQAugyA1giQByg+CNADIAAAAQCMgEBxA9QA2AjAvAyQAvAyAfA3QBHBvAFCLIAAABQgFCKhFBvQggA4gvA0QgvAxg0AiQhzA+iNgCIgOAAQiDAAhrg7g");
	this.shape_1.setTransform(91.8,88.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#680035").s().p("AkAHBQg1glgug1Qgugzgdg5QhLhwgKiKQAKiJBJhvQAfg7Atg1QAtgzA0gkQB0g6CPAHIAAgBQCOgGBzA4QA1AlAtA0QAuAzAfA6QBKBvAKCKIAAABQgKCJhJBvQgfA7guA0QgtA0g0AkQhzA6iQgGIgcAAQh9AAhngyg");
	this.shape_2.setTransform(97.3,91.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#680035").s().p("AkEHHQg0gmgsg3Qgsg1geg6QhOhwgPiKQAPiIBNhwQAdg9Atg1QAsg1AygnQB2g1CRAKIAAAAQCQgLB1A0QAzAnAtA2QAsA1AeA7QBOBwAPCJIAAABQgPCIhNBvQgdA9gtA2QgsA1gyAmQh3A3iRgLQgXACgXAAQh0AAhigsg");
	this.shape_3.setTransform(102.8,94.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#690036").s().p("AkIHOQgygogsg4Qgqg3gdg9QhShwgUiJQAUiIBQhvQAdg/Arg4QArg2AxgoQB4gyCTANIAAAAQCSgNB3AwQAyAoArA4QArA3AdA8QBSBwATCJIAAABQgSCIhSBvQgcA+gsA4QgqA3gxAoQh5AyiTgNQgeADgfAAQhtAAhegmg");
	this.shape_4.setTransform(108.3,98);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6A0037").s().p("AkMHVQgxgrgqg6Qgqg4gbg+QhWhwgZiJQAZiIBUhvQAchAAqg6QApg4AwgqQB6gtCVAQIAAAAQCTgRB6AsQAwAqArA6QApA4AcA/QBWBvAZCIIgBACQgYCHhUBvQgcBAgqA6QgqA5gvAqQh7AtiVgQQgoAFgnAAQhlAAhYggg");
	this.shape_5.setTransform(113.7,101.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6B0038").s().p("AkPHcQgwgtgpg7Qgpg6gahBQhbhvgdiJQAdiHBZhvQAbhCApg8QAog6AtgsQB9goCXATIAAAAQCUgUB8AnQAwAtApA7QApA6AbBAQBZBvAeCJIAAABQgdCHhZBvQgbBCgpA7QgoA7guAsQh9ApiWgUQgyAHgwAAQhdAAhRgag");
	this.shape_6.setTransform(119.2,104.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#6B0038").s().p("AkTHjQgvgugog+Qgng8gZhCQhehwgiiIQAhiGBdhwQAZhDAog9QAng9AtgtQB/glCYAXIAAAAQCWgYB+AjQAuAvAoA9QAoA8AaBBQBdBwAiCIIAAABQghCGhdBvQgZBEgoA+QgnA7gtAvQh/AkiYgXIAAAAQg9AKg5AAQhTAAhLgVg");
	this.shape_7.setTransform(124.7,107.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6C0039").s().p("AkXHqQgtgxgng/Qgmg+gZhDQhihwgmiHQAmiHBghvQAZhFAmhAQAmg9ArgwQCCggCZAaIABAAQCYgbCAAeQAtAxAnA/QAmA+AZBDQBhBvAnCHIAAACQgnCGhgBvQgYBFgnA/QgmA/grAwQiCAgiagbQhHANhDAAQhKAAhDgQg");
	this.shape_8.setTransform(130.2,110.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6D003A").s().p("AkbHxQgsgzglhBQglhAgYhFQhlhvgsiIQAriGBkhvQAYhHAlhAQAkhAArgyQCDgcCcAdIAAAAQCageCCAaQArAzAmBBQAlBAAYBEQBlBvAsCHIAAACQgrCGhkBuQgXBHgmBBQglBBgqAyQiEAcibgeQhSAQhNAAQhBAAg8gMg");
	this.shape_9.setTransform(135.6,114.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6E003B").s().p("AkfH4Qgqg1glhDQgkhBgWhHQhqhvgwiHQAwiGBohvQAWhJAkhDQAjhBApg0QCGgXCdAhIABgBQCbghCFAVQAqA1AkBDQAkBBAXBHQBpBvAxCGIAAACQgwCFhoBvQgWBJglBCQgjBCgpA0QiGAYidghQheAUhWAAQg4AAg0gIg");
	this.shape_10.setTransform(141.1,117.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#6F003C").s().p("AkjH/Qgpg3gjhFQgjhDgWhJQhshvg2iGQA1iGBrhvQAWhKAihFQAjhDAng2QCIgTCfAkIABAAQCdglCHARQApA3AiBFQAjBDAXBIQBsBvA2CGIAAACQg1CEhrBvQgWBLgkBEQghBEgoA2QiIATifgkIgBAAQhqAZhiAAQgsAAgrgFg");
	this.shape_11.setTransform(146.6,120.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#6F003C").s().p("AknIGQgng5gihHQgihFgUhKQhxhvg7iGQA6iFBwhvQAUhMAihHQAghEAng4QCKgPChAnIAAAAQCfgoCJANQAnA5AiBGQAhBFAVBKQBxBvA7CFIAAACQg6CEhvBvQgUBMgjBHQggBFgnA4QiKAPihgoIAAAAQh4AfhsAAQgjAAghgDg");
	this.shape_12.setTransform(152.1,123.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#70003D").s().p("AkqIMQgng7ghhHQgghIgUhLQh1hwg/iFQA/iEBzhwQAThOAhhHQAfhHAlg6QCMgLCkArIAAAAQChgsCKAJQAnA7AgBIQAhBHAUBLQB1BvA/CFIAAACQg/CEhzBuQgUBOggBJQggBHgkA5QiOALiigrQiGAlh4AAIgtgCg");
	this.shape_13.setTransform(157.6,127.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#71003E").s().p("AkuITQglg9gghKQgfhIgThOQh5hvhEiFQBEiEB3hvQAShPAfhLQAfhIAjg8QCPgGCkAuIABAAQCigvCNAEQAlA9AgBKQAfBIATBOQB4BuBFCFIgBABQhDCEh3BvQgSBPggBLQgeBIgkA8QiOAHilgvIAAAAQiUAsiDAAIgYgBg");
	this.shape_14.setTransform(163,130.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#72003F").s().p("Al0GPQgehKgShPQh9hvhJiFQBJiDB7hwQARhRAehMQAdhKAig+QCRgCCmAyIABgBQCkgyCPAAQAkA/AeBMQAeBKASBPQB9BuBJCEIgBACQhICDh6BvQgSBRgeBMQgdBLgiA+QiRACingyQijAziQAAQgkg/gehMg");
	this.shape_15.setTransform(168.5,133.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#72003F").s().p("Al2GTQgdhNgQhRQiBhuhOiEQBOiEB+hvQARhTAchOQAchLAhhAQCTACCoA1IABAAQClg2CSgFQAiBBAdBOQAdBMARBQQCABvBPCDIgBACQhNCDh+BvQgRBTgdBOQgbBMghBAQiUgCiog1IAAgBQilA3iSAEQgihBgehNg");
	this.shape_16.setTransform(174,136.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#730040").s().p("Al3GWQgchOgQhTQiEhvhSiDQBRiDCDhwQAPhUAbhPQAchOAfhCQCVAHCqA4IABAAQCng6CTgJQAiBEAbBOQAcBPAQBSQCFBuBTCDIgBACQhSCCiCBvQgPBVgcBPQgbBPgfBBQiWgGipg4IgBgBQinA6iUAJQghhDgchPg");
	this.shape_17.setTransform(179.5,140);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#740041").s().p("Al5GZQgahQgPhUQiIhvhXiDQBWiDCHhvQAOhWAahSQAahPAehEQCXALCsA8IABgBQCpg9CVgNQAgBFAbBRQAaBQAQBUQCIBuBXCDIAAABQhXCDiGBvQgOBWgbBSQgZBPgeBEQiYgKisg9QipA+iWANQgghGgbhQg");
	this.shape_18.setTransform(184.9,143.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#750042").s().p("Al6GcQgZhSgOhWQiMhvhciCQBciCCJhwQAOhYAZhTQAYhRAchGQCaAQCuA+IABAAQCqhACYgSQAfBIAZBSQAaBSAOBVQCLBvBdCCIgBABQhbCCiJBwQgOBXgZBTQgYBSgdBGQiagPiug/IAAAAQirBAiYASQgehHgahTg");
	this.shape_19.setTransform(190.4,146.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#750042").s().p("Al7GfQgYhTgNhYQiQhvhhiCQBgiCCOhvQANhaAXhVQAXhSAchIQCcATCvBCIABAAQCshECagWQAdBKAZBVQAYBTANBXQCQBuBhCCIAAACQhgCBiOBvQgNBagYBVQgWBTgcBIQicgUivhCIgBAAQisBEiaAWQgehJgYhVg");
	this.shape_20.setTransform(195.9,149.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#760043").s().p("Al9GiQgWhVgNhaQiThuhmiCQBliBCShwQAMhbAWhXQAWhUAZhKQCfAYCxBFIABAAQCuhHCcgbQAcBMAXBWQAXBVANBZQCTBuBmCBIAAACQhlCBiSBwQgLBbgXBWQgWBVgaBKQifgXiwhGIAAAAQivBHicAbQgchMgYhWg");
	this.shape_21.setTransform(201.4,152.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#770044").s().p("Al+GlQgVhXgMhbQiXhuhriCQBqiACWhxQAKhcAVhZQAVhWAZhMQCgAdCzBIIABgBQCwhKCegeQAaBNAWBYQAWBXAMBbQCXBuBrCAIgBACQhpCBiWBvQgKBdgVBZQgVBWgZBMQiggcizhJIgBAAQivBKifAfQgbhNgWhYg");
	this.shape_22.setTransform(206.8,156.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#780045").s().p("Al/GoQgUhYgLheQichuhviAQBviBCahwQAJhfAThZQAUhZAXhOQCjAhC1BMIABgBQCxhNChgjQAZBPAUBaQAVBZALBdQCbBuBwB/IgBACQhuCAiZBwQgKBegUBbQgTBYgYBOQijggi0hMIAAAAQiyBOigAiQgahOgVhbg");
	this.shape_23.setTransform(212.3,159.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#790046").s().p("AmAGrQgUhagJhfQifhuh1iAQB0iACdhxQAJhfAShdQAShaAWhQQClAlC3BPIABAAQCzhRCignQAYBRAUBcQATBbAKBeQCfBtB1CAIgBABQhzCBidBvQgJBhgSBcQgSBagXBQQilgli2hPQi0BQiiAoQgZhRgThcg");
	this.shape_24.setTransform(217.8,162.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#790046").s().p("AmCGuQgShcgJhgQijhvh5h/QB5iAChhwQAHhiASheQAQhbAWhSQCnApC4BSIABAAQC1hUCkgsQAXBTASBeQASBcAKBgQCiBuB6B/IgBACQh4B/ihBwQgIBigRBeQgQBbgWBSQingoi4hTIgBgBQi0BVilAsQgXhTgTheg");
	this.shape_25.setTransform(223.3,165.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#7A0047").s().p("AmDGxQgRhegIhiQimhuh/h/QB+h/ClhxQAGhjAQhgQAQhdAThUQCqAtC6BWIABAAQC2hYCngwQAVBWASBfQARBeAIBhQCmBvB/B+IgBACQh9B+ilBxQgGBjgQBfQgQBfgUBTQipgti6hWIAAgBQi3BZinAwQgWhVgRhgg");
	this.shape_26.setTransform(228.7,169);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#7B0048").s().p("AmFG0QgPhggHhjQiqhuiEh/QCCh/CqhwQAFhlAPhiQAOhfAThWQCrAyC7BZIACAAQC4hbCpg1QAUBYAQBhQAQBgAHBjQCrBuCCB+IAAABQiCB/ioBwQgGBlgOBiQgPBfgSBWQisgyi8hZIAAAAQi4BbiqA1QgUhXgRhig");
	this.shape_27.setTransform(234.2,172.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#7C0049").s().p("AmGG4QgPhigFhlQivhuiHh/QCHh+CshwQAFhoANhjQANhhARhXQCtA2C/BcIABAAQC6hfCqg5QAUBaAOBjQAPBhAGBlQCuBuCJB9IgBACQiHB+isBwQgFBngNBkQgNBhgSBYQitg2i+hdIgBAAQi6BfirA5QgThZgPhjg");
	this.shape_28.setTransform(239.7,175.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#7C0049").s().p("AmHG6QgNhjgFhnQiyhuiOh9QCNh/CwhwQADhpAMhlQANhiAPhaQCwA6C/BgIABAAQC9hiCsg+QASBcAOBlQAOBjAEBnQCyBtCNB9IgBACQiKB+ixBwQgDBpgMBlQgMBjgRBaQiwg7i+hgIgBAAQi8BiiuA+QgRhcgOhlg");
	this.shape_29.setTransform(245.2,178.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#7D004A").s().p("AmIG9QgNhkgDhqQi3htiRh9QCQh/C1hvQAChrALhnQALhkAOhcQCyA/DCBjIABAAQC9hmCvhCQARBeAMBnQAMBlAFBoQC2BuCRB8IAAACQiRB9i0BxQgCBqgLBnQgLBlgOBbQiyg+jChkIAAAAQi+BmivBCQgRhegMhng");
	this.shape_30.setTransform(250.6,181.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#7E004B").s().p("AmKHBQgLhngDhrQi6huiWh8QCVh+C4hwQAChsAJhpQAKhmANheQC0BDDDBnIABgBQDAhoCxhHQAPBgALBpQALBnAEBpQC5BuCXB8IgBACQiVB9i4BwQgBBsgKBpQgJBmgNBeQi1hDjDhmIAAgBQi/BpiyBHQgPhggMhog");
	this.shape_31.setTransform(256.1,185.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#7F004C").s().p("AmLHDQgKhogChsQi+huibh8QCah9C8hxQABhtAIhrQAIhoAMhgQC3BIDFBqIABgBQDBhsCzhLQAOBjAKBqQAKBoACBsQC+BtCbB8IgBACQiZB8i8BwQAABugJBqQgIBpgMBgQi3hHjEhqIgBAAQjBBsi0BKQgOhhgKhrg");
	this.shape_32.setTransform(261.6,188.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#80004D").s().p("AmMHHQgJhrgBhtQjChuigh8QCfh9DAhxQgBhuAHhtQAIhpAKhjQC5BNDGBsIABAAQDDhvC2hPQAMBjAJBtQAJBqABBtQDCBtCgB8IgBACQieB8jABwQABBwgIBsQgHBqgKBhQi5hLjGhtIgBAAQjDBvi2BQQgMhkgJhsg");
	this.shape_33.setTransform(267.1,191.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#80004D").s().p("AmOHKQgIhtABhvQjGhtilh7QCkh9DDhxQgChxAHhuQAGhrAIhlQC7BRDJBxIABgBQDEhzC4hTQAMBmAHBuQAIBsAABuQDFBuCmB7IgBABQijB8jEBxQACBwgGBvQgGBrgJBlQi7hRjJhwIAAgBQjEB0i5BTQgLhmgIhtg");
	this.shape_34.setTransform(272.6,194.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#81004E").s().p("AmPHNQgGhuAAhxQjJhuiqh6QCph9DHhwQgChzAEhwQAGhtAHhmQC9BVDKBzIABAAQDGh3C7hXQAKBoAGBwQAHBtgCBxQDKBtCqB7IgBABQioB8jHBwQACBzgEBwQgFBtgIBmQi9hUjKh0IAAAAQjHB3i6BXQgKhogHhvg");
	this.shape_35.setTransform(278,198);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#82004F").s().p("AmQHQQgFhwABhyQjNhuivh7QCuh7DLhxQgEh0ADhyQAEhvAGhoQDABZDMB3IABgBQDIh5C9hcQAHBqAGByQAGBvgDByQDOBuCvB6IgBABQitB8jLBvQAEB1gEByQgDBvgHBoQjAhYjLh3IgBgBQjIB6i9BcQgIhqgFhxg");
	this.shape_36.setTransform(283.5,201.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#830050").s().p("AmSHTQgEhyADh0QjRhui0h5QCzh8DPhwQgFh2ACh0QADhxAFhqQDBBeDOB6IABgBQDKh9C+hgQAHBsAEB0QAEBxgCB0QDRBtC0B6IgBABQiyB7jPBwQAFB2gDB0QgCBxgFBqQjBhdjOh6IgBgBQjJB+i/BgQgIhsgEhzg");
	this.shape_37.setTransform(289,204.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#830050").s().p("AmTHWQgDh0AEh1QjVhui5h5QC4h7DThxQgGh4ABh1QABhyAEhsQDEBiDPB9IABgBQDMiADAhlQAGBvADB1QADBygEB3QDVBtC5B5IgBABQi3B7jSBwQAFB4gBB1QgBBygEBtQjDhijQh9IAAgBQjMCBjBBlQgGhugDh1g");
	this.shape_38.setTransform(294.4,207.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#840051").s().p("AmUHZQgCh1AFh4QjZhti+h5QC9h7DWhwQgHh6AAh3QAAh0AChuQDHBmDRCBIABgBQDNiEDDhpQAEBxACB3QACB0gFB4QDZBtC+B4IgCACQi7B6jWBwQAGB6AAB3QABB0gDBuQjGhljRiBIgBgBQjNCFjDBpQgFhwgBh3g");
	this.shape_39.setTransform(299.9,210.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#850052").s().p("AmVHdQgBh4AFh6QjdhsjCh4QDBh7DbhxQgIh7gBh5QgBh2ABhwQDHBqDUCEIABAAQDPiHDFhuQADBzAAB5QABB2gGB5QDdBtDDB4IgCABQjAB6jaBwQAIB8ABB5QABB2AABwQjJhpjTiFIgBAAQjOCHjGBuQgDhyAAh4g");
	this.shape_40.setTransform(305.4,214.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#860053").s().p("AmXHfQABh4AGh8QjghtjHh3QDFh6DfhxQgJh9gDh7QgCh4gBhyQDLBvDVCIIABgBQDRiLDGhyQACB1AAB7QgBB4gGB7QDgBtDHB3IgBABQjFB6jeBwQAJB9ADB7QACB4AAByQjKhujViIIAAAAQjRCLjIByQgCh0ABh7g");
	this.shape_41.setTransform(310.9,217.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#860053").s().p("AmZHiQACh6AIh9QjkhtjNh4QDLh5DjhxQgLh+gEh9QgDh6gBhzQDMByDXCLIABAAQDSiODJh3QABB3gCB8QgCB7gHB8QDkBtDNB3IgCACQjKB4jiBxQALB+ADB9QAEB6ABB0QjMhyjWiMIgBgBQjTCPjJB2QgBh2ABh8g");
	this.shape_42.setTransform(316.4,220.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#870054").s().p("AmaHmQADh9AJh+QjohtjRh4QDPh4DmhyQgLh/gFh/QgFh8gDh2QDPB4DZCOIABgBQDUiRDLh7IgDD4QgDB7gJB+QDoBtDRB3IgBABQjPB5jlBwQALCAAFB+QAFB9ADB1QjPh2jZiPIAAgBQjUCTjMB7QAAh5ADh9g");
	this.shape_43.setTransform(321.8,223.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#880055").s().p("AmbHpQAEh/AKiAQjthtjVh2QDUh5DqhxQgMiBgHiBIgKj1QDRB8DbCRIABAAQDViVDOiAQgCB8gEB/QgEB+gKCAQDtBtDVB1IgBACQjTB5jqBwQAMCCAGCAQAHB+AEB4QjSh8jZiRIgBgBQjWCWjOB+QACh6AEh/g");
	this.shape_44.setTransform(327.3,227.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#890056").s().p("AmcHsQAFiAAKiCQjwhtjah2QDZh4DuhxQgNiEgIiCQgHh/gGh6QDUCADcCVIABgBQDXiYDQiDQgDB9gGCCQgFB/gKCBQDvBtDbB2IgBABQjYB4juBwQANCEAICCQAHB/AGB6QjTh/jciVIgBgBQjYCZjQCDQADh8AGiBg");
	this.shape_45.setTransform(332.8,230.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#8A0057").s().p("AmdHvQAGiCALiEQjzhtjgh1QDeh4DxhxQgOiFgIiEIgQj9QDVCFDfCYIABgBQDZibDRiIQgEB/gGCDQgHCBgLCDQDzBtDgB1IgBABQjeB4jxBxQAOCFAKCEIAPD9QjViEjfiYIAAgBQjaCcjRCIQAEh+AHiDg");
	this.shape_46.setTransform(338.3,233.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#8A0057").s().p("AmfHyQAIiEAMiFQj4htjkh1QDjh3D1hxQgPiHgKiGQgKiCgIh+QDYCIDgCcIABgBQDbifDUiMQgGCBgICGQgICCgMCFQD4BtDkB0IgBACQjiB3j1BwQAPCIAKCFQAKCDAIB+QjYiIjficIgBgBQjbCgjUCMQAFiAAIiFg");
	this.shape_47.setTransform(343.7,236.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#8B0058").s().p("AmgH1QAIiGAOiHQj7hsjqh1QDoh2D5hyQgQiJgMiHIgUkEQDaCNDhCfIABgBQDeiiDWiRIgQEKQgKCFgNCHQD7BsDqB0IgCACQjmB3j5BwQAQCJAMCHQALCEAJCBQjaiNjhieIgBgBQjcCijXCRQAHiCAJiHg");
	this.shape_48.setTransform(349.2,240);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#8C0059").s().p("AmiH4QAKiIAPiIQkAhtjuhzQDth3D9hxQgRiLgNiJIgXkIQDcCRDkCjIABgBQDeimDYiVIgSEPQgKCGgOCIQD/BsDuB0IgCABQjrB3j9BxQARCKANCJQANCGAKCCQjciQjjijIgBAAQjeCmjZCVIASkNg");
	this.shape_49.setTransform(354.7,243.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#8D005A").s().p("AmjH7QAMiJAOiKQkDhtjyhzQDwh3EBhxQgSiMgOiLIgZkMQDfCWDlClIABgBQDgipDaiZIgVESQgLCIgPCKQEDBtDzBzIgCABQjwB2kBBxQATCMAOCLIAZEMQjeiVjlimIAAAAQjhCpjbCaQAJiHAMiKg");
	this.shape_50.setTransform(360.2,246.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#8D005A").s().p("AmlH+QANiKAQiNQkHhsj3hzQD1h2EFhxQgTiOgPiNIgdkPQDhCaDoCoIAAAAQDiitDdieIgYEWQgMCKgQCMQEGBsD5BzIgCABQj1B2kFBxQAUCOAOCMIAdEQQjhiZjmipIgBgBQjiCtjdCeIAXkVg");
	this.shape_51.setTransform(365.7,249.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#8E005B").s().p("AmlIBQANiMARiOQkLhtj8hyQD7h2EIhxQgUiQgQiOIggkTQDjCeDpCsIACgBQDjiwDfiiQgMCMgOCOQgOCMgRCNQELBsD8ByIgBACQj6B1kIBxQAUCQARCOQAQCLAOCIQjiidjpisIgBgBQjjCwjgCjQANiLAOiOg");
	this.shape_52.setTransform(371.1,252.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#8F005C").s().p("AmnIEQAPiOASiPQkPhtkBhyQD/h1ENhyQgViRgSiQIghkXQDlCjDqCvIABgBQDmizDhinIgdEeQgPCOgRCOQEOBtEBBxIgBABQj/B1kMByQAVCRASCQQASCNAPCKQjkiijrivIgBgBQjlC0jhCmIAckcg");
	this.shape_53.setTransform(376.6,256.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#90005D").s().p("AmoIHQAPiQATiRQkShskGhxQEEh1EQhyQgWiTgTiSIgkkaQDoCnDsCyIABgBQDni2DjirIgfEhQgQCQgSCQQESBsEGByIgCABQkDB0kQBxQAWCUATCRQATCPARCMQjnimjsizIgBgBQjnC4jjCqQAOiOARiSg");
	this.shape_54.setTransform(382.1,259.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#90005D").s().p("AmqILQASiSATiTQkWhskLhxQEJh1EUhxQgXiVgUiUIgmkeQDpCrDuC2IABgBQDpi6DlivIghElQgRCRgUCSQEWBtELBwIgBABQkJB1kTBxQAXCUAUCUIAnEfQjqirjui2IAAgBQjpC7jmCvIAhkjg");
	this.shape_55.setTransform(387.5,262.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#91005E").s().p("AmrINQASiTAViVQkahskQhwQEOh1EYhxQgYiWgWiWIgokiQDrCvDxC5IABAAQDqi+DnizIgkEpQgTCTgUCUQEaBsEQBwIgBABQkNB0kXBxQAXCWAWCWQAVCSATCQQjriujvi6IgBgBQjrC+joC0IAkkog");
	this.shape_56.setTransform(393,265.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#92005F").s().p("AmsIQQATiVAViWQkdhtkVhvQETh0EchxQgZiYgXiYIgskmQDuCzDyC9IABgBQDtjADoi4IgmEtQgUCUgVCWQEeBsEVBwIgCABQkRB0kbBwIAvEwIAsEmQjuizjxi8IgBgCQjtDCjqC4IAnksg");
	this.shape_57.setTransform(398.5,269);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#930060").s().p("AmuIUQAViXAXiZQkihrkahwQEYh0EfhxQgZiZgYiaIgukqQDvC5D0DAIACgBQDtjFDsi8IgpExQgVCWgWCYQEhBsEaBvIgCABQkWB0kfBwIAyE0IAuEpQjwi3jzjAIgBgBQjuDEjtC9IApkvg");
	this.shape_58.setTransform(404,272.2);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#940061").s().p("AmvIWQAWiZAXiZQklhskfhvQEdhzEjhxQgbicgZibIgxktQDzC8D1DDIABgBQDwjHDujBIgsE1IgtExQElBsEfBvIgCAAQkcBzkiByQAbCbAZCbIAxEuQjyi8j2jEIgBAAQjvDIjvDBIAsk0g");
	this.shape_59.setTransform(409.5,275.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#940061").s().p("AmwIaIAvk2QkphskkhvQEihyEnhyIg3k6IgzkxQD1DAD3DHIABgBQDyjLDvjFIguE5IgvE0QEpBsEkBuIgCACQkgByknBxIA3E6IAzEyQj1jBj2jGIgBgBQjyDMjwDFIAuk3g");
	this.shape_60.setTransform(414.9,278.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#950062").s().p("AmyIdIAyk6QkuhrkohuQEmhzErhxIg4k+Ig2k1QD3DFD5DKIABgBIHlmYIgwE9IgyE4QEtBsEpBuIgCABQklBykqBxIA4E9IA2E2Qj3jFj4jKIgBgBQj0DPjyDKIAwk7g");
	this.shape_61.setTransform(420.4,281.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#960063").s().p("AmzIgIA0k9QkyhskthtQEshyEuhyIg7lBIg4k5QD5DKD7DNIABgBIHpmgIgzFBIg0E7QExBsEuBtIgCABQkqBykuBxIA7FBIA4E5Qj5jJj6jNIgBgBQj1DTj1DOIAzk/g");
	this.shape_62.setTransform(425.9,285.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#970064").s().p("Am0IjIA2lAIpojZQExhyEyhyIg9lEIg7k8IH4GeIABgBIHtmnIg2FEIg1E+QE1BsExBtIgCABQkuBxkyByIA9FEIA7E9Qj7jNj8jRIgBAAQj3DVj3DSIA2lCg");
	this.shape_63.setTransform(431.4,288.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#970064").s().p("Am2ImIA4lEIpvjYIJrjjIhAlIIg9lAIH8GlIABAAIHxmvIg4FIIg4FCIJvDYIgCABQkzBxk2BxIBAFIIA9FBQj9jSj+jUIgBgBQj4Daj6DWIA4lGg");
	this.shape_64.setTransform(436.8,291.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#980065").s().p("Am3IpIA6lIIp5jXIJ0jiIhBlMIhAlEIIAGuIABgBIH0m3Ig6FMIg6FGIJ5DXIgDABIpxDhIBBFMIBAFEIn/mtIgBgBIn1G4IA6lKg");
	this.shape_65.setTransform(442.3,294.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#990066").s().p("Al8DhIqBjXIJ8jiIiGqXIIEG1IH5m/Ih5KZIKBDXIp8DiICGKXIoEm1In5G/g");
	this.shape_66.setTransform(447.8,298);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#950462").s().p("AgGHCIn2GpIB0qQIprjdIgBAAIJyjkIh1qHIH+GrIH2mpIAAABIh0KQIJsDcIgBAAIpxDlIB1KIg");
	this.shape_67.setTransform(440.4,290.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#92075F").s().p("AgRHJInyGVIBvqJIpWjkIgBAAIJojlIhlp4IH6GjIHymUIAAACIhvKGIJXDlIgBAAIpoDkIBlJ6g");
	this.shape_68.setTransform(433,283.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#8E0B5B").s().p("AgcHRIntF+IBoqAIpAjrIgBAAIJfjkIhVprIH1GbIHul/IAAABIhqJ/IJCDsIgBAAIpeDlIBUJqg");
	this.shape_69.setTransform(425.6,276.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#8B0E58").s().p("AgoHZInoFoIBkp4IosjzIgBAAIJVjkIhDpcIHvGTIHplpIAAABIhkJ3IItDyIgBAAIpUDkIBDJdg");
	this.shape_70.setTransform(418.2,269.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#871254").s().p("AgzHgInkFSIBepvIoWj5IgBAAIJLjlIgzpOIHrGKIHllTIAAABIhfJuIIYD6IgBAAIpKDlIAzJNg");
	this.shape_71.setTransform(410.8,262.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#841551").s().p("Ag+HnIngE9IBapnIoCkBIgBAAIJBjlIgio+IHmGAIHhk8IAAABIhZJlIICEBIgBAAIpADlIAiI/g");
	this.shape_72.setTransform(403.4,254.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#80194D").s().p("AhJHuIncEoIBUpfIntkIIAAAAII3jlIgSowIHjF4IHcknIAAABIhUJeIHtEHIgBAAIo3DmIASIwg");
	this.shape_73.setTransform(396,247.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#7D1C4A").s().p("AhUH2InYERIBPpWInXkPIgBAAIIujmIgBohIHdFwIHYkRIAAAAIhQJVIHZEPIgBAAIotDmIABIig");
	this.shape_74.setTransform(388.5,240.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#792046").s().p("AhgH+InTD7IBKpOInCkWIgBAAIIkjmIAQoSIHYFnIHUj8IAAABIhLJMIHEEWIgBAAIojDnIgQITg");
	this.shape_75.setTransform(381.1,233.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#762343").s().p("AhrIFInPDlIBFpFImtkdIgBAAIIajnIAgoDIHUFeIHPjmIAAABIhFJEIGvEeIgBAAIoZDmIghIEg");
	this.shape_76.setTransform(373.7,226.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#72273F").s().p("Ah2IMInLDQIBAo8ImYklIgBAAIIQjmIAxn2IHQFWIHKjQIAAABIhAI7IGaElIgBAAIoPDnIgyH1g");
	this.shape_77.setTransform(366.3,218.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#6F2A3C").s().p("AiBIUInGC5IA6o0ImDkrIgBAAIIGjnIBCnnIHLFNIHGi6IAAABIg7IzIGFEsIgBAAIoFDnIhDHng");
	this.shape_78.setTransform(358.9,211.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#6B2E38").s().p("AiNIbInBCkIA1osIlukyIAAAAIH8joIBSnXIHGFEIHCikIAAAAIg1IrIFvEzIgBAAIn8DnIhSHYg");
	this.shape_79.setTransform(351.5,204.5);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#683135").s().p("AiXIjIm+COIAwokIlZk5IgBAAIHzjoIBjnJIHBE7IG9iOIAAABIgvIiIFZE6IAAAAInxDoIhkHJg");
	this.shape_80.setTransform(344.1,197.3);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#643531").s().p("AijIqIm5B4IArobIlElAIgBAAIHpjoIB0m7IG9EzIG5h5IAAABIgrIaIFFFBIgBAAInoDoIh0G7g");
	this.shape_81.setTransform(336.7,190.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#61382E").s().p("AiuIxIm1BjIAmoTIkvlHIgBAAIHfjpICFmsIG4EqIG0hiIAAAAIglISIEwFIIgBAAIneDpIiFGsg");
	this.shape_82.setTransform(329.3,182.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#5D3C2A").s().p("Ai5I5ImwBNIAgoLIkalOIgBAAIHVjpICWmdIGzEhIGwhNIAAABIggIJIEaFQInUDoIiWGdg");
	this.shape_83.setTransform(321.9,175.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#5A3F27").s().p("AjEJAImsA3IAboCIkFlWIAAAAIHLjpIClmOIGvEZIGsg3IgbIBIEGFXIgBAAInKDoIinGPg");
	this.shape_84.setTransform(314.5,168.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#564323").s().p("AjQJIImnAhIAWn6IjwldIAAAAIHBjpIC3mAIGpEQIGoghIgWH5IDxFeIgBAAInADpIi4GAg");
	this.shape_85.setTransform(307.1,161.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#534620").s().p("AjaJPImkAMIARnyIjblkIgBAAIG3jpIDIlxIGlEGIGkgKIgRHwIDbFlIAAAAIm2DpIjJFyg");
	this.shape_86.setTransform(299.7,154.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#4F4A1C").s().p("AjmJXImfgLIAMnpIjGlrIgBAAIGujqIDYliIGgD+IGfALIgLHoIDGFsIAAAAImsDqIjaFig");
	this.shape_87.setTransform(292.3,146.9);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#4C4D19").s().p("AjxJeImaghIAGngIixlyIAAAAIGjjqIDplUIGcD2IGaAgIgGHgICxFzImjDqIjpFUg");
	this.shape_88.setTransform(284.8,139.7);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#485115").s().p("Aj8JmImWg3IABnXIicl6IAAAAIGZjqID6lGIGXDuIGWA2IgBHXICcF6ImZDrIj6FFg");
	this.shape_89.setTransform(277.4,132.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#455412").s().p("AkHJtImShMIgEnPIiHmBIGPjrIELk3IGSDlIGSBMIAEHPICHGCImPDqIkLE2g");
	this.shape_90.setTransform(270,125.3);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#41580E").s().p("AkTJ0ImNhhIgJnIIhymHIGFjsIEbknIGODcIGOBiIAJHGIByGIImFDrIkcEog");
	this.shape_91.setTransform(262.6,118.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#3E5B0B").s().p("AkeJ7ImJh3IgOm/IhdmPIF7jrIEskZIGJDTIGJB4IAPG+IBdGPIAAAAIl8DsIksEZg");
	this.shape_92.setTransform(255.2,110.9);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#3A5F07").s().p("AkpKDImEiOIgVm1IhHmWIFyjsIE9kKIGEDKIGECNIAUG2IBIGXIlyDrIk9ELg");
	this.shape_93.setTransform(247.8,103.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#376204").s().p("Ak0KLImAijIgZmvIgzmdIFojrIFNj8IGADBIGACkIAZGtIAzGeIloDsIlOD8g");
	this.shape_94.setTransform(240.4,96.5);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#336600").s().p("Aq7HZIg8tJIK8naIL3FyIA8NKIq9HZg");
	this.shape_95.setTransform(233,89.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#326304").s().p("AgHM2IgDACInAjXIkoiTIg6s5IAAgBIKvnKIAAAAIADgCIAOAGIAHgEIADABIAFgDIACABIACgBIAIAEIAGgDIACABIABgBIACABIADgDIAGADIADgCIADACIACgCIAOAHIAKgHIAKAFIgDACIgIgEIgHAFIAIAEIADABIAAAAIAHgEIAAAAIADgDIgDgBIAHgFIAJAEIAGgEIADACIAEgCIADABIABAAIABABIABgBILjFpIA6M0IqzHRIgHgDIgGAEIgQgIIgMAIIgIgDIgFADIgEgCIgEACIgRgIIgCACIAAgBIgKAHIgIgFIAAAAIAIgFIgFgCIgIAFIAAAAIAAAAIAFACIgHAFIgFgCIAAAAIgBAAIgBABIgDgCIgGAFgABAM2IACAAIAEgDIgBgBgABbMvIgBAAIgIAFIABABIAIgGIABAAIABABIABgBIgCAAIgBgBIAAABgAAAMxIAHADIAHgEIAAAAIABgBIgFgCIAHgFIgBgBIgIAFIgBAAIACABIAAABIgCgBgABRMqIgMAIIACABIADgDIAIgFIADABIABAAIAAAAIgDgCIAAABIgCgBgAAsMsIAAAAIABABIABAAIAFgEIgCAAIgFADgAAhMnIAAAAIABAAIgBAAgAA3MdIgBABIAHADIgIAFIABABIAJgGIAAAAIgBgBIgHgDgAAXMiIACABIAEACIABAAIAFgDIgFgDIgFAEIgCgBgAAmMjIABABIAKgHgAlspYIlfDsIA6M3IK0FWIAEgCIq1lVIg6s0IAngaIA3glIDCiDICRhiICihsIAwghIgCAAIklDDgAApMWIgBABIACAAIAEADIABgBIAAAAIgEgCIgCgBgArGHNIFrCyID6B6IAFACIpqkugABrL2IAggVIAigXgAJwmCIgFhLIgEgCIAJBNgAKDnMIAAgBIgxgYIAxAZgAg6sYIgBAAIAEACIABAAIABgBIgBgBIgDgBIgBABgAhEsdIACABIABgBIgCgBIgBABgAg6skIACABIgDACIAIAEIADgCIgIgEIACgCIgBgBIgDACgAgtshIADABIgBAAIABABIABgBIAAAAIAAgBIgDgBIgBABgAhSskIAAAAIACACIABgBIAAAAIgCgBIgBAAgAhEstIgBAAIADACIAAgBIABAAIgDgBIAAAAgAgQsyIAGADIAHgEIgGgDIgHAEgAhis0IAFACIACgCIgCgBIgCgBIgDACgAhYs2IAGADIABAAIgGgDIgBAAg");
	this.shape_96.setTransform(234.3,96.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#306009").s().p("AhLMiIgHAEIm4jRIkfiTIg4spIAAgBIKim6IAAAAIAGgEIAbANIANgJIAHADIAKgHIAEADIAEgCIARAIIALgIIAEACIACgCIAEACIAHgEIALAFIAGgEIAHADIAEgCIAcANIATgOIAVAKIANgJIASAIIAMgIIAIAEIAHgFIAGACIACgBIADACIADgCILMFhIA4MeIj/CwImqEaIgOgHIgLAIIghgPIABgBIgEgBIAAAAIADACIgYAQIgNgGIgDgCIgKAHIgIgEIgIAFIgigQIgEACIAAAAIgTANIgQgIIgPAJIgJgEIAAAAIgCgBIgDACIgHgDIgNAIgABFMhIACABIAKgGIgDgBgAgWMYIAAAAIAJAFIAMAFIAPgLIgUgKgABpMMIgQAKIgIAGIAFACIAJgGIAPgKIABAAIgGgDIAAABgAlxJwID0B3IBRAmIgBABIAFACIAIAEIARgLIgNgGIgQAKImajIIBVArgAAbMBIAMAGIAIgGIgLgGgAgOL6IAEACIAIAEIABgBIgIgEIgEgCgAAzLyIALAFIAAAAIACgCIgLgFgAlHpNIlXDlIA4MlIJnE2IAIgFIpqkyIg4sfIAAgBIAmgZIA1gkIC9h+ICOhfICdhoIAGgEIgFgCIjyCfgAAmLqIgBABIAEACIABgBIACgBIgEgCIgCABgAAVLiIADACIgBABIAIAEIADgCIgIgFIgDgBgAIamzIgBgPIgBAAIACAPgAJJnAIgBgCIhGgiIgCgBIBJAlgAg5rmIgDACIAFACIACACIAEgDIgDgBIgFgCIACgCIgRgIIgCACIgFgCIgCABIAFADIACgCIARAIgAg7r4IAXAMIAGgEIAFgEIgCgBIgFgCIACgCIAFADIACgBIgFgDIgRgHIgEgDIgCABIgCACIAFACIAQAIIgGAEIgQgIIgFADgAhrr9IAGADIABgBIgGgDIgBABgAhOsQIgCABIgFAEIgGADIAWALIALgHIgRgIIACgCIARAJIABgBIgRgIIgFgDIgBABgAiQsPIAMAGIABgBIgMgGgAAOsXIgNAJIAFACIgBABIABABIABgBIAMgJIAAAAIAHgFIALAGIAOgJIgMgGIgNAJIgFgDIgHAFgAgusgIARAJIAFgCIgSgJIgEACgAi6sgIASAJIAEgCIgSgJIgEACgAh2shIgFAEIAMAFIAHgEIgMgGIgCABg");
	this.shape_97.setTransform(235.6,104.4);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#2F5D0D").s().p("AiQMNIgJAGImwjLIkWiRIg2saIAAgBIKTmrIABABIAIgGIApATIAVgOIAJAFIAPgKIAHAEIAFgDIAaAMIASgLIAEACIAEgCIAGADIAJgHIAQAIIAKgHIAMAGIAFgEIApATIAAABIgBAAIAeAQIABgBIACgBIAHADIgBACIACABIACgBIARgNIgBgBIAKgHIgHgEIAVgOIAaAMIATgMIAKAGIAMgIIAJAEIACgBIAFACIADgCIK4FYIA3MIIkBCyImhEQIgUgKIgRAMIgxgXIAFgDIgFgCIgEACIgEgCIgCABIAFACIAFACIglAYIgUgKIgDgBIgPAKIgMgGIgLAHIg0gYIgGAEIgBgBIgbAUIgagMIgUANIgOgHIgBABIgDgBIgEADIgLgFIgTANgABXMBIAFACIAMgIIgEgCgAhaLyIgBABIANAGIAagSIktiVIg+gcIj+h0IgBgKIAAgEIgdgOIg2sIIJBmDIgRgJIg9AqQkLC1kJC6IAAACIAnIkIAPDkIgFgDIABANIAbAOIBBAgID8B5ID8B2IAqAUIACgCgAgFLoIAQAIIACgCIALgIIgRgHgAB5LqIAFACIAIADIACgBIADgCIgIgEIgDADIgFgDgAApLbIgMAJIADABIAhgWIgEgCIgUAOgAgXLdIAGACIALgHIgGgDIgLAIgAg0LSIAFACIANAGIANgIIgNgGIgKAHIgFgDgABALMIADABIAEgCIgEgBIgDACgAkipCIlRDeIA3MTIItEeIAjgZIgLgGIgHgDIgFAEIAGADIgSANIolkSIg2sKIAlgZIA0gjIC4h7ICKhbIB0hLIgIgEIi/B8gAAfK/IABABIADgCIgBAAIgDABgAAcq0IAWALQBxA4BwA6ICBBDIB7A/IAHAEIAFBaQAICMAGCKIASEGIAJCJIgEADIAAAIIleD3IiEBWIAOAIQD8irD6iyIgnokIgQjkQihhSihhPIjHhhgAhZrFIgGAEIAIAEIAZANIAHAEIAFADIAHgGIAFgDIgFgCIgIgEIgagMIgIgEIgEADgAgvrgIgFADIgIAFIgIAFIAsAXIAJgGIAAgBIAHgEIgEgDIgIgDIAEgCIACgBIACgCIgTgJIgGgDIgIgEIgCACgAh/rXIgEACIAHAEIAWAMIAKgGIgYgLIgHgEIgEADgAgHrNIAEACIADgCIgEgCIgDACgAhXrzIgEACIAHAEIAZANIAHgFIgZgMIgIgDIgCABgAhqrmIAHAEIAIgGIgHgDIgIAFgAjGr6IAGADIAHAEIgBABIARAJIAFgEIgSgIIgHgEIgHgCIgCABgAgnsMIAaAOIAHgEIgcgNIgFADgAiKr/IAMgHIgSgJIgEADIgHAFIgIgEIgHAFIAIAEIAHgFIARAIgAAAsAIAegUIAfAPIgLAHIgXgLIgTANIgIgEgAA9sFIAAAAg");
	this.shape_98.setTransform(236.9,112);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#2D5A11").s().p("AjTL5IgNAIImojEIkOiRIg0sLIAAgBIKGmbIABABIAMgIIAsAUIAKAFIAcgSIALAGIAUgMIAKAFIAHgFIAjAQIAWgPIAHAEIAFgEIAHAEIANgIIAWAKIANgJIAPAIIAIgGIA2AaIgCABIgBAAIAKAGIABgBIACgCIgKgEIADgCIAmgYIAoATIgOAKIgYAQIgDACIAKAGIAEgDIAXgQIAPgJIgLgGIAcgTIAkARIAZgQIANAHIAQgKIAMAFIADgCIAGAEIAFgEIKhFRIA1LyIkBC0ImWEFIgLgFIgEACIgRgHIgTANIhAgeIAGgEIgHgEIgHAFIAIADIgyAgIgagNIgFgDIgTAOIgRgIIgOAKIhFghIgIAFIAAAAIgmAaIgigQIgcASIgSgJIgCABIgDgCIgGAEIgPgGIgZAQgABOL2IAGADIASgMIgHgDgAjLL0IAJAFIASgMIgKgEgABhLpIAQAIIAQgLIAdgUIADgCIgJgFIgDACIgdAUIgHgEgAhpLlIABAAIgGgDIgNgGgAiDLYIm4jZQBQAqBQAoQBzA5B1A3IAgAQIgDACIAHADIADgCIAJAEgAgYLFIAXALIgDACIAEACIACgCIgDgCIANgJIgWgLgAAOKsIAVALIgPAKIAEACIArgcIgGgCIgUgKgAgzK4IgEADIAIAEIABAAIAEgCIgBgBIANgIIgJgFIgMAJgAhbKqIAHADIAQAIIAEgDIgQgIIgHgDgAgOKdIgOAKIALAGIAogaIgDgBIgMgGIgWARgAAtKYIAVAKIAFACIAEgCIgGgDIgUgJgAj8o3IlKDXIA1MCIHaD3IAPgJIAXgQIAGgEIAMAGIADgCIgNgGIgKgFIADgBImDjNIg1sHIgbAUIAAADIAlIVIAPDdQCjBUCjBRIA0AXIAiAQIgeAWInVjsIg1r2IAkgYIAzghICzh3ICGhYIBCgrIgKgFIiJBYgAAZKOIADABIADgCIgCgBIgEACgAA4qCICtBaIByA9IB/BDIAJAEIAEBYQAHCKAFCHIARD4IAJCFIgHAFIABAKIlUDzIhEAsIASAJQDaiWDZibIgloWIgQjeQichQidhNIiAhAgAhjqZIgLAGIAKAGIAkATIALgIIAJAEIAIAEIAHgGIgHgDIgJgEIgmgSIgKgFIgGAFgAgzq5IgJAGIAKAFIAjARIgNAKIAQAIIAMgKIgFgDIAGgFIAHADIAIgGIgGgDIgKgFIghgRIgLgEIgHAEgAicqsIAIAFIAcAPIAKgHIgegOIgJgEIgHAFgAhgrWIgFAEIgKAGIAKAFIgMAIIAeAQIAegTIghgQIADgCIgKgEIgDACgAjLrLIAEgDIgXgLIgEADIgKgFIgJgEIAFgDIgEgCIgFADIgBABIAVAMIADgCIAXALgAjFr2IgKAHIAKAFIAKgHIAKgHIgLgEIgJAGg");
	this.shape_99.setTransform(238.2,119.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#2C5716").s().p("AkXLlIgQAKImhi+IkEiQIgzr8IAAAAIHqk3ICPhVIABABIAOgKIAsAUIAZAKIAigVIAOAHIAagPIANAGIgNAIIgMAJIAoAVIAOgJIgdgPIANgIIgNgGIAHgFIAsATIAcgSIAIAEIAGgEIAKAEIAQgKIAcAMIAQgLIARALIALgHIBEAfIAOgJIAlgXIAzAYIAigXIAtATIAfgSIARAJIAUgOIAPAHIADgCIAIAEIAGgEIKMFHIAzLdIhgBAIiiB1ImMD8IgOgHIgIAGIgWgJIgTANIhSgkIADgCIgJgEIAGgEIgKgFIgFAEIAJAFIgDACIAJAEIg9AoIgZgOIgOgGIgYAQIgVgJIgSANIhWgpIgJAGIgBgBIgvAgIgsgTIgjAXIgWgMIgCABIgEgDIgHAGIgTgIIgfAVgAh7LXIAeAOIAogcIgbgOgAkNLfIALAFIAVgOIgLgFgABqLQIAUAKIAVgOIgVgKgAiSLMIABAAIADgCIAogZIgCgBIgHAEIgfAWgAmwJBQBwA4BxA2IAIADIAIAEIAVALIANAFIgMgGIAngdIgcgOIgoAdIoikPQCcBRCcBNgAgxKoIAbAMIAGADIAEgDIgGgCIARgMIgcgMgAChKqIAJAEIAMAGIAEgDIAFgEIgLgGIgGAEIgJgEgAgMKQIAdAQIATgMIgHgEIAfgUIAHADIAGgEIgHgEIgGAFIgYgLgAhXKXIANAGIAFgEIACABIAPgKIgCgBIgPAKIgMgGIgGAEgAh/J9IAMAGIARAIIANgJIgRgIIgNgGgAgyJ8IAOAIIASgMIgQgIIgQAMgAAAJWIASAJIgJAGIAEACIAMgIIgEgCIgUgJgAjXosIlDDRIAzLuIGJDRIAQgLImLjJIgzrgIAAAAIAkgYIAxggICuhzICChVIAPgKIgMgGIhTA0gAgPJPIADgCIgQgHIgQgHIAAABIgKAHIAMAGIADABIAKgHIAOAIgABSpOIBkA1IB2BAIBxA8IALAGIADBWQAGCEADCBIAQDxIAJCBIgIAFIABAOIlMDwIAUAKQC4iAC4iFIgkoHIgQjXQiXhQiYhKIg7gegAgypPIgPALIAKAEIAIAEIAQgKIgJgEIgKgFIALgIIgxgXIgKAHIgNgHIgPAKIANAHIAPgKIAwAYgAgWpkIALAGIASgNIgLgFIgSAMgAgoqbIAMAGIAsAWIgMAIIAMAGIAIAEIANgIIgJgEIAHgFIgMgGIgtgVIgMgFIgGADgAirqLIALAFIAjARIAKgFIgkgRIgMgGIgIAGgAiLqhIAvAYIAugbIgqgTIgEADIgJAGIgLAIIgMgGIgPALgAkMq5IgEACIAaAPIABABIAMgJIgdgNIgGAEgAAvrGIAzAcIAFgEIAFADIAFgDIAagVIgDgBIgcATIgNgGIgEAEIgqgUIgCABgAkprHIAGADIgDACIAKAFIAEgCIAFgEIgKgFIgGgCIgGADgADbLBIAAAAg");
	this.shape_100.setTransform(239.5,127.2);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#2A551A").s().p("AlbLRIgTAMImZi5Ij8iPIgxrsIAAAAIHiksICJhQIACABIARgMIArATIAoASIAogaIARAJIAfgTIAPAHIAJgGIA2AXIAhgVIAJAEIAIgEIALAFIAUgNIAhAPIATgNIAVANIANgJIBTAlIAYgQIAkgWIA9AeIgVAPIAPAHIAUgOIgOgIIAqgdIA1AXIAlgWIAUALIAYgQIASAIIAEgCIAKAEIAHgEIJ2E/IAyLGIhgA/IiiB5ImDDyIgRgKIgNAKIgZgKIgVAOIhhgrIhKAvIgZgOIgXgJIgbATIgagLIgVAPIhpgxIgKAHIgCgBIg4AnIg0gXIgqAbIgagPIgDACIgFgDIgIAGIgYgJIgkAYgAlPLJIAOAHIAYgRIgNgFgAB0K3IAYANIAZgRIgZgNgAi7KzIAAAAIAAAAIA1ghIgCgBIgRALIgiAXIgKgFgAjKKrIgPgHIAwgkIgigRIgwAkInyj5QCUBOCUBKQBjAyBmAwIgFAEIALAFIAZALIAEgEIAPAHgAhHKIIAgAPIAIADIAXgQIgIgDIgggOgADTKSIAGgEIgOgHIgLgGIgNAJIALAFIAGgDIAPAGgAADJ5IAIAEIAWgOIgJgEIgVAOgAhvJuIARAIIgHAFIACABIAXgPIgDgBIgUgJIgMALgAifJhIAUAJIAIgGIgUgJIgIAGgAg3JBIAWAMIgUANIAEACIAVgNIgFgCIAfgVIgagMgAAeJHIAcAMIAKAFIAOgJIgLgFIgEADIgagNgAiyogIk7DJIAxLdIFRC3IASgNIlSivIgxrLIAAAAIAigWIAvggICqhvIBYg4IgQgJIgZAQgAAJIwIgHAFIgEADIAGADIAOgJIgIgDIgSgIIARAJgAhJIMIAXAJIATAIIABgBIANgKIglgUgABqoaIAdAQIBwA+IBsA6IANAIIADBUQAFB/ACB9IAPDoIAIB8IgKAGIABARIkEC/IAaANQCXhqCXhvIgjn4IgPjSQiOhKiOhHgAn2ojQilB0ijB8IAAABQANDGAPC+IAQDMIALBuIAXAMIgLgGIgPjRIgjn5IAAgCQDxiuDyimIACgBIgPgHQhQA4hPA7gAhLopIAdAOIgQALIALAFIAGACIAQgJIARgLIgJgEIgPALIgKgFIALgIIAEgDIg/gdIgQgIIgOAJIgTAMIAQAIIATgMIAhARgAAIpEIgWASIAKAFIAYgRIgMgGIAJgGIAMAGIARgMIAKgGIgOgGIgJAGIg3gbIgPAKIgPgIIgMAHIgXAPIAPAIIAYgPIA4AcgAjBpkIAOAHIAkASIAPgIIgogSIgOgHIgLAIgAg1qAIAIgEIgvgWIgQgHIgFAFIgLAHIAPAHIALgHIAtAVgAB6qVIAPAHIgBAAIgHAGIAGADIAIgGIAHgFIAdgXIgDgCIghAXIgPgIIgGAFgAlEqjIgFADIAcAQIAQAKIATgMIgjgQIgQgGIgHAFgAA3qzIA8AiIAHgEIgPgHIgkgRIgOgHIgCABgAlZqtIgDACIAHAEIADgCIAIgFIgHgEIgIAFgAkZrBIAvAaIAhgWIghgRIgCAAIgPAKIgPgIIgPALg");
	this.shape_101.setTransform(240.8,134.7);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#29521F").s().p("AmgK9IgVANImRiyIjziOIgwrcIAAgBIHZkhICFhMIACACIAUgOIAqASIA2AYIAvgdIAUAKIAkgWIASAJIALgIIA+AbIAngZIALAGIAJgGIAMAGIAYgOIAmARIAXgPIAYAPIAPgLIBhAqIAjgWIAjgVIBHAjIAxgiIA+AbIAsgaIAWANIAdgSIAVAJIAFgDIALAGIAJgGIJgE3IAwKwIhmBDIidB3Il5DoIgUgLIgRANIgegMIgVAPIhogtIgLgFIAGgDIgOgHIAJgFIgJgFIgHgDIgIAGIgGAFIAPAHIAOAFIhVA2IgZgOIgggMIgfAWIgfgNIgYARIh6g4IgMAHIgCgBIhBAuIg+gaIgwAeIgegRIgEACIgGgDIgKAHIgcgKIgpAcgACZKKIAMAHIgcATIARAJIAcgUIAbgTIASgOIAIgEIgRgIIgHAFIgOgGgAkGKKIAWAJIAMAGIABABIA+gnIgUgKIg3AqIn5j+QCHBJCIBEQBTArBVAoIAOAGIgHAFIAdANIAFgFIADACgAheJpIAkAQIAKAFIAbgSIgKgFIgTAOIgjgRgAiYJPIAWAKIAFACIAKgGIgGgCIgWgMIgJAIgABFIhIgOAKIgkAZIgXARIAJAFIBPg0IgNgGIgbgLIAZAMgAhgIhIAbAPIAWgPIAIAFIAjgWIgKgFIgigQgAnBlTIAvLLIENCVIAUgOIAUAKIAegVIgVgKIgagMIgbAUIj1h/Igvq2IAAgBIAhgVIAugfIClhqIAcgSIgTgLgAgnH0IAnAPIAJAEIADgCIgKgFIghgSgAhBHdIgNAJIAXAIIAKgHIATgOIgTgKIgUAOgACBnlIA+AkIBqA7IAQAJIACBSQAEB+ABB8IANDXIAIB4IgMAIIACASIi9COIAhAQQB2hUB1hXIginqIgOjLQhng4hng0gAiKoXIAUAKIASgMIgUgJIgSALgABAolIgXAPIALAGIAYgPIAOgKIgNgGIgNAKgAgjpXIgTAMIgNAIIgdASIASAKIAPgKIAPgJIALgHIAUgNIALgHIgSgIIgLAGgAjWo9IgTAOIANAHIAVgOIAPgKIgQgHIgOAKgAh5qAIARAIIAzAZIALgGIg2gYIgSgIIgHAFgABwqLIAqAUIAQAIIgMAIIAIAFIAMgJIAIgHIAggaIgDgCIAYgRIgRgJIgYARIgQAMIgVAPIg9gcIgHAEIgSgIIgCACIAQAJIAEgDIATAJgAmOqRIArAZIAfAUIANgIIgngTIgRgIIgEgCIgNgGIgJgFIgFADgAkKq6IgRAMIgTANIAmAVIAxggIgqgTIgJAFgAlMqxIANAHIASgMIgNgHIgSAMgAEjKUIAAAAg");
	this.shape_102.setTransform(242.1,142.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#274F23").s().p("AnkKpIgYAPImJisIjriOIgurNIAAAAIHRkXICAhGIACABIAXgPIAoARIBHAeIA1ghIAVAMIArgZIAUAKIANgJIBHAfIAsgcIANAGIAKgGIANAHIAbgRIAsATIAagQIAcAQIARgMIBpAtIAHADIgHAFIAUAJIgFAEIAlAXIAYAQIAOgKIAKgHIhIgfIgVgJIAugdIAigUIBRAoIA3gnIBHAeIAygcIAZAPIAigVIAYAKIAGgDIAMAGIAKgGIJLEuIAuKaIhsBHIiYB1IlvDeIgWgNIgWAQIgigNIgXAPIhlgpIgegOIhhA9IgdgRIglgNIgjAYIgjgOIgbAUIiNhAIgNAIIgDgCIhJA0IhHgcIg2AiIgjgUIgEADIgIgFIgKAIIghgLIgvAhgAjnKSIAvAVIA/gsIgogUgACIKEIAgASIAfgXIgSgKIAcgTIAVgPIAUAIIAJgGIgSgJIgPgIgAkhJ3IAFACIAOAHIACABIBJgsIgbgOIg+AyInajwQCEBHCDBCQBDAjBDAgIASAJIgJAGIAgAOIAHgGIAYALgAh0JLIAoARIALAFIAegTIgMgGIgTAOIgmgTgAiuIgIAcAPIAIADIAQgJIgJgEIgggOgAFJolQBrAwBqAzIBNAnIACARIA9AWIAbFNIAkDBIAGBaIlaEaIAkASIBTg0IAggVIAtgdQBGgtBFgtIA9gpIgDhQIgNjLIgamEIilhXIhpg3Ih/hEgAjcIIIgRANIAYAKIAPgLIAMgIIgXgLIgLAHgAh3HzIAsAZIAagQIgxgagABUH8IASgMIgugbIgcAUIAnAPIADgDIAOAHgAioG9IAfAQIgVAPIAVANIAWgQIgWgMIAdgVIglgNgAgQHZIAOAFIALgIIgLgGIgOAJgAhSGlIAVAMIAZgTIgUgMIgaATgACVmvIAAAAIBbA1IASAKIABBPQADB6gBB2IANDPIAHB0IgNAJIACAVIh2BbIAnAUQBUg+BVhAIggnbIgPjFQhBgkhBgigAgkmsIAGACIAFABIgEgCIgFgCIgCABgAgho1IgZAPIAVAMIgLAGIgkAYIBTAuIAjgbIAGgEIAbgUIgbgOIg0gbIAOgJIgWgKIgNAIgAiunfIAbAKIACgCIAPgJIgXgMgAjqoVIAPAHIgYARIAjARIAxgbIgqgUIgQgHIgRANgAiBpjIgSANIgOAKIASAJIAxAZIAngVIg3gbIAKgGIgUgJIgJAGgAmHpiIgMAHIAmAZIAggVIgugUIgUgJIgTgIIgLAHIgGgDIgGgCIgGADIAcARIAIgGIAUAKgADxpqIgbATIAKAEIAjgeIgFgCIgNAJgAlzqbIBNAqIA5gkIgwgXIgKAHIgUAOIgUgKIgPgIIgVAOg");
	this.shape_103.setTransform(243.4,149.9);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#264C27").s().p("AopKWIgaAPImBilIjiiNIgsq9IAAgBIHHkNIB8hAIACABIAagRIAnAQIBWAjIA8gjIAWANIAxgcIAXALIAOgJIBSAhIAvgfIAPAIIALgIIAPAIIAggSIAxAVIAdgTIAeAUIAUgOIBnAqIAXAKIgJAFIgEADIA1AhIAmAaIARgNIAUAKIAOgJIALAFIAlggIgGgDIgVgLIhHglIg2AiIgZgLIA5giIAhgUIBaAuIA/gsIBQAgIA5gfIAbARIAngYIAbAMIAGgEIANAIIAMgIII0EmIAtKFIhzBLIiRByIj8CZIhoA7IgagPIgaAUIgngPIgXAQIhjgoIgygVIhsBDIgcgQIgwgQIglAaIgpgQIgeAWIifhHIgNAIIgEgCIhTA7IhPggIg+AmIgmgWIgFADIgIgFIgNAKIglgNIg0AkgAC0JSIAQAIIgjAZIAVALIAigZIgUgLIAPgKIAogbIANgJIATAJIAKgHIgUgJIgTgIgAlIJeIgBABIANAFIAIAEIBTgxIgigSQgiAegjAbgAE7JRIAXAJIAKgGIgYgLIgJAIgAoEIDIBlAyIgMAJIAWAKIAiAPIALgJImUjPQB8BFB8A/gAhfI/IAOAGIALgIIgMgGIgNAIgAjfIHIAjAPIASgLIgPgIIgVgLgAF7oCQBOAkBMAlIBMAlIADAWIBEAXIAaE5IAnC6IAFBYIkxD/IAuAYIAugdIAggUIArgcQBEgrBDgsIA7goIgEhNIgKjFIgal3IifhVIhlg2IhdgzgAkAHfIgVAPIAbAMIAUgOIAMgIIgZgOIgNAJgAiOHCIAsAZIAagRIgxgagAALHCIAxAQIAMgJIgqgWgAlolLIAsKnIBjA5IASgOIhcgxIgsqMIAAgBIAfgUIAsgcIBGgtIgegNgAhfFqIgQAMIADAYIADABIAdgXIAqggIgBgbIAvgkIgdnHIgNi5IASgLIgCgKIAAgCIgMgFIgNAKIg5AtIACALIA/gvIABAJIgvAcIAqKIIg8AtgAB0mYIAKAFIAgAQIAPAIIAIAEIABAIIAUAMIABBNQACB3gCBxIAMDEIAGBwIgOAKIACAYIgyAmIAuAYIBohPIgenMIgOi/Ig4gfIgNgIIgogWgAi6m2IAfAMQAeAKAdAIIgvgaIAbgRIgagOgAh2ngIB6BGIArghIAPAHIgMAIIAOgHIACgBIgCgBIgOgIIAigZIAPAHIAIAFIAcgQIgNgFIgQgIIhggsIgXgKIgCgBIgSALIAZAMIggAUIAkAVIA2AhIgDACIhhgyIgZgNgAkInUIAhAOIAZgOIgigRIgYARgAhYoQIAegPIg6gdIANgJIgWgJIgLAIIAUAKIgXAQIgTgLIgPAMIAUAJIAOgKIAzAcgADZojIAMAIIAVgQIgNgGIgUAOgAnqphIgHAFIAgAUIALgIIgWgLIANgJIgNgFIgOAIgAlEqQIgXAPIgZARIAwAbIBBgqIg2gZIgLAIgAmbqGIASAKIAYgRIgRgIIgZAPgAgjE9g");
	this.shape_104.setTransform(244.7,157.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#24492C").s().p("ApuKCIgcARIl6ifIjYiMIgrquIAAgBIG/kCIB2g7IADACIAcgUIAnAPIBlApIBBgmIAZAPIA3gfIAaAMIAPgLIBcAlIA1giIAPAIIANgIIAQAJIAjgVIA4AXIAfgUIAiAWIAVgQIBlAoIApARIgLAHIgFACIAXAQIAJgGIBaArIAPgLIAXAKIAOAGIAlgiIgGgDIgXgNIgtAiIhdgnIgcgMIBDgoIAggTQAzAZAyAbIBFgyIBZAjIA/ghIAeASIArgaIAeANIAHgEIAQAIIAMgIIIfEeIArJuIh0BLIiRB1Ij2CTIhkA3IgegSIgdAYIgsgQIgYARIhggmIhFgdIh4BJIgegSIg3gRIgpAdIgugSIghAYIixhOIgOAIIgFgCIgIAGIhUA8IhXgjIhFApIgpgZIgHAEIgKgGIgNALIgrgNIg4AngAF9I5IAUAKIAgAPIBiAsIATgNIigg9gAkvJkIA5AaQAogbAngcIgvgYgACdJQIARAKIAlgbIAXAMIA6gqIAPgLIAJgIIgVgJIgJAGIgRAMIgVgJgAl0JDIgBABIAYAMIBfg3IgsgXQgkAigmAfgAmbIzIADgDIlri6QB1BCB1A8IBCAhIgOAMIAaALIAPgJIAhAQgAhjIYIgPALIAPAGIAjgXIgRgHIgSANgAkFHiIAsAUIANAFIAWgNIgMgHIAIgGIAGgEIgvgYgAhCHKIApAZIAcgVIgsgYgAGsneIBiAuIBJAkIADAaIBMAXIAZEmIApCzIAFBWIkJDkIA5AeIAJgFIAfgUIArgbQBBgpBBgrIA5gmIgDhKIgKi/IgZlrIiYhTIhig0Ig8gigAlkG2IAnASIAdAOIAZgTIgbgOIAPgMIgngWgAAZGBIAwAaIgaATIAUAGIAVgQIAfgYIg9glgAilGQIA2AgIAbgSIg9gggAhGkVIAAACIAeG8IAIB0IgVASIAKBUIAigWIAxghIgVlJIgNg4IgWkKIg2AqgAk9lHIArKWIAZAPIASgPIgOgIIgrp3IAAAAIAegUIAqgbIAOgIIgkgPgAC2k/IAqJoIAjgcIgdm9IgMimgAB8lsIAAAGIAwgcIgmgVIgQgJIgCgCIAcgVIgQgIIhugzIgXAPIgnAYIgIAEIgdgOIgyAeIAcARIAmgYIANgJIBgAzIARAIIAIAEIgCACIAGADIANAIIAEgCIAiARgAiemDIACgBIgIgCIAGADgAkrmyIATAGIAUgPIgQgJIgXASgAikoAIAxAZIAPgHIgxgcIgPAKgADkoLIASALIAPAJIAYgTIgPgHIgVgLIgVARgAnsocIAtAeIAagRIg1gZIgSAMgAiQooIAWAKIAPgKIgXgLIgOALgAnxo0IARgMIgagKIgRAKIgQgHIgIAFIAOAJIAKgHIAaAMgAl7prIgdAUIA0AeIA6glIg4geIgZARg");
	this.shape_105.setTransform(246,165);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#234630").s().p("AqzJvIgfARIlxiYIjRiMIgoqeIG2j5IByg2IADACIAggVIAlAPIB0AuIBIgpIAaAPIA+giIAcAOIARgMIBhAmIAEACIA5gkIARAJIAPgJIARAJIAngXIA9AaIAjgWIAlAYIAYgSIBhAmIA6AXIBNgtIAhgSQA4AcA2AeIBNg4IBhAmIABAAIBEgjIAgAUIAxgdIAhAOIAIgEIAQAJIAOgJIIJEVIAqJYIh5BOIiMB0IjxCNIhhAzIgggUIghAcIgwgSIgZARIhegjIhYgkIiEBOIgegSIhBgTIgrAeIg0gTIgjAaIjFhVIgOAIIgFgCIgUAPIhRA5IhdgjIgEgCIhKAsIgugcIgIAFIgLgGIgOAMIgwgOIg9AqgAG0IpIAvAWIBgAqIASgNIiXg4gAlSJOIA9AbQAsgdAsgfIgygagACnI1IArAaIAWgPIAUgPIgZgNIA8grIAWgPIAHgFIgWgJIgCgCgAliHvQgfAeggAbIgCABIAeAOIBqg8Ig1gcgAFtIaIAhAOIAPgJIgigRIgOAMgAouHkIAeAQIAhAQIgSANIAmAQIARgPIlBimQBuA/BvA5gAiGIGIARAHIATgMIgRgIIgTANgAkqG9IAzAYIARAHIAtgbIgRgJIgRAKIg1gcgAhPGeIAxAeIAdgWIg0gdgAHdm8IAoAUIBHAjIAFAfIBTAYIAYESIArCsIAFBUIjhDIIBGAlIABAAIAogaQBAgoA/gpIA3glIgDhIIgJi5IgYleIiShRIhdgzIgdgQgAAmE/IgWARIAGAtIATAJIARAJIAigaIgQgJIAngeIgamoIgDAzIAKCuIAFBnIgRAMIgHhuIAEhfIAFhUIgSkcIgBgRIhIA8IAkI1IAEgDIADAlgAioFKIBIAoIBEgsQgLgwgNg0gAjSlnIAQAJIABAVIgTAMIgdATIAAABIAoJUIgeAXIAaAPIAUgQIBAg1IgLisQgKidgLiTQgFhFgKhDIgCgNIgdgJgAB+l3IAGAEIgxAkIAGAEIA4ghIgNgHIAZgTIgfAPgAgHmIIBHAtIADgBIAZgNIAZgTQg9gjg8ggIAegSIghgQIhIAsIAdASIgEADIghgRIg7AjIAhAUIA7gmIAvAYgAkpmZIAQAJIAVAMIAlgVIgbgOIgSgJIgdAXgAjem7IAeASIA/geIgtgXIgwAjgAiAn/IghAXIAtAcIBMgjIg0gXIgRgHIgTAOgAEZn8IgZATIATAMIARAKIAcgWIgRgIIgWgLIASgPIAvgjIhXgvIg+AmIgCABIgPAIIgegOIgGAEIAZAQIALgGQAzAYAyAagAoboXIgRANIATAOQAYARAYAQIA4glIg8gaIgbgMIgTAPgAnUpMIBQAvIBUg2IhDgdIgNAJIgbASIgBgBIgagMIgeAWgApLouIgLAGIAQALIANgIIATgNIgRgIIgUAMg");
	this.shape_106.setTransform(247.3,172.6);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#224334").s().p("Ar4JcIggASIlqiSIjIiLIgnqPIGujuIBtgxIAEADIAigYIAkANICDA0IBPgrIAbAQIBEgkIAcAOIADABIASgNIBfAjIARAHIA9gmIASAJIAQgJIATAKIArgZIBDAbIAmgXIAnAcIAbgVIBeAjIBMAeIgQAJIAhAPIASgKIgjgOIBYgzIAfgRQA9AfA7AhIAIgFQAmgdAmgbIBfAjIAMAFIBLgmIAiAWIA2gfIAlAPIAHgEIASAJIAQgJIHzEMIAoJCIh9BQIiJB0IjrCIIhcAvIgkgYIglAhIg1gTIgaASIhagiIhsgrIiQBUIgggUIhKgUIgtAhIg4gVIgmAcIjZhcIgOAIIgGgDIgfAXQgmAdgoAcIhagiIgRgGIhQAvIgxgfIgKAGIgLgIIgQAOIg2gPIhBAugAl2I4IBDAdQAsgdArggIAIgFIg0gcgACzIaIAuAeIANgKIAfgXIgxgbgAoAH3IAwAXIAkARIB2hBIgRgJIgYAQIg5AkIgbARIhIgmQAsgeArghIgogNIhRA7Ijvh9QBiA5BiAzIgVAUIAiAOIADABIAWgPIAiARgAGWHwIgRAPIAmAQIAkgVIgvgUIgKAKgAjQHSIA2AYIATAIIAYgPIgTgKIARgMQgbgOgbgPgAFMHJIgaATIAZALIAXgTIgWgLIAFgEIgcgIIAXAMgAHyGZIA9AnIATgLIAcgSIAWgNIhPgrgAkwF7IA/AjIgiAWIAUAJIAigUIgUgLIAVgOQgggTgfgVgAhbFxIA3AiIAegWIAtgjIgShIgAiEFXIAXAOIAdgRIgagOIgaARgABpE7IBQhEIghoRIgFgnIgYASIgBgTIhWBJIAAAEIgBAnQAKCYAKCLQAIBaAJBTIAIA4IAYgTIABAUgAimk1IgcARIAAABIAkIYIBFhAIgEhSIguilIgWj2IgFADgAg4l4IBCArIAbARIgBAfIBVguIgBgHQg+gmg9giIAQgLIATgMIgjgQIgiAUIAiATIg2AhIglgTIhEAoIAkAWIBFgrIABABgAkBmeIASALIA3gpIgWgKIgzAogAiGngIgnAcIAeASIAJAGIBcgnIhEgeIgYARgAosntIgZASQAZASAaAQIBAgpIhBgeIgZATgAFQnuIgXASIgUAQIgIAGIAUAMIAfgWIAagTIAwgiIgagOIgwAlgApGn7IAXgRIgigOIgWAPIgVgKIgNAHIASANIAQgKIAhAQgAHEoiIgBAAIAxAdIBlg6IhCgdQgqAcgpAegAl3odIAxgfIhJggIgPAKIgegPIgcAUIgeAXIgDACIAaAQIAkgaIBEAhg");
	this.shape_107.setTransform(248.5,180.1);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#204039").s().p("As9JJIgiASIliiLIi/iLIgmqAIGljiIBpgsIAEACIAkgZIAkANQBHAaBLAdIBVgtIAcASIBLgnIAcANIAGADIgeAWIgYASIgNAJIBaA4IAygfIhHgkIAegVIgggRIAUgOIBbAhIAfAMIBBgqIATALIASgLIAUALIAvgZQAlANAjAQIApgaIAqAfIAegXIBcAhIBdAkIBig4IAegPQA9AeA6AhIALAGIASgNIBKg2IBbAhIAYAJIBRgoIAjAYIA9giIAnARIAIgGIAUALIARgLIHeEFIAmIsIiFBVIiCBxIjlCBIhYArIgngaIgqAlIg6gUIgaASIhXgfIiAgyIibBZIghgVQgtgKgngLIgvAiIg+gWIgoAeIjrhjIgOAIIgIgEIgqAgIhMA3IhXgfIgcgLIhXAyIg0giIgMAHIgMgIIgRAPIg9gPIgEAEIhAAtgAmaIhIBIAfQArgdApgdIATgOIg1gdgAC+H/IAxAgIAFgDIAqggIBAgzIgagLIhBAvIgZgPgAncIBIAagRIA2giIAggVIhBgUIgmAlIgoAnIgwgaQArgfApghIgpgOIhQA7IjChmQBJArBJAnIAoAVIAEACIAlATIAxAZIADgCgAiuHNIAVAJIAdgSIACABIAAAAIALgJIgNAIIgWgLIgcAUgAFgGoIAJACIgHgDIgCABgAIWFuIBJAwIAMgIIAmgXIAWgOIheg0gAkHF4IAXANIAagPIgXgPIgaARgAhnFBIBRA0IAcgYIBPhDIgFgSIhcBLIhDgngAloE+QAoghAngjIgKijQgJiWgLiJQgEg4gIg2IgEgVIgcAZIhQBGIAAABQAKCoAMCZQAIBWAJBRIAHAtIAagVIADApgAi3k7IAlJgIAhAVIAPgJIglpRIgEgCIATgNIAngXIAPgJIATAMIgBgJIgNgGIA5gkIgmgWIg9AkIgBAAgACIj7IAAACIAaGRIAIBxIBLgxIgjoOIhKA7gAkSl2IAkAXIBTgsIgegVIgKAHIgVgMIg6AvgADPnzIAPAMQAuAnAvAdIAUANIAigaIgXgMIgVgLQgzgbg1gZIgOAIgAqun9IAXAMIgSAMIAiAbIAUAPQAbATAbAQIBIguIhEgfIgdAVIgagOIgngTIAbgRIgXgKIgbAPgAilnOIANAHIALAGIAyAaIAsgSIhBgdIgdgMIgYAUgAH4oRIgMAIIAyAfIByhAIhIgfQgoAcgoAcg");
	this.shape_108.setTransform(249.8,187.7);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#1F3D3D").s().p("AuEI2IgjATIlZiGIi3iJIgjpwIGcjZIBjgnIAEADIAogbIAjALQBOAcBTAhIBbgvIAeATIAAAAIBSgqIAaAMIAKAFIAVgPIBZAfIAsAQIBFgrIAUALIATgLIAVAMIA0gcQAoAPAmAQIAsgbIArAiIAigaIBZAfIBuAqIgWAMIAnASIgRAJIANAKQA0AxA3AfIAigbIg4geQgogVgpgVIAagOIAlAPQA1AWA1AXIAbAMIAwgkIgcgQIhwg/Ig2AfIgYAMIgrgQIBtg8IAdgPQA8AdA4AhIAYAOIAbgVIBIg1IBYAfIAlANIBXgpIAkAaIBDglIArARIAIgFIAUALIATgLIHID8IAkIWIiGBVIiBBzIjfB8IhUAmIgqgdIguAqIg/gWIgaATIhVgdQhIgahMgeIimBeIgigXQgzgKgrgLIgxAjIhEgXIgpAgIkBhqIgMAIIgJgFIg1AoIhJA2IhVgdIgogPIheA1Ig2glIgOAHIgOgJIgSAQIhFgOIgIAHIg9AsgAB4IhIAZAKIAngaIgfgKgADJHjIA1AkIAxgmIg4gggAoMG2IgdAeIgDADIAxAYICDhHIgdgIIhJgXgAq5GMIAzAcIgfAWIAqASIAcgTIAEgDIi1hhIBXAzgAHJGwIALAFIAtAZIATgLIhDgagAHmGTIA6ApIA9gjIAagQIAXgOIhUg7gAFdF0IAcAQIgLAJIAeAHIAFgDIAmgfIgtghgAlRD+IADA0IAzAgIAugeQgFhEgEhMgAhplBIAAAJQAKCPAHCMIAHCLIAEBcIgWAPIACAlIgIAHIABASIBNAzIAbgYIgSgLIBOhBIgamCIgLihIgegTIgpgaIg8giIhDAjIAUAOIAkAZIAGgDIAIAEgAn/k2IAhJcIBMg/IgZl9IgLinIgFgDQgUgDgTgFgADNjxIAAACIAZGBIAEBBIALgHIgenFIgKAIgAjjmCIhAA2IASAMIBEg2IAGgEIgUgOIgIAGgAFulzIARAJQAUgLATgOIgUgLIgkAbgAqanAIgUASIgGAFIAVAPIAggYIAhgYIgfgOIgdAYgAodoiIgRANIgWAQIBeA9IA4gjIhMgnIAggXIgigSIghAZgAItn/IgYARIAyAfIB/hFIhMghQgoAaglAcgADynjIAAAAg");
	this.shape_109.setTransform(251.1,195.3);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#1D3A42").s().p("AvEImIgFgDIglATIlSh+IitiJIgiphIGTjOIBfgiIAEADIArgdIAhALQBSAcBWAhIAIADIBigwIAeAUIAKgGIBQgnIAZAMIAOAHIAWgQIBWAcIA6AVIBJgtIAVAMIAVgMIAVAMIA4geQAsAQApARIAugcIAuAlIAlgcIBWAcQA/AWBBAZIgaAOIAqAVIA7AfQAtAYAsAZIAYAOIApggIgbgNQhHgghLgeIgKgEIgugSIB3hAIAcgOQA6AcA2AgIAmAWIAlgcQAigaAjgZIBWAcIAxASIAxgbIArgRIAmAcIBIgnIAvASIAIgFIAWAMIAVgMIGyDzIAiIBIiMBYIh8BxIjZB2IhQAjIguggIgxAuIhEgXIgbATIhSgaQhRgdhWgiIiyBiIgkgYIAAAAQg5gJgvgLIgxAjIhLgYIgrAhIkUhvIgMAHIgLgGQgfAZggAYQgjAbgjAZIhSgaIg1gUIhkA3Ig4gnIgQAIIgPgKIgUARQgpgGgkgIIgMALIg7ArgAGFHvIAaAPIBkg4Ig8gaQggAlgiAegADUHHIAbASIAzgkIgegSgAqCGmIAoAVIA5AdICEhIIghgJIhSgaIg1A3IgSAUIjBhoIAlAXIA6AhIgkAbIAuAUIAggYIANAHgAIGFsIBEAyIAggSIAagQIAjgVIAagPIhjhGgAixF2IAYAOIAZgTIgXgPIgaAUgAFIFuIAtAIIAcgVIgegTgAjuldIhGA+IASAMIg0ArIAAACIAPDkIAVA6IAEBNIgPAQIAJCSIAHAEIBFgtIggoBIgagQIBMg+IAIAFIADABIAHAEIBMgmIghgUIg8AvIgUgPIgFAEgAhfkyIAiI7IAdAUIAagXIA7g4QgJiggMiNQgHhSgKhMIgJg4IgIgwgAESjoIAEA9IAAgGQABgggCgZIgDACgAHtk0QARAEAQACIAAgDIgRgMgAomk0QAQAEAPACIAAgEIgUgNIgLALgAsUnMIAfAQIgYAPQAVAUAWARIAdgYIACgBIAggcIgwgVIgdgNIgkATgAo/oNIgMAJIgfAYIBiBBIA/gnIhRgpIAigYIgkgUIgjAagAHwnBIAcARIAygkIgfgUIgvAng");
	this.shape_110.setTransform(252.4,202.8);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#1C3746").s().p("Av9IYIgPgIIgoAUIlLh4IikiIIghpSIGLjEIBagcIAEAEIAuggIAgAKQBPAaBUAfIAcALIBpgxIAeAVIAUgLIBNglIAZAMIARAJIgkAbIgHAFIgpAgIBmBFIBFgrIhVgsIAkgZQAiATAgAVIAOAJIAngYIgOgFQgqgTgpgQIgWAPIgmgVIAYgSIBTAaIBIAZIBLguIAYAMIABAAIAVgMIAWAMQAegQAegPQAvAQArASIAygeIAwApIApgfIBTAaQBGAYBKAcICChEIAbgNQA5AcAzAfIA0AfIAvglQAggaAjgYIBSAaIA9AVIA6geIApgPIAmAeIBPgqIAyATIAIgEIAYANIAWgNIGcDrIAhHqIiRBaIh4BxIjSBwIhNAfIgxgkIg1A0IhJgZIgbAUQgngLgpgOQhVgdhdgjIgIgDIAigTIgjgTIASgKIg6gVIgYgEIA+AhIACACIgnAWIAoAQIi+BmIglgbIgBABQg/gIgzgMIgBAAIgwAkQgogLgogOIgBAAIgtAjIkqh1IgJAGIgOgIQgkAeglAcQghAbgiAYQgngLgpgOIhBgWIhpA5Ig8grIgSAJIgQgLIgVATQgugFgpgJIgNAPIg7AqgArAGAIApAWIAOAIQAhASAiARICDhIIgkgKIgrAaIgwAeIgVAOIjEhqIgSgMIgnApIAXAMIAqATIAogfIArAYgAjAFTIgsAjIAdANIAoggIAdgZIgVgSIghAbgAInFCIBPA9IACgBIAYgPIAigUIBCgnIhxhUgAkaEbIAJAFIA2gyIgIh4IALhLIALhAQgGiGgJiIIAAgIIgDgBIA/gzIgagPIg8A1IAXANIgEADIgVgPQgoAkgoAnIAdHjIAPgKIACAvgAA0ktIgiAUIgXAPIAAABIAfH5IADABIBhg8IggnqIgPgJIgbARgALrkhIAhHrIA+AjIAAgsIgHiYIgSkhIhFgqgADinDIAqAjQAxAzA0AfQATANAVAIIAhgdIAYAPIAvglIgcgOIgrAkQg9glg9ghIgngVIgtgWIgKAEgAtImzIAjASIgbAQQAYAYAZASIAYAQIAlgcIgbgQIgrgXIgNgHIAngZIghgPIgpAWgAIXmkIAcARIAzgmIAeAVIAJAGIAKAEICahQIgBAAQgrgTgogQQgkAXgjAaIgwAjIgfgVIgwAqg");
	this.shape_111.setTransform(253.7,210.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#1A344A").s().p("Aw3IKIgYgOIgtAVIlChyIiciHIgfpCIGDi6IBUgWIAGAEIAwgiIAfAJQBMAYBSAeIAvASIBxgyIAeAWIAdgQIBMgiIAXALIAVAKIAZgSIBQAXIBXAdIBPgvIAXANIAYgNIAXANQAggSAhgQQAyARAtATIA1gfIAyAtIAtgiIBQAXQBOAaBTAfICNhIIAbgLQA2AaAyAfQAhATAfAWQgZAVgYAXIAdASIA0goIgggWQAcgZAdgWQAggZAggXQAoAKAoANIBKAZIBCghIAmgNIAmAgIBXgtIA1AUIAJgEIAYANIAYgNIGHDiIAfHUIiWBcIhzByIjNBqIhJAbIg0gpIg5A6IhOgaIgbAUQgngKgngMQhSgbhaghIghgOIAcgOIANgGIAZgNIgIgEIgmgNIAIgFIAXgNIAFgDIhbhKIgwA2IAnAXIAvAbIguAYIArARIjKBqIgngcIgCABQg8gFgygLIgOgEQgYATgaASQglgKgngMIgLgEIguAkIk8h5IgEgBIgGAFIgLgHIgIgBQgnAjgpAeQgfAbgiAXQglgKgogMIhNgaIhwA6Ig9gtIgVAJIgRgLIgWAUQg0gEgtgJIgQASIg4AogAneGkIhIAlIAOAHIBMAeQAkgYAigaIA+gtIgLgGQgrgJgvgMgAEMGkIAgAYQAbgWAagYIgfgTIg2ApgAq3F/IgCABIANAHQAgASAiARIADgCIB/hFIgigLIgDgEIgxAeIguAcIgVAOIg1gegAttEZIBRAtIgvAlIAyAXIAogiIACgBIgtgZQgmgYgmgagAkAFYIAdAPIAugnIgagQIgxAogAkUj+IACAeIgMAKIAAACIAVFVIADApIAGBlIAxAhIBEg6QgFg+gDhHIgVmbIAQgHIgQgKIhJA+IACACQgBAfgDAeQgGBagPBSIgKjOIAhgdIgUgQIgPAPgAGcjWIAAACIAVFVIAKCOIAHAFIAXAQQAqgnAqgqIganaQgNgBgOgDIgUgFIgFgBIhDA7gABhklIghAUIgGADIgSAKIAAABIAfHUIBAgoIgfnTIgHAFgAM4C5IAXAMIAAgJIgGiRIgRkVIgcgRgAHfkRIAYgJIAAgBIgHgFIgRAPgAkVkZIgBgHIgbgPIAcAWgAE+mNQBOAqBNAzIAwgqIg0gaQg+gfhCgaIgpAWIgxgaIgJAFIAkAhIAUgKIACgCIASAKgAqCniIgBAAQgZAUgZAVIBnBIIBNgvIhZguIAlgbQAdARAcATIAbASIAsgbIgXgKQgpgSgngPIgZAQIgngXIgmAegAt9mcIAeAQIAJAGIAsgcIglgQIguAWgAJqGnIAAAAg");
	this.shape_112.setTransform(255,218);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#19324F").s().p("AxwH7IghgTIgyAXIk6hsIiUiGIgdoyIF5iwIBRgSIAGAFQAZgTAZgRIAfAIQBKAWBPAcQAhAMAgANIB4gyIAgAWIAngVIBJggIAXALIAXANIAbgUQAmAJAnAMQAxAPA0ASIBSgyIAZAPIAagPIAXAOQAigSAkgQQA1ARAwAUIA4ghIAyAxIAyglQAmAJAnAMQBQAZBXAfIALAFQBSgnBGgkIAagLQA0AaAwAeQAoAYAlAcQAigeAigbQAfgZAfgWQAlAJAoAMQAqANArAOIBJgkIAlgJIAnAhIBdgwIA5AWIAJgGIAZAPIAagPIFxDaIAdG/IiaBeIhvBxIjHBkIhFAYIg4guIg9BBIhTgbIgbAUQglgJglgLQhRgZhWggIg6gWIAJgEIAngTIAagNIgIgDIgtgPIgdgKIgRgBIAgASIg0AcIAtATIjWBuIgpgfIgCACQg7gDgwgJIgcgIIgxAmQglgJglgLIgVgHIguAmIk/h4QgcgCgegFQgnAkgpAgIg/AwQgkgJglgLIhagdIh1A8Ig/gxIgZALIgSgNIgXAWQg6gDgwgJIgBgBIgRAVIg2AngAoQGXIg5AdIAVAIIBJAdQAjgWAhgaIBCgxQg1gKg8gRgAt9jmIgbAZIAQD1IgXBIIAGBrIgOAQIACALICRBRIAtAbIAmAVQAYAOAaANICLhKIgQgjIgQglIhKgsIh4CNIgugaIgOgJIgrgcQA3gvA4gzIgUlDIgHh3IgCgBQgXgBgbgKQgQgEgOgIIgyAsgAD5FuIAiAZIA2gpIgmgYgAkpjIIgCATQgDAkgEAhQAGBrAJBdQAGBLALBDIANBFIAogiIAZARQAsgpArgtIgFhZIgUmQIAAgBIgDgSIgoAiIgYgRQgxAtgvAygAHfjuIgyA1IAAAAQAHCUALB9QAHBLALBDIABALIAogkIAPgOIgUlHIgGhSIgCgXIgOADgALthpIAyCNIAJCbIAsAkIAAgsQgBhCgEhIIgPkKIhcg5gAHskRIABAVIAjgjIAHAhQA0guA0grIgdgTQgpAkgpAnIgKgxIg5gdQgvgWgxgUIg7gXIgnATIgKAEIAmAkIAWgLQBYAvBXA+gAlPkQIAAgFIgTgKIATAPgAJxkvIAqgVIgFgEIgIgEQgNAPgQAOgAtDlBIAbARIAwgnIgegSQgggRgggQIgyAeIgggRIgLgHIgUAKIALALIAVAXIAfgUQAjAUAiAXgApTnUQAYAPAWAQIAnAaIAygfIghgNIhMgfIgaASgAsDm9IAoAdQAbgXAcgWIgDgCIgjgSIg5Akg");
	this.shape_113.setTransform(256.3,225.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#172F53").s().p("AyqHtIgpgZIg3AYIkzhlIiKiFIgbojIFwilIBMgNIAGAFQAagUAcgSIAdAHQBGAUBOAbQAqAOAoAQICCgxIAeAWIAzgZIBHgdIAVAKIAbAPIAdgVQAkAHAmALQA4AQA7AUIBVgyIAaAQIAcgQIAXAPQAkgUAngRQA4ARAyAXIA7gjIA0A1QAbgVAbgTQAlAHAlALQBOAXBTAdIAiANIgXAKIgYAMQgzgbg3gWIhIgdQggAVgeAYIgDACIA6A7ICsgWIANgGIAkAUIATALIAkgRIATgLIg/gZICjhNIAagLQAyAZAuAeQAuAdAsAiQAmglAogfQAdgYAegVQAkAHAmALQAwAOAyAQIBRgmIAjgHIAmAiIBlgyIA9AWIAJgFIAaAQIAbgQIFcDSIAcGpIigBfIhrByIjBBeIhBATIg7gyIhABHIhagcIgbAUIhHgSQhNgXhUgeQgpgOgogQIA2gbIgsgaIAJgFIgMgEIgOgBIARAKIgCABIgWAMIgjASIAxAWIgMAFIjYBrIgqghIgDADQg5gBgvgHIgqgKIgxAlIhHgSIgfgJIgvAmIlAh0QgjgCgngGIgPgCQgmAlgpAgQgdAYgfAWIhHgSQgygOg0gSIh8A9Ig/gzIgdAMIgTgPIgZAYQg3gBgvgHIgPgEIgTAYIg0AmgAsLFJIA1AeIAjAUICphYQgKgVgIgWIgCgEIh8hLIh8CZQgegSgegUIgsgfQAngiAmglIgTk1IgHhoIASACQARAEASAAQgGgqgJgoIgvAoIgdAbIgwAtIAUE5IAHCBIAIAGIgIAHIgtgaIAMA3IAhgdgAkdivIAAABQAHCPALB4QAHBJAKBAIACALIAHAkIAlglQAjgjAignQgHiPgKh4QgHhIgLhBIgCgMIgqAnIgDgmQgiAkgiAmgAmJkHIAAgDIgLgHIALAKgAu6lQQAmAXAmAbIAvgsQghgVgkgSIg2AhgAKmk3IABABIAOAJIAFgHQAUgXAMgWIg0AqgAssmnICTBxIC2huIgpgRIhJgdIgdAUIgrgaIgeAYIgLAIIgHgEIgigQIg9Alg");
	this.shape_114.setTransform(257.6,233.1);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#162C57").s().p("AzjHfIgygfIg8AaIkqhfIiDiFIgZoTIFoibIBHgHIAGAGQAbgWAegTIAcAGQBEASBLAZQAzARAwAUIAKgFICCgtIAdAYIA+geIBEgbIAVAKIAeARIAfgXQAiAGAlAKQA+ARBEAWIBXgyIAbARIAegRIAYAPQAmgVApgSQA8ASAzAYIA/gkIA1A6IACgBQAcgXAdgVQAiAGAlAKQBKAUBRAcIA3AUIgEACIjOBjIAvAzIAAgBIAUgMIAdgQIBgg1IABACQAnA1ArAbQAVAQAbAIIANADIgDgmQg6gug8glIA4gaIAGgDQghgPgigOQBggqBQglIAYgJQAwAXAsAeQAwAeAsAmIAMAHQAqgsAugkQAcgXAdgVQAiAGAlAKQA1AOA5ATIBZgpIAggFIAmAjIBtgzIBBAWIAIgEIAbARIAegRIFGDJIAZGTIikBhIhnByIi7BYIg8APIhAg4IhEBOIhfgcIgaAUQgigHgjgJQhKgUhRgcQg1gTgygVIA8gdIgvgeIAAAAIACgBIgEAAIACABIgdARIgUALIgQAHIA0AYIgiAPIjRBlIgrgkIgFAEQg2ACgugHIg4gMIgwAlQgigHgjgJIgpgLIgvAnIlChyQggAAgjgEQgagDgcgEQglAkgnAgQgcAXgeAWQgigHgjgJQg3gPg7gTIiBA+IhAg2IgiAMIgUgRIgZAbQg2ACgugHIgdgGIgTAbIgzAlgArrFYIAUAMICnhVIgDg3IiGhSIgBgZIiNC4IBKAqIABAAIARAJgAEUEuIAmAgIA2gtIgqgdgAu/DlQAGAgAIAeIAnglIAIgIIgOgLIgDgwIAPgPIgSkmIgEhPIgPAOIASErIAEBLIgpAmIgEgCIABAGgAMODeIAhAfIAJgGIAmgZIACgwIgEh+IgOjwIhXg7gAnGAqIgQA7IADAyIAfgVIgBhDIgChGQgHAZgIAYgAi5BwIACAeQgFh2gIhkgAuajJIAJgJIgKgCIABALgAJfjZIAvgEQAfgeAggcIgVgOIgJgHQgpAngnAsgAnDj/IAAgBIgDgCIADADgAtWmQICXB3IBng9Ihhg0IgugYIgNgGIgfgQIhDAogAwilUQAaAPAaARIA7gkQgYgOgZgMgANpmUQgxAkgwAnIARAEIAIADQAJABAQAEIDKhbIgcgMIhFgaQgeAUgcAWgAqRmoIAdAUQAeAVAdAXIBAgmQgZgLgagLIhFgaIggAWg");
	this.shape_115.setTransform(258.9,240.7);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#14295C").s().p("A0cHRIg6gkIhCAbIkjhZIh5iEIgYoEIFfiQIBDgCIAGAGQAdgXAegUIAbAGQBCAPBJAYQA9ATA3AZIAcgOIB5glIAcAXIBJgiIBCgYIAVAKIAhATIgWASIgWASIgRgJIgdgPIhJArIBWBHQAogiApgfQAyAaAxAcIBlg7QgcgOgegMQgigNgggLIgjAYQgWgQgXgOIAggZQAhAGAjAIQBGASBMAZIBZgzIAcARIAfgRIAZAPQAngWAsgSQBAASA1AZIBDglIA2A/IAJgHQAagXAcgUQAhAGAjAIQBIASBOAaQAnANAkAPIAPgHQBggoBOgjIAXgIQAuAXAqAcQAuAeAqAlIAeAWIAHgHQArgwAwgmQAagXAcgUQAhAGAjAIQA7APA/AUIBhgqIAegEIAmAlIB1g1IBAAVIADADIAKgFIAcARIAfgRIEwDAIAYF9IipBjIhjByIi1BTIg4ALIhEhAIhHBXIhlgdIgaAUQgggFgigIQhHgShOgbQhBgVg8gbIg4AZIjKBeIgtgoIgFAGQg0AFgtgGQghgGgmgJQgXATgYASQgggFgigIIgzgOIgvAoIlDhvQgdACghgDQgrgDgygKQgjAlgmAfQgbAXgdAVQgggFghgIQg9gQhCgVIiHA/Ig/g5IgnAMIgWgSQgNAPgNAOQg0AFgtgGIgsgIIgTAdIgxAjgACNGLIAuARIAggWQgagIgZgKgAsAFJIAHAEICHhDIgDg9IhOAuIguAcIgkAWIgOAHQgogYgogeIgngeIgDgtIACgDIgFh9QgGh5gJhlIgBgIIgBgGIgFgBQgNABgQgDIgXAcIACArIAREYIgbAbIDMB4IADgCIAjAVgAL1j1Ig3A1IgPAPIAGBXIApA/IADBBIgiAvIABAVIAHB2QAgAZAgAXIgVAMIAOgBIASgEIgLgHIA4ghIAhgWIADgCIACgtQAAg4gDhAIgMjkIgRgMIhDguQAKgKAIgMQAOgSAIgSIg1AxgAHhCiQgwAzgyAtIAhAWQAzgyAyg8QgZgsgQg0gAmCkZIAUIGQAngkAlgnIgQkaIgIh3QgfgZgfgXgAivixIAREbIAEBBIBUgxIgYl6QgpAlgoAqgAoCApIgRA4IACArIAhgWIAAg+IgCg8QgHAXgJAWgAFXkoIAAABQAkA1ApAbQARANAXAHIgDgsQgugmgvgeIgVALgAxbk9QAcARAbAUIgWANIgLAHQAYAcAaARQARANAWAHIgDgsQgagWgbgTIBAgmQgZgQgbgOgAOdmCQg1Ang0AsIALACIAHACQAIAAATAEIDWhdIghgOQgigNgggLQgcATgbAVgAEwlXIAHAEQAbAPAaARIAIgEIA9gdQgigQgkgPg");
	this.shape_116.setTransform(260.2,248.2);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#132660").s().p("A1WHDIhBgqIhIAcIkbhSIhxiDIgWn0IFXiHIA9ADIAHAIQAegZAggVIAaAFQA/ANBHAWQBGAWA/AdIAugVIBwgeIAaAXIBWgmIBAgWIATAJIAkAWIAigaQAgAEAiAHQBEAQBMAZIAQAFIBcgzIAcASIAigSIAYAPQAqgYAvgSQBDASA2AbIBHgnIA3BFIAQgOQAZgVAbgTQAgAEAhAHQBFAQBMAZQAxAPAvAUIhEAeIgFgDQg4gkhBgYIg+gXQgcASgZAVIggAZIAgAoIBJgbICWARIASgIQAgASAfAWIgTAJIgmAUIgZAOIgTAKIAWF8IBkBIQAjgiAjgmIgQkLIgEhBIgDgwQgigdgigYIAjgSIAqgTIgZgMIgvgVIAhgPICohGIAWgHQAtAWAoAcQArAdAnAkQAYARAXAUQgbAcgbAeIARAMIAPALIAKAHIgIAIQgRAOgVAIIgFAFIAQEMIAHBvIAmAgIAVgNQASgKAQgMIADgsQAAgzgCg+IgMjXIg2gnQAPgNAKgRQALgPAHgOIAAgBIgfgcIAYgWQAogvAtgmQAZgVAbgTQAgAEAhAHQBAAPBGAWIBqgsIAcgBIAjAmIB/g4IA+AVIAGAFIAMgFIAcASIAigSIEbC3IAWFnIiuBkIheB0IiwBMIg0AGIhIhGIhKBgIhrgdIgZATQgfgEgggHQhFgQhLgZQhMgYhFggIAAgBQgmASgqARIjDBXIgugqIgHAHQgxAHgsgEQgngGgwgLQgVATgYARQgegEghgHIg/gQIguAoIlFhrQgaACgcgBQg7gBhJgRQgiAmglAeQgaAXgbAUQgfgEgggHQhCgPhJgYIiOA/Ig9g6IgtAMIgYgUQgMAQgPAPQgxAHgsgEQgcgEgggHIgTAgIguAigACPF2IAyARIAfgUQgcgKgagLgAsaE2IBmgwIgEhCIg6AiIgsAaIgiAUIgJAFQgsgegsgkICEjBIgDg/IhEhdIgNg+Ig+APIgMANIAPEMIAHBvIAEAEIgKAOIBcA3IAGgDQAXAPAYAOgAL2ETIgNAHQAWgEATgGIAigHQALgEAJgCIgXgQQgdARgeAPgAlgClIAMgNIgPkLIgHhxIgFgDgAyXkmQAeAUAdAYIgkAVIAXFYIA/AmIAAAAIgQkJIgDhEIgEgwIgbgWIANgIIA5ghQgbgTgdgRgAU3BkIAjAWIgOjtIgHhxIgkgdgAiTioIAQEMIABAWIAUgMIgSkqIgTAUgAo8AmQAEAnAHAhIAFgDIAAg8IgBgwQgHAUgIATgArelwIgaAUQAxAbAxAdIAdgRQgkghgngeIgLgIIgPAMgAupliIA6AzIAKgKQAdghAegcIgWgLIgcgPIhNAugA2zlxIgTAPIALARIAYAcIA4gVICSAQIAUgJIgBAAQg4gkhBgYIhBgYQgaASgZAUgAuhDXIAAAAgAMzkjIAAAAg");
	this.shape_117.setTransform(261.5,255.8);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#112365").s().p("A2PG1IhMgyIgEAFIhHAbIkThNIhoiCIgVnlIFOh8IA5AJIAHAJQAfgbAigWIAZAEQA8ALBFAUQBFAUA+AeIARAIIBCgeIBngWIAYAXQA2gWAsgTIA+gUIASAJQAVALATAMIAkgcQAeAEAgAGQBCANBJAXIAlAMIBdg0IAdAUIAkgUIAZARQAsgZAygTQBGASA4AdIBLgoIA3BLIAYgVQAXgUAagUQAeAEAhAGQBBANBJAXQA8ASA2AYIA3gXICig+IAVgHQArAVAmAbQApAdAkAjQAhAZAfAeQgaAdgZAgQgVAagVAdQASABAZgJQAJgDAJgEIAOD7IAHBpIAOANIARgMIADgoQABgygCg5IgLjMIgZgSIgGgEQAZgcAagZIgfggQAUgVAVgTQAmgvAqgkQAYgUAZgUQAfAEAgAGQBBANBJAXIAHACIBygtIAaABIAiAmICHg4IA8ATIAJAJIAPgIIAdAUIAlgUIEECxIAVFQIi2BmIAAACIhXBxIiqBHIgwACIhMhQIhNBrIhygdIgYASQgdgCgfgHQhBgNhKgXQhJgWhBgfIgagNQgwAYg4AVIi9BRIgugvIgIAKQgvAKgrgEQgugFg4gNQgVASgWAQQgegCgfgHQgkgHgmgKIgtApIlFhnQgYADgaAAQhDABhagWIgDgDIgFgGQgjAqgnAhQgZAVgZATQgegCgfgHQhBgNhJgXIgNgEIiUBAIg7g8Ig0ALIgZgXIgBACQgNASgOAPQgvAKgqgEQgkgEgpgIIgSAhIgsAhgAs7EfIBFgfIgVmRIhqhiQghAgggAkIgOAPIAOD+IACAmIgpBBIBlA+IAKgGQAaATAZAPgAMBEHIgZAOIgBAAQAkgCAdgHIgHgVIggAQgAEzkmIgxAVIASFxIAdBVIA5A6QAZgYAZgbIAqguIgOj8IgEg+IgDgtQgWgUgXgSIgPAHIgdgmIglgIIAQgHIgQgJQg0gig+gXQgegLgdgJQgaARgXATIgsAjIAaAhIBugngAyLhsIAOD6IglApIAuAuQAdghAcgnIgBgSIhBgpIgRkOIgBAAIAEBAgAh3igIAOD+IABAPIAUgLIgSkVIgRATgAVTBSIAjAYIgNjYIgGhrQgSgQgSgPgAp6AqIABgCQAJgSAIgUQgBhtgHh1IgZgPIgCgBgAr3lfIghAaQAyAcAwAdIAbgPQgmgngrghIgDgDIgIAHgAvUlMIA/A7IAZgZQAYgdAZgZIgbgPIgagNIhUAwgA3tlfIggAZIACAEIAbAhIBfghIB/AbIARgHIgNgHQg0gig+gXIg9gVQgZAQgXAUg");
	this.shape_118.setTransform(262.8,263.4);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#102069").s().p("A3IGmIhYg6IgIAMIhFAZIkLhGIhgiCIgTnWIFGhxIA0AOIAIAKQAfgdAkgXIAYADQA6AJBCATQBDASA7AcIAjASIBYgmIBfgOIAUAVQA/gXAxgVIA7gQIASAIQAWAMAVAOQASgQATgOQAdACAfAFQA/ALBGAVQAeAJAbAKIBUguIANgFIAcAUIAngUIAZARIAHgEQArgZAxgRQBJARA5AfIBPgpIA5BTQAPgQAPgOQAXgUAYgSQAdACAfAFQA/ALBGAVQBGAUA9AeIBOgfICbg5IAUgGQApAUAlAbQAnAbAhAkQApAgAoAtQAcgjAdgdQAjgtAogjQAXgUAYgSQAcACAfAFQA/ALBGAVIAZAHIB6guIAXAEIAgAmICSg5IA5AQIAMAOIASgIIAdAUIAngUIDvCnIATE7Ii+BoIAAAEIhQBvIijBAIgtgBIhPhbIhRB3Ih5gdIgWASQgdgCgdgFQg+gLhGgVQhHgUg/geQgXgMgWgNIgCgDQg6AfhKAaIi1BLIgvgzIgKAMQguAMgpgCQgugDg7gNIgNgEQgUASgVAQQgbgCgegFQgqgIgtgLIgsApIlGhjQgUAEgXAAQhAAEhZgTIgDgEQgQgQgOgUIgPAPQgjAtgoAjQgXAVgZATQgbgCgegFQg/gLhGgVIgegJIiaA/Ig4g8Ig9AJIgagaIgEAGQgMARgNAPQgsAMgqgCQgqgDg0gLIgPAjIgrAggACTFLIAFACQAcAKAZAIIAdgUIgSgIQgTgKgSgNgAqkBSIglAZIADBBIheB+QAjAVAoAOQAcAKAaAIQAagRAXgUQA8gtA5g7IgJgTIiehvIAAACgAtZEKIAhgOIgTl/IhOhNQgbAbgZAfIADAxIAdAgIACA9IhUCOIAHAtIBWA2IANgGIAFgDQAbAXAcATgAEukNIATGaIBaBgQAYgbAZggIgigbIAPgSIgNjvIgEg/IgCglIgZgXIgdgYIgQgMIgggLgAo4h8QAEBjAGBFQAGAuAJAkIBEAvIAjgoIgNjvIgGhkQgSgRgTgQQglAwgjBDgAzUCKIAEgFIgNjrIgEhEIgCgCgAq5AyIAEgKQAKgQAIgSQAAhpgGhyIgYgOIgJgGgAN+iWIAAACQAQgPALgSIgbAfgAsOlOQArAjAkArIBlg4QglgVgsgQQgcgKgbgIQgXAPgVASg");
	this.shape_119.setTransform(264.1,271);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#0F1D6D").s().p("A5lFTIgLAUIhEAXIkEhAIhWiAIgRnHIE8hnIAvATIAIALIAAACIABgBQAhgfAlgYIAXABQA3AIBAARQBBARA4AaQAaANAZAQIBwgwIBXgFIARASQBHgYA2gVQAegJAbgGIARAIQAYAOAWAPQATgRAUgPQAbAAAeAEQA7AKBEASQAoAMAlAOIBYgvIALgFIAdAWIAfgRIALgFIAZATIANgIQAqgYAxgQQBMARA5AhIBVgrIA4BbQATgUAUgSQAVgTAXgSQAbAAAeAEQA7AKBFASQBDATA7AcIAUAKIhOAeIAAAMIAgAPIAVAhIAKAKIABAKIAFBUIAMDfIghApIAgAlQAXgcAWghIgmggIgPkVQAUAhAWAQQALAKAQAGIgBgFQgGg6gMgwIgFgQQgHgagKgZIgWgMIBlgnICWgyIAUgFQAnATAhAaQAlAbAgAjQAxAoAuBBQAlg0AngpQAggsAmghQAVgTAXgSQAbAAAeAEQA7AKBEASIApANICDguIAWAFIAdAmICbg6IA3AQIAPARIAVgLIAdAWIAfgRIALgFIDZCfIAREmIjFBpIAAAHIhJBsIieA8IgogGIhUhnIhUCFIh/geIgVARQgbgBgcgDQg7gJhDgTQhFgSg7gdQgXgKgVgNIgJgaQgLAFgNAFIgHADQg9AjhUAcIiuBEIgwg4IgMAPQgrAPgoAAQgtgDg5gMIgfgHQgTARgUAPQgbgBgcgDQgwgHg0gOIgpApIlJheQgRAEgTABQg9AIhWgSIgDgDQgggcgWgnIgCgCQgSAVgSASQggAtgmAhQgVAUgYASQgbgBgcgDQg7gJhEgTQgZgHgXgIIgBABIigA+Igwg6IhJAEIgbgcIgGAKQgLAQgNAPQgqAPgpAAQgsgDg5gMIgKgCIgOAlIgoAgIhHAAgACTE1IAPAFQAaAJAXAGQAQgJAOgLQgfgPgZgWgAsDCkIhKBrQAoAaAxARQAaAJAYAGQAYgPAWgTQBDgzBAhJIinh6IgGhuQgNAngTAgQABhlgFhvIgWgOQgugggxgdQg1AugyA8IghgjQAagjAcgdQAOgUAQgRIgngVIgXgMIhiA2IBMBQQgeAngdAvIAmAlIADA7IgkBBIAEAzQAFAqAGAkIA7AmIAMgGIAOgIQAbAaAbAUIgDhKQA+giA6glgAowhyQADBgAHBDQAEAiAHAdIAlAbIAMgRIgMjfIgFheIgFgGQgZAmgXAxgABsk7QghAZggAfIBFBwIgDh2ICDgpIBmAyQgTgOgTgMQgtgeg4gTQgbgKgZgGQgXAOgUASgA06jIIAGANIAAgIIgIgIgA5ik8QgdAXgcAaIAnA5IAVAGIgDg9ICFgpIBjAxQgRgNgSgLQgugfg2gSQgdgLgZgGQgXAOgUARgAsGBPIAVgnQAKgPAJgPIAAANIgCAvIgmAZg");
	this.shape_120.setTransform(265.4,278.6);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#0D1A72").s().p("A6pE6IgCAAIgOAcIhCAUIj7g6IhPh/IgPm3IEzhdIArAYIAHALIgCAIIALgLQAegeAjgXIAXABQA0AFA+APQA/AQA0AZQAiAQAeAWICMg3IBNACIAOAQIAMgFQBKgXA1gUQAegHAZgEIAQAIQAaAOAXASQAUgTAVgQQAagBAcADQA4AHBCARQAzANAtATIBdgwIAKgDIAcAVIAkgSIAJgDIAaATIAVgNQAogXAvgOQBQAQA4AjIBbgsIA5BmQAWgbAZgXQAUgTAVgQQAagBAcADQA5AHBBARQBBAQA4AbQATAKATALICBgvICPgtIATgEQAmATAfAZQAjAaAcAjQA3AwA0BWIABANQAvhQAxg4QAfgrAjggQATgTAWgQQAagBAcADQA5AHBBARQAdAHAbAKICOgvIAUAIIAZAlIClg6IA2AOIAQAVIAQgIIAKgDIAcAVIAkgSIAKgDIDDCWIAPEPIjOBrIABALIhCBpIiYA1IgkgJIhYh3IhWCWIiHgdIgTAQQgaAAgagCQg5gHhBgRQhBgRg5gbQgVgKgUgMIgNgjQgfAXgxAHIgBAAIgIgDQg5AfhOAYIioA9Igwg9IgNATQgpARgnABQgrgBg4gKQgagGgYgHQgSARgTAPQgZAAgcgCQg0gHg8gPIgnAoIlIhZQgPAFgQACQg7AKhTgQIgEgDQgggZgYgkQgLgRgKgTIgIgGQgaAkgbAeQgdArgkAgQgUAUgWARQgaAAgbgCQg4gHhAgRQgngKgjgNIgGAKIiZA3Igsg3IgFACIhOgCIgcghIgJAOQgKARgMAPQgoARgoABQgrgBg4gKIgegHIgKAmIgnAeIhEAEgAtACaIg3BVIAOAKQAnAdAyAQQAZAIAWAFQAXgOAUgSQBGg2BChVIiKhqIgQkQIA4gdQgogbgygRQgZgIgYgGQgVANgTARIgMAKQgUAQgUATIg0gfIgugaIgVgKIhqA4IBUBgQgTAegTAhIAAABQAFBxAHBTQAFAlAGAgIAHAgQAJAhANAdIAJAFIAxgTIgEhWIAQgJQA4geA1ghgAQYCiIABACIgBgCgAGWBvIAFAGIADgGIgKjQIgFhYIgFgGgA19BTIAGAHIgKi5IgFhbIgDgDgAoohqQACBdAHA/IgHiTIgBgLIgBACgAB4krQgoAfgmAnIBsC+IAAABIgHjgIBzghQgYgLgagJQgZgIgXgGQgWANgSARgA6ckrQgkAbgiAjIAZApIBHAQIgChSIByghQgXgLgagIQgagJgYgGQgVANgSARg");
	this.shape_121.setTransform(266.7,286.3);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#0C1776").s().p("A7dEsIgXgFIgBAAIgOAdIhAARIjzgyIhGiAIgNmnIErhSIAmAdIAGALIgaAlIACADQASgaAWgUQAcgdAjgXIAUAAQAyAEA8ANQA8AOAyAYQAqAUAjAdIAAgEICng9IBFALIAJAMIAggLQBIgUAygRQAcgHAYgCIAQAHQAbAPAYAUIAFgDQASgSAUgRQAZgBAbABQA1AFA/APQA9APA1AZIBjgyIAJgCIAcAWIAngUIAJgCIAaAUQANgKAPgIQAngWAvgNQBRAQA4AlIBigtIA5B0QAZglAdgbQATgSAVgRQAYgBAaABQA2AFA+APQA/APA1AZQATAJATAMIAJAUIgBgKICmg3ICJgmIASgDQAkARAdAZQAgAaAaAiQAyAuAuBWIACBCQAJgBAMgDQAOgDAMgIIgBgaIAAAAQASgkATgeIgCgCQgRgRAQgCQADgHAFgKIAVgVQAOgOATgKQAbgpAfgdQASgSAWgRQAYgBAbABQA1AFA+APQAlAJAiAMICZguIARAKIAXAkICwg6IAzANIARAZIAWgLIAJgCIAcAWIAogUIAJgCICtCOIAOD4IjPBoIgHACIABARIg7BmIiSAwIgggOIhdiKIhZCsIiOgdIgRAOQgYACgagCQg0gFg/gPQg/gOg1gaQgVgKgTgLQgJgVgIgXQgdAdgyAKIgCAAQgPgDgWgLIgTAKQg3AfhPAWIihA1IgwhDQgHANgJALQgmAUgmADQgpAAg3gJQglgHgfgLQgRARgTAOQgYACgZgCQg2gFg+gPIgLgCIgjAmIlKhSQgMAEgNADQg4ANhSgOIgCgDQgigWgZgiQgSgYgOggIgDABIgHACIgZgTQgdAvgeAjQgbArghAfQgTATgVAQQgXACgagCQg1gFg/gPQg0gMgugUIgMAUIiTAwIgng3IgXAJIhEgKIgeglIgMATQgJARgLAOQgmAUgmADQgqAAg3gJQgbgFgYgIIgIApIgkAdIhBAHgAvaCVQAbAgAdAYQASAPARAMQAkAbAwAPQAXAGAUAFQAWgNASgQQBJg6BBhjIAAgBIhqhXIgNj5IAagNIgIgFQgmgcgxgOQgYgJgVgEQgVAMgQARIgUAQQgUARgSAUQgZgSgbgQIgOATQgpAxgmBGIAaAiIALDoIAFgDIAegQIADgCIACAdgAgIh/IAJDDIAEBRQAsA0AvAfQAYARAbAMIA5g9IgIgNIgBgCQgFgqgCg7IiSkXQgaAfgYAlgALZhyIABA3IhoDnIBJBFIBRgOIAOgFIATgJQAggOAdgQIABgBQgigcgTgqQgcg6gChRIAAAAIAEgJIAIgQIBegaIgBgUQgYgWgYgYIgfgeQgwgzgvgmQgLAAgKACQgVgBgWAIQgXAEgZAIIhDAcIgTAIIgNAHIAGCNIABAAQAYAEAOgHQAZgCATgkIAogmIAIgjgAprALIAaAWIgHh+IgEhTQgOgRgOgOgAuBBDIAaggQAJgKAJgLIgCAdIgqAag");
	this.shape_122.setTransform(268,294);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#0A147A").s().p("A8QEeIgWgDIgVgFIgEgBIgMAdIg+APIjsgsIg9h/IgMmXIEjhIIAhAiIAFALIgnAgIAMAZQAWgoAcgcQAbgbAhgWIATAAQAwABA5AMQA6AMAwAWQAsAWAkAgQAOApAGAyIAEAvQANAeAQAQIgIjGICeg2IA9ATIAFAHIAzgPQBFgTAwgOQAbgEAXgDIAOAIQAeARAZAXIAIgJQASgSATgPQAXgDAZABQAzACA8ANQA7ANAyAYIAVALIBrg0IAIgBIAaAXIAtgWIAIgBIAZAWQASgNATgLQAlgUAtgMQBUAOA3AoIBqguIA5CEIAIgKQAYgqAegdQASgSATgPQAXgDAZABQAzACA7ANQA8ANAyAYQATAJARALQANAZAKAeQAMAnAGAzIABAOQANAeAPAPIgHjFICegxQBOgUA1gLIASgDQAhARAcAYQAeAZAXAhQAsAsAoBWIACA7QAJALANACQAOAGAXgFQATgFAOgMQAGgGAFgHIgBgUIAAgBIARgjIgBgFQAAgGgFgEIgEgFQgXgUANgFQAAgHAFgLQAGgKALgNQATgbAkgOIAKgBIAMABQATgcAXgWQARgSAUgPQAXgDAZABQAyACA8ANQAsAKAnAPICnguIAPANIATAhIC7g4IAxALIARAeIAdgOIAIgBIAaAXIAtgWIAIgBICYCFIAMDjIi+BaIgHACIgagXIACA5IgzBlIiMAqIgdgSIhhijIhcDGIiJgaIgFgJIgXATQgWADgYgBQgygCg8gNQg8gNgygYQgUgJgSgMQgMgZgKgdIAAgBQgbAkgyAPIgCAAQgdgBgxgcIgGAMIgaAQQg0AbhNAUIiaAuIgwhLQgIARgKANQgkAXglAEQgoACg2gJQgvgHglgQQgRAQgSAPQgXADgYgBQgygCg8gNIgegHIghAlIlEhJIAAgFQgMAFgNADQg1AQhQgLIgDgDQgigUgZgeQgZgdgQgpIgFgSIgVAJIgHACIgigdQgeA9giApQgYAqgfAdQgRASgUAQQgWADgZgBQgygCg8gNQg7gNgzgYIgNgHIArhRQgNgOgNgRIgDg4QAcgOAbgRIgMANIADBJIgRAgQAbAdAdATQAgAbAtAMQAVAGATAEQAUgMAQgQQBBg0A5hfIgIizIgEhLQgbglgcgYQgLgJgLgHQgjgagtgNQgXgIgTgDQgTALgPAQQgPALgOAOQgSASgRAUQgZgTgbgRIgFAJQgsA2gnBXIAAABQADBoAIBJIAEAcQAFAjAIAbQAKAdANAZIAYAOIgRAfIiMArIgjg2IgpANIg8gSIgfgrQgHAPgIAMQgIARgKANQgkAXglAEQgoACg2gJQgogGgggNIgEAtIgjAcIg9AKgAATh3IAICzIAEBLQAnAzArAcQAQANASAKIBEhOIhrioIhBiTIgYAlgAVTkBIAMF1IA6BqQA4gzAyhTIgIizIgEhLQglgygpgbQgTgPgXgLgALYiCIABA2IhUDUIBOBRIBHgFIANgGQAHgEALgDQAegLAagQQALgGALgIIABgEQgOgRgKgUQgcg2gChPIAAAAIAEgJIAHgQIAtgLIgBghIgrgsIgbgdQgpgygqgkQgJgBgKACQgSgDgUAFQgUADgYAGIg+AYIgSAIIgMAGIAHCZQAWgFAQglIAjgsIAHg2gAzMjUIgeAQIACAuIAhAUIABA2IgaBBIABAXICYBzIADgBIgIjOIhniQIgZAMg");
	this.shape_123.setTransform(269.2,301.8);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#09117F").s().p("A9DERIgVgDIgUgEIgegHIgKAdIg8ANIjkgmIg0h9IgLmIIEag+IAdAoIAEAKIg0AcIAUAxIARgZQAUgoAagaQAZgbAegUIATgCQAtAAA3AKQA3AKAtAVQArAUAhAhQANAjAGAsIAFAJIAPAlQAPA5AWAUQALANATAEIgIjuICWgvIA0AbIABACIBHgUQBDgPAsgMQAagDAWgBIAOAHQAgASAZAbIANgPQAQgRATgOQAVgEAYgBQAvAAA5ALQA5AMAvAWQASAIAQALIADAEIB5g2IAHgBIAYAXIAzgWIAHgBIAaAZQAUgSAXgNQAkgTAsgKQBVAMA1ArIBzgvIA5CXQALgSALgPQAVgoAcgcQAQgRASgOQAVgEAYgBQAwAAA5ALQA5AMAvAWQARAIARALQANAXAKAcQAHAWAFAaIAIAMIAOAlQAQA5AWAUQALANATAEIgIjuICXgqQBMgRAxgJIARgBQAgAQAZAXQAcAYAUAiQAnApAiBWIACA1IAdgHIAFAHQAKAWARAFQANAHAXgEQASgEANgMQAOgOAGgTQAIgVgGgUQgCgGgGgFIgGgGQgdgWAJgHQgBgJADgMQAFgMAJgOQARgdAigQIAJgDQAVgDAeALQARgbAUgUQAQgRASgOQAVgEAYgBQAwAAA5ALQAyAKAqATIC4gtIANAOIAOAeIDHg1IAuAKIASAhIAjgPIAHgBIAZAXIAygWIAHgBICCB9IALDNIitBNIgGACIgZgZIgbAMIACBBIgsBiIiGAjIgZgVIhZitIgYAFIhTDMIiEgWIgKgUQgNANgPAMQgVAEgXABQgvAAg5gLQg5gLgwgXQgSgIgRgLQgNgXgKgcIgFgTIgEAFQgWAqgxARIgCABQgdAEg0gYQgXgLgTgPQgHARgJAPQgMAIgNAGQgxAZhKARIiTAoIgvhUIgFAHQgHARgKAOQgiAYgjAGQgmADg1gHQgxgGgkgRIgPgIIgBABQgRASgSAOQgVAEgXABQgvAAg5gLQgagFgYgIIgdAlIk1g/IAAgRQgQAKgTAHQgyAShOgKIgEgCQgjgRgagcQgZgbgQgmQgJgYgFgfIgrATIgFACIgtgsQggBOgkAxQgWAogcAcQgQASgTAOQgUAEgXABQgwAAg5gLQg5gLgvgXQgTgIgQgLIgHgNIgZA2IiFAkIgeg1Ig7ASIg0gaIghg0QgIAVgIAPQgIARgKAOQghAYgkAGQgmADg1gHQgwgGgkgRIgJgFIgDACIACAyIghAaIg5AOgACaj6Qg5AwgyBbIAHCkIADBEQAiAyAlAaQAJAIAKAGQAXAOAdAIQAUAFARADQASgLAPgPIAQgOQgNgRgKgWIAAgCQgGgwgChPIgHkfIAIgCQgPgGgQgFQgVgFgSgDQgRALgOAOgAvzA1IAEBEQAhAyAmAaQAdAZApALQAUAFASADQASgLAOgPQA5gvAxhbIgHikIgDhGIgCgDQgfgvgjgXQgfgZgqgMQgVgFgSgDQgRALgOAOQgTAQgTAVQgPASgPAVIgDgCQgXgVgagSQgggXgkgUIgQgIQg9AbhJAiIgbAOIAKC6ICCBpIAHgCIATgJIASgJIAGgDQAngSAjgUgALXiRIABAzIhCC+IBUBhIA+ADIALgEIARgHQAcgKAYgNQAKgGAKgHQADgMACgMQADgYABggQgJghgBgoIAAgBIAEgJIADgIIgCgzQgSgUgSgWIgYgbQgjgwgkghQgIgDgIABQgQgGgSAEQgSABgWAFIg5AUIgRAHIgLAFIAFBzIAagqIAGhJgAWfj+IAKFkIApBWQAogsAlhDIgHikIgEhGQgggxgjgYQgTgQgYgKg");
	this.shape_124.setTransform(270.5,309.5);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#070F83").s().p("A92EDIgUgDIgTgDQgdgEgagJIgJAeIg7ALIjbgfIgsh9IgIl5IERg0IAXAuIAEAKIhBAYIAcBQIAEAAQAOgiAQgYQARgnAYgZQAXgZAdgUIASgCQAqgDA1AJQA1AIAqAUIAWAMIAegFIBGCTIAMAmQANA5AUATQALAOAWACQAUADALgIIgIj8ICNgmIAmAeIBdgXIBqgWQAZgCAUABIANAHQAiASAYAfIACACQgOATgMAYIgKARQgjgdgpgXIgNgIQg2AWhBAcIgYALIAICkIBtBgIAGgBIAQgIIAWgKIAZgLQAngTAigWIAEgZQAEggABgjIAAgQQAUgqAWgfQgQgngageQAJgMAJgKQAPgRARgOQAUgFAWgBQAtgCA2AIQA2AKAsAVQARAIAPAKIALAUICRg8IAFgBIAWAYIA6gXIAFgBIAbAdQAWgWAcgQQAjgRArgJQBVALAyAqIABABIB+guIA4C1QARgmASgcQATgnAZgaQAPgRARgOQAUgFAXgBQAsgCA2AIQA3AKAsAVIAFACIAcgEIBFCTIAMAmQANA5AUATQAMAOAWACQASADALgHIgHj9ICQgjQBJgOAugFIAQAAQAeAOAXAXQAZAYATAgQAhAoAdBVIABBAIBIgNIAEAHQAIAZAQAFQAMAJAWgFQASgCAMgLQANgOAGgTQAGgUgIgTQgDgIgIgGIgIgGQghgaAFgJQgDgKACgNQADgNAIgPQAOggAfgTIAKgEQAdgIAyASIAKAEQAOgYARgSQAPgRARgOQAUgFAXgBQAsgCA2AIQA3AKAsAVIABAAIDKgrIALARIAKAZIDTgxIAsAIIAQAmIAsgSIAFgBIAXAYIA5gXIAFgBIBtB1IAIC3IibBBIgFAAIgXgYIg4AYIACBHIgkBfIiAAeIgUgZIhNivIg6ANIhFDHIh/gSIgNgiIgDAEQgQARgRAOQgTAFgWACQgsACg2gKQg3gJgsgUQgSgJgPgKQgNgVgLgaQgGgTgFgXIgHASQgTAtguAUIgCAAQgeAJg1gTQg2gUgdgtIgFAUQgJAagOATIgYANQgtAXhIANIiMAiIgvhiIgJAVQgGAQgJAOQggAbgiAGQglAFgzgFQgwgHgigQQgRgIgPgLIgGAHQgQARgRAOQgTAFgWACQgsACg2gKQglgGgggMIgbAlIklg1IgBgkQgSAVgaALQgwAVhMgIIgEgCQgkgNgagaQgagZgRglQgLgcgGgpIAAgEIgMgNIg5AYIgFAAIhEhIIABAiQgcBTggAwQgTAogaAbQgPARgRAOQgUAFgVACQgsACg3gKQg2gJgsgUQgSgJgPgKQgOgVgKgaIgFgRIgpBlIh/AeIgZgyIhOAVIgrgiIgjhAQgIAegKAUQgHAQgJAOQgfAbgiAGQglAFg0gFQgvgHgjgQQgNgGgMgJIgMALIAHAvIgfAZIg2ARgAV2jpQgXATgVAeQAHAJAGAJQAdAtABBFIAAAAIgDAIIgDAIIhQAPIACBCIADA/QAdAwAgAYQAaAYAmAJQASAEAQABQARgJANgOQAWgTAUgcQAZgjAWgwIgGiUIgDhBQgagvgfgWIgNgKQgXgQgegIQgTgEgRgCQgPAKgNAOgACljpQgxAqgqBXIAGCVIADA/QAdAwAgAYIACACQAZAWAlAJQASAEAQABQARgJANgOQAPgNAPgSQAfgnAcg8IgGiUIgDhBQgagvgfgWIgNgKQgXgQgegIQgTgEgRgCQgPAKgNAOgAuojpQgZAVgXAhIACADQAUAyAFAkQgGARgHAOQgTAqghAbIABAjIADA/QAcAwAhAYQAZAYAnAJQASAEAQABQARgJAMgOQAxgqAphYIgGiUIgDhBQgbgvgegWQgbgYgogKQgTgEgQgCQgQAKgMAOgA+FjqQgpAjgjBBIAVADIAiAQIACBrIhDAfIABAXIADA+QAcAxAhAXQAZAYAmAJQATAEAQABQAPgJAMgNQAQgOAPgUQAfgmAdg7IgGiTIgDhCQgbgvgegWIgMgKQgXgQgfgHQgTgFgRgCQgPAKgMANgALbjSQgYAGgdAKIgPAFIgCABIgIADIAJDUIBsCPIAzALIALgDQAGgEAJgCQAagJAVgLIATgMIAGgWQADgZABglIgEh1IgegnIgUgaQgdgugeggQgHgDgHAAQgNgIgQACIgEAAQgPAAgRADg");
	this.shape_125.setTransform(271.8,317.2);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#060C88").s().p("A+pD1IgTgBIgSgDQglgFgggNIgLgFIgIAiIg5AJIjUgZIgih8IgHlpIEIgqIATAzIADAKIhPATIAXBPIAmAFQAMglAPgZQAPgmAVgYQAWgZAbgSIARgDQAngFAzAGQAyAHAoATQANAGAMAIIA/gKIA4CkIAJAlQALA6ARARQALAOAVADQAWACAKgLQARgJAJguIAGgQIgGjEICFgfIAXAdIBzgXIBkgRQAYgBAUACIALAGQAgATAWAeIAJALQAKgRANgOQANgQAQgNQATgGAVgDQApgEA0AHQA0AHApAUQAPAHAOAKQAOATAKAYQANAfAGAtQAGA4ACBcQgWBSgcAuQgQAngXAZQgOAQgPANQgTAHgUACQgpAFg0gIQgzgHgqgUQgQgHgPgKQgNgTgKgYQgMgfgGgsIgFg1IgTAKIACA9IgsCFIh6AYIgSguIhiAXIgigqIglhUQgJAtgLAZQgGAQgIAOQgdAdghAIQgjAHgzgFQgugEghgPQgXgMgRgRQgLALgNAJIAMAuIgcAYIgyAUgAvBjZQggAdgbA4QAHAeABAXQgGAQgIAOIgIANIADBKIACA5QAYAuAbAWQAWAWAjAIQARADAOABQAPgJALgNQApglAhhUIgFiGIgCg6QgWgugagUQgXgXgkgHQgSgEgOAAQgPAIgKANgA/AjaQglAigeBGIAQAGIACBmIgWAHIABAlIADA4QAXAvAcAWQAWAXAjAHQARADAOAAQAOgIAKgMQAcgaAZgvQALgWALgbIgFiFIgDg7QgKgWgLgQQgMgSgNgKQgYgWgkgIQgSgEgOAAQgOAIgLAMgA0Li/IgVAJIAHCOIBXBWIAEAAIAPgGIASgIQAfgMAcgPIAbgQIAFgVQAIg0gCg/IgJgIIAAAAQgjgogwgbIgLgHQguAQg6AWgAKWCOQgGAYgHAQQgFAQgJAOQgdAdghAIQgjAHgygFQgvgEghgPQgbgOgTgWIgOARQgNAQgQANQgTAHgUACQgpAFg0gIQgwgHgogSIgXAmIkWgqIgBhEQgTAogkATQgsAYhLgGIgDgBQglgMgbgXQgbgWgRgjQgMgbgFgnIgDgaIgJADIgEAAIgTgYIhBAYIgEAAIhXhsIgGijICKgzIAEABIATAXIBAgYIAFABIAbAiQAYgeAigRQAhgRAqgHQBSAIAvAqIAEADICJgsIA6DmQAWhLAZgsQAQgmAXgZQANgQAQgNQATgGAVgDQApgEA0AHQA0AHApAUIAMAGIA8gJIBCDJQAKA6ARARQALAOAVADQAWACAKgLQARgJAJguIAFgOIgFjGICJgcQBHgMAqgCIAPABQAcAOAWAWQAWAXAQAgQAcAmAXBVQACBngDA2QgEAngLAdQgJAYgPASQgLAHgMAFQgpAUhGALIiFAbgACxjZQgqAlghBUIAFCGIACA1IAvApIgLARQAHAIAIAGQAWAWAjAIQARADAOABQAPgJALgNQAbgZAYgsQAMgXALgdIgFiGIgCg6QgLgXgMgQQgMgRgNgKQgXgXgkgHQgSgEgOAAQgPAIgKANgAMDjHQgWAEgaAHIgNAFIgDABIASAFIAAAwIgUBXIACA0IBXCIIAoAUIAKgDIAOgEQAYgHATgKQAJgFAIgGIAGgTQAEgWABghIgChpIgZgmIgPgYQgYgsgYgdQgGgFgFgBQgLgKgOAAIgRgBIgPABgAZ1D2IgQgzIgPASQgNAQgQANQgSAHgVACQgpAFg0gIQgzgHgqgUQgQgHgPgKQgNgTgKgYQgMgfgGgsQgHg5gChcIADgIQgLgIgRACQgQADgLALIgFAFQAJATgFAUQgDAOgGAJQgMgTAGgTQADgOAIgKQgEgHgLgIIgJgHQgngcACgMQgFgLABgOQABgOAGgRQALgjAegUQAEgEAGgCQAdgNAzAPQAaAHAUANQAMgXAPgRQAOgQAPgNQATgGAVgDQApgEA0AHQA0AHApAUIAKAFIDhgoIAOAlIDegqIAqAGIAPApIA0gUIAFABIASAXIBBgYIAEABIBXBsIAHCiIiKAzIgEAAIgUgYIhAAYIgEAAIgRgWIACBuIgdBdIh6AXIgQgdIg+itIgFACIhZAPIg3DDgAWrjZQgaAYgXAqQAPAjACAwIgBABIgCAIIgDAHIgjAGIADBUIADA5QAXAuAbAWQAWAWAjAIQARADAOABQAPgJAMgNQAdgbAag1QAJgTAJgWIgFiGIgCg6QgLgXgMgQQgMgRgNgKQgXgXgkgHQgSgEgOAAQgPAIgKANgAR0DEQg5gQgegtQgegtgBhJIAAgBIADgIIAEgOIBngQIADAIQAHAaAOAIQALAJAWgCQARgDAMgMIAHgIQAEAIAIAJIAFAEIAIAGIAGAEQAhAXABAOQADAKAAAOQgCAOgGAQQgPAxgsAXIgBABQgOAGgTAAQgWAAgegJg");
	this.shape_126.setTransform(273.1,324.9);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#04098C").s().p("A/cDnIgSAAIgRgCQgjgEgegMIgOgGQgMgLgLgOIgKA1Ig3AGIjLgTIgbh7IgFlaIEAgeIAQBBIhcAPIARBOIBJAHQALgnAMgaQANglATgXQATgYAagQIAPgFQAlgHAxAFQAxAFAkARQANAHAMAHIBjgLIAxDZQAHA5AQARQAKAOAUACQAWACAJgMQAOgKAIgyIAPg9IADiCIAYAFIgBgnIB8gWIAaAxIADAKIgBglIB1gSQA8gIAjgDQAXgBASAEIALAGQAeARATAeQAJAMAHAOQAMgbAQgTQAMgPAPgMQARgIATgDQAngIAxAGQAxAGAmASQAPAHANAJQAOARAKAWQANAcAFArQAHAzABBYQgRBPgVAsQgOAmgVAYQgMAPgPAMQgQAIgUAEQgmAGgxgFQgwgGgngSQgPgHgNgJQgOgSgLgVQgMgcgFgqQgEgfgDgrQgPAJgUAFIgeANIAAA4IghCBIhyASIgNgnIh1AWIgagyIgnhzIgGAfQgGAugJAZQgFAQgIANQgbAggfAJQgiAJgygDQgtgEgfgOQgegPgUgcIgEgGQgPAWgVAQIASArIgbAXIguAYgAvFjcQgNAHgIAMQgiAggZBQIAEB4IACAyQASAtAWAUQASAVAhAGQAPACANAAQANgIAKgMQAhggAYhQIgDh4IgDg0QgQgsgWgTQgTgUghgGQgNgCgLAAIgFAAgA/kjcQgNAHgJALQghAhgZBQIADB4IADAyQARAtAWAVQATAUAhAGQAPACANgBQALgHAJgLQAiggAZhSIgEh2IgCg1QgQgsgVgSQgUgVgggGQgOgCgMAAIgDAAgA0gi4IgSAHIAFB3IBBBNIAEABIAMgFIAQgFQApgNAggVIAGgSQAJgqgCg2IgGgHQgbglgngXIgKgGQglAKgzARgAKrBXIgBADQgGAugKAZQgFAQgHANQgbAgggAJQghAJgygDQgtgEgfgOQgegPgUgcQgFgHgFgJQgKAUgLAOQgNAPgPAMQgRAIgSAEQgmAGgygFQgxgGgmgSQgKgEgKgGIgUApIkHgfIgElIIgVAFQgBgggHgJQgJgLgTAFQgWAEgHAYQgIAhABBfIABAIIAAASIgOBuQAEAXASACQARADAEgMQACgLgFgkIgEgPIAeAHIAmgHIgBAHIgCALQgNBAgsAcQgqAbhIgEIgEgBQgmgIgbgVQgbgUgSgiQgMgZgFgmQgEgagCglIghALIgEgBIgOgWIhJAXIgDgBIhBhjIgGiNIB6gnIADACIAOAVIBJgXIADACIAcAqIAJgNQAXgdAhgRQAggQApgFQBQAGAsAqIAFAEIAAgDICWgoIBAFSIAVANQgFgvAAhHQAQhRAWgrQANgmAVgYQAMgPAPgMQAQgIAVgDQAmgIAxAGQAxAGAmASIAPAIIBhgLIAxDZQAHA5AQARQAJAOAWACQAVACAJgMQAOgKAHgyIAPg9IADiCIAZAFIgBgnICCgUQBDgJAoABIANABQAbANATAWQAUAWAOAgQAWAkARBUQABBjgDAxQgFAkgLAbQgKAWgOAQIgWALQgnAShDAIIh9AUgADTjcQgNAHgKAMQghAggZBQIAEB4IACAyQARAtAXAUQASAVAhAGQAPACANAAQANgIAJgMQAiggAYhQIgEh4IgBg0QgSgsgUgTQgVgUgggGQgNgCgLAAIgEAAgAMqi8QgSABgYAGIgMAEIgJACIAFCrIBBB+IAfAcIAJgCIANgDQAVgFARgJQAIgEAIgFIAGgRQAFgRAAgfIgBhdIgSgjIgMgXQgRgrgTgbQgEgFgEgDQgKgLgLgCQgLgEgOAAIgEABgAaxDlIgShLQgLAXgOARQgNAPgOAMQgRAIgTAEQgmAGgygFQgwgGgngSQgPgHgOgJQgOgSgKgVQgMgcgFgqQgHgzgBhYIAAgEIgQACIgBgEQgEgXgNgKQgKgLgSACQgQACgKALQgLAMgDARIgBAHIgIgGQgrgfgEgOQgFgLgBgQQgBgQAFgSQAIglAcgWQAEgFAGgDQAegSA0ALQAxALAdAfQALgZAPgSQAMgPAPgMQARgIAUgDQAmgIAxAGQAxAGAmASIAPAIID8gjIAJAfIDpgjIAnAEIALArIA/gUIAEACIANAVIBJgXIAEACIBBBkIAFCLIh6AnIgCgBIgPgWIhJAXIgDgBIgpg+IAECpIgXBaIh0ARIgMghIgyi1IAAAGIgpANIhWALIgpC/gAX2jcQgNAHgKAMQggAggaBQIAEB4IACAyQASAtAVAUQATAVAhAGQAPACANAAQANgIAKgMQAgggAZhQIgEh4IgCg0QgRgsgUgTQgUgUghgGQgNgCgLAAIgEAAgASxDDQg7gMgfgqQgfgqgBhGIAAgBIADgIIADgQIBkgKIACAIQAGAcAMAKQALAKAVgBQAQgCAMgMQALgMADgTIABgGIgFgDQgZgZABgXIADADQAMAIAGAJQAJAPgBAQIACABIAJAFIAHAGQAmAZADAPQAFAMABAPQAAAPgEASQgLA0gpAaIgCABQgTALgcAAQgSAAgVgFg");
	this.shape_127.setTransform(274.4,332.7);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#030690").s().p("EggPADaIgRAAIgQgBQgigDgcgLIgMgGQgQgMgMgQQgUgcgJgoQgHgjgChAIhXAQIABA/IgFAkIBqAMIgLBYIg0AEIjFgMIgSh7IgDlKID3gVIALBHIhpAJIALBPIBaAGIATADQAIgqAKgaQAKglARgUQASgYAYgPIAOgGQAjgJAtAEQAvADAiAQQAgAPAWAbIgBghIBrgIIAhDoQAEA5ANARQAJAOAVABQAUABAJgNQALgLAGg0IAJhCIACiWIBIALIgBghIB0gPIAsCrIgCicIBugNQA6gEAggCQAWACAQAEIAKAGQAcAQARAdQAPAYAIAeIAEgOQAMglARgVQALgPANgLQAPgJATgGQAkgIAuAEQAuADAjARQANAGANAJQAOAPALAVQANAYAFApQAGAtABBUQgLBOgRApQgKAkgTAXQgLAPgNALQgPAJgSAGQgjAIgugEQgvgDgjgQQgOgHgMgJQgOgPgLgVQgNgZgFgnQgFgmgChAQgeAfgzAKIggAIIAAA1IgWB9IhtAMIgciKIABBvIh0AQIg6jpIgLBcQgEAugIAYQgEAQgHANQgZAjgeAKQggAKgwgCQgsgCgegOQgdgOgTgbQgMgSgHgaIgEAJQgQAogeAWIAXAqIgYAWIgqAagAvhjKQgLAGgIALQgZAbgQBNIACBoIABAsQANAsAQASQAQATAdAEQAOABALgBQAMgHAIgKQAZgcAQhMIgChpIgCgtQgLgsgRgQQgQgTgegEIgNgBIgMABgEggigDKQgKAGgJALQgZAbgQBNIACBoIACAsQANAsAQASQAQAUAcADQAPABALgBQAKgHAIgKQAYgbAShOIgChnIgCgvQgMgrgQgPQgQgUgdgEIgOgBIgMABgA01ixIgPAEIADBgIAsBGIACABIALgCIANgEQAigJAbgRIAFgNQAKggAAguIgFgGQgRghgfgTIgIgFQgeAEgrALgAIZDdQgsgCgegOQgcgOgTgbQgOgUgHgfIAAgCIgBgIIgFAMQgLAkgSAXQgLAPgOALQgPAJgSAGQgjAIgugEQgvgDgjgQQgOgHgMgJQgIgJgHgKIgVA4Ij3gVIgCk+IhEAMQgBgjgIgMQgJgOgUADQgWAEgIAWQgIAdABBfIAAAIIAAAQQgHBUABAYQAFAWAVACQASACAGgOQAEgMgEgmIgDgQIAjADIAsgDIgBAHIgBAMQgKBEgoAfQgnAehGgCIgDgBQgngGgdgTQgbgRgTggQgMgWgFgmQgFgjgBg9Ig8AQIgCgCIgJgTIhTAVIgCgCIgrhcIgEh4IBpgZIACADIAJARIBSgUIACADIAdA8QAIgVALgPQAVgdAfgQQAfgPAngEQBOAFApAoQAYAXANAjIgBhCICFgcIArFRIBGAeIgCgLQgGgugBhTQALhQARgpQALglARgVQALgPAOgLQAPgJATgGQAjgIAvAEQAtADAkARQANAGANAJQALAMAJAPIgBglIBqgIIAhDoQAFA5AMARQAKAOAUABQAUABAKgNQALgLAFg0IAKhCIACiWIBIALIgBghIB7gOQBBgGAkAEIAMADQAZAMASAWQASAUAKAgQARAhALBVQABBegEArQgFAigMAZQgKATgQAOQgJAHgLAEQgjAPhBAGIh3ANIgvjaIAAgHIgJBJQgEAugHAYQgFAQgGANQgaAjgeAKQgZAJgkAAIgTgBgADajKQgKAGgJALQgZAbgQBNIADBoIABAsQAMAsARASQAPATAeAEQAOABALgBQALgHAJgKQAYgcARhMIgDhpIgBgtQgMgsgRgQQgPgTgegEIgOgBIgMABgAMsivIgKADIgIACIAECVIArB3IAVAlIAHgBIAMgDQATgDAQgHIAMgIIAIgOQAEgPABgcIAAhQIgMghIgIgWQgLgogNgYQgDgIgDgCQgHgPgJgDQgKgGgPgBQgPAAgXADgAYpDbQgugDgjgQQgPgHgMgJQgOgPgLgVQgMgZgGgnQgFgugBhTIAFgkIg+AFIAAgEQgDgZgLgLQgKgMgRABQgPACgKAKQgLALgBARQgEAZAgAdIAIAEIAKAHIAJAHQAqAaAHASQAGAMACARQACAQgDASQgHA5gmAcIgBABQghAYg4gJQg+gHgegoQgfgmgBhFIAAAAIABgJIAEgPIBggHIACAKQADAfALAKQAJAMAWgBQAPgBALgMQALgLACgTQADgTgNgRQgIgKgOgKIgMgJQgxghgHgQQgHgOgDgQQgBgRADgUQAFgnAZgZIAKgKQAggWA1AIQA6AHAfAmQALAMAHARQALgjARgVQALgPANgLQAPgJATgGQAjgIAuAEQAvADAjARQANAGAMAJQAMAMAJAQIgBgmID9gYIAEAWIAAABIDzgbIAlAEIAIAqIBJgSIADADIAIARIBTgUIACADIAsBbIADB2IhpAbIgCgCIgJgTIhSAVIgCgCIgrhcIgChQIgZADIAGAfIADEFIgPBYIhuAMIg0kQIgBhNIhKgRIgBgmIhSAJIAMBKIB1AbIAABgIgrAIIhSAIIgcC6IhtgHIgUh6IgJAYQgLAkgRAXQgMAPgNALQgQAJgSAGQgYAGgdAAIgcgCgAYnjKQgMAGgHALQgZAbgRBNIACBoIACAsQANAsARASQAPATAeAEQANABAKgBQANgHAIgKQAZgcAQhMIgChpIgCgtQgMgsgQgQQgQgTgdgEIgOgBIgMABg");
	this.shape_128.setTransform(275.7,340.4);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#010395").s().p("EghDADMIgQABIgPgBQgggBgZgKIgNgFQgPgLgNgOQgUgZgJgmQgIgqgBheIAAgBQAFhOALglQAIgkAOgUQAQgXAWgOIAOgGQAggLArACQAsABAgAPQAfAOAUAdQAOAYAFAmQAGAqAABLQABAigBAcQgGAxgKAbQgNAngcAVIAdAnIg9AzgEghfgC4QgKAGgGAJQgRAWgIBJIABBaIABAlQAHArAMAQQAMASAaACQANABAJgDQAJgGAGgJQARgWAIhKIgBhZIgBgoQgHgqgLgOQgMgSgagCIgHAAQgJAAgGACgAIyDOQgsgBgcgNQgbgNgRgcQgNgTgGgfIAAgCQgFgiAAhGIgBjtIBmgEIARD4QACA7ALAPQAIAOAUABQAUAAAHgNQAKgOACg4IAFhFIABiqIBgAIIgOEQQgCAugGAXQgDAQgGANQgXAlgdAMQgcAKgqAAIgHAAgA8QDOQgrgBgcgNQgcgNgQgcQgNgTgGgfIAAgCQgFgigBhGIgBjtIBngEIAQD4QADA7AKAPQAJAOATABQAUAAAIgNQAKgOACg4IAFhFIABiqIBfAIIABAqIgODmQgCAugGAXQgDAQgHANQgWAlgdAMQgcAKgqAAIgIAAgAZjDNQgsgCgggPQgNgGgLgIQgPgOgLgSQgMgWgFglQgGgpgBhPQAGhOALgmQAIgjAPgVQAKgOAMgLQAOgKARgGQAggLAsACQAsACAgAPQAMAGALAHQAPAOAKATQANAWAGAlQAGApAABQQgFBMgLAmQgJAkgPAVQgKAOgMALQgOAKgQAGQgcAKgjAAIgNgBgAZYi4QgKAFgGAKQgRAWgIBJIABBaIABAmQAHAqAMAQQAMASAaACQAMABAKgDQAKgFAGgKQARgWAJhJIgChaIgBgoQgHgqgLgOQgNgSgagCIgGAAQgJAAgHACgADuDNQgsgCghgPQgNgGgLgIQgOgOgLgSQgMgWgGglQgGgpAAhPQAGhOAKgmQAJgjAPgVQAJgOANgLQAOgKARgGQAggLAsACQArACAgAPQANAGALAHQAOAOALATQANAWAFAlQAGApABBQQgGBMgLAmQgIAkgPAVQgKAOgNALQgNAKgRAGQgbAKgkAAIgMgBgADji4QgKAFgGAKQgRAWgIBJIABBaIABAmQAHAqAMAQQAMASAaACQAMABAJgDQALgFAGgKQARgWAIhJIgBhaIgBgoQgHgqgLgOQgNgSgagCIgGAAQgJAAgHACgAvzDNQgrgCghgPQgNgGgLgIQgOgOgLgSQgNgWgFglQgGgpAAhPQAFhOALgmQAIgjAPgVQAKgOAMgLQAOgKARgGQAhgLArACQAsACAgAPQANAGALAHQAOAOALATQANAWAFAlQAGApAABQQgFBMgLAmQgIAkgQAVQgKAOgMALQgOAKgQAGQgcAKgjAAIgNgBgAv9i4QgKAFgGAKQgRAWgJBJIABBaIABAmQAIAqALAQQAMASAbACQAMABAJgDQALgFAGgKQARgWAIhJIgBhaIgBgoQgHgqgMgOQgMgSgbgCIgGAAQgJAAgGACgALrgMIgCjmIB0gHQA+gCAgAHIANADQAWAMAPAUQAQAUAIAgQALAgAGBTQABBagFAkQgFAhgNAWQgLATgQAMQgJAFgKAEQggANg+ACIhwAHgANZioIgJABIgHACIACCAIAgCcIAHAAIAKgBQARgCANgFQAGgDAFgEQAEgFAEgGQAFgMACgYQABgXgBgtIgGggIgDgUQgGgmgHgWIgEgNQgEgQgHgFQgIgIgNgDIgPgBIgSABgAiaC+IgCmwIB0gNIAVFQIB0AYIgUBggA20gMIgCjmIBngGQA4gCAdABQAVADAPAGIAKAFQAZAPAPAdQAPAdAEAnQAEAvgNAgQgHAMgJAKQggAlg/AGIgiAEIABAyIgLB4IhnAGgA1KiqIgMACIACBKIAXA+IAHgBIALgCQAdgEATgMIAHgKQALgXAAgjIgCgGQgJgdgXgQIgGgEIgMAAQgTAAgaAEgEgnxADDIgIh6IgCk7IDugKIAGBMIh2AFIAFBOIB8AGIAABYIh1AKIgCBfIB3AGIgFBXIgzACgAcpDFIgNimIgCkRIDxgMIAHBQIh0AGIAGBJIB3AQIAABZIh7AJIgOC2gAlaDIIgEAAQgogDgcgQQgdgQgSgdQgMgWgGgkQgFgmgBhNQAEhMAJglQAHgjAOgVQAUgcAegPQAdgNAngDQBMACAlAoQAmAnAFBMIhYAJQgBgngJgOQgJgQgVABQgXACgIAWQgJAZABBcIAAAJIAAAQQgDBTAFAWQAGAWAWABQAVABAHgPQAGgOgBgpIgCgRIBYAAIAAAHIgBANQgEBJglAiQgjAghCAAIgEAAgEAhlgBJIAAhIIhNgJIgBhYIDxgNIAjABIADAnIBVgNIAJAjIAAgWIBXgNIABADIAWBTIACBgIhYAOIgBgDIgIgfIAAAUIhXAOIgBgDIgWhTIgBg0IhCADIADAfIACDzIgHBVIhpAGgA5XgwIgBjCIBrgHIAeD1IACC/IhrAIgAUoDAQg+gEgggjQgfgkgBhCIAAgBIABgIIACgPIBdgEIABALQACAhAKANQAJAMAUAAQAQgBAJgLQAKgLABgTQABgTgOgSQgJgKgQgJIgOgKQg2glgKgSQgJgOgEgSQgEgSACgVQADgrAWgbIALgLQAfgaA4ADQA7AEAfAiQAfAhABA8IAAABIgBAIIgBAHIhbAEIgBgFQgBgagKgNQgJgNgRAAQgOABgKAKQgJALgBAQQgCAZAkAcIAIAFIAMAIIAKAGQAvAeALAUQAHANAEASQADARgCAUQgDA8gkAfIgBABQgdAZgwAAIgOgBgApJhCIAAAUIhYAOIgBgDIgVhTIgChhIBXgNIAJAjIAAgWIBYgNIABADIAVBTIACBgIhXAOg");
	this.shape_129.setTransform(277,348.1);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#000099").s().p("Egh2AC/IgPABIgNAAQgfAAgXgJQgYgJgRgRQgUgXgJgkQgJgjAAhcQABhMAFgjQAGgjAMgTQASgcAdgOQAdgMAqAAQApAAAdAMQAcAOASAcQAOAUAFAlQAGAlAABHQAABdgLAnQgJAmgaATIAiAlIg4A1gEgipgCZQgJASAABFIAABKQAABFAJASQAIAQAYABQAWgBAJgQQAIgSAAhFIAAhKQAAhGgIgRQgJgQgXgBQgWAAgJARgAZWCxQgcgNgTgcQgMgTgGgkQgFgigBhMQABhMAFgiQAGgjAMgUQATgcAcgOQAegNApAAQApAAAdANQAdAOASAcQANAUAGAjQAFAiAABMQAABLgFAjQgGAjgNAUQgSAcgdANQgdAOgpAAQgpAAgegOgAZ9iZQgJASAABFIAABKQAABFAJASQAJAQAXABQAXgBAIgQQAJgSAAhFIAAhKQAAhGgJgRQgJgQgWgBQgYAAgIARgAUECeQggggAAhBIABgIIAAgOIBaAAIAAALQABAkAIANQAIAOAUAAQAPAAAJgKQAJgLAAgSQAAgfgsgcIgLgIIgFgCQg7gogOgUQgLgPgGgTQgEgUAAgWQgBg4AggfQAggfA5AAQA8AAAgAeQAhAfAAA7IAAAHIgBAHIhYAAIAAgFQAAgcgJgPQgIgNgRAAQgOAAgIAKQgJAKAAAQQAAAaAwAfIAOAIIALAIQAzAgAOAXQAJANAGATQAEASAAAVQAABAggAiQghAig+AAQhAAAggghgAIGCzQgbgNgPgbQgMgTgDgfQgFgegBhHIAAjjIBkAAIAAEHQAAA8AIAOQAHAOATAAQAUAAAHgPQAHgPAAg6IAAkHIBiAAIAAEMQAAAtgEAXQgDAWgKARQgPAdgcANQgdANgtAAQgqAAgbgMgAC4CxQgdgNgSgcQgNgTgFgkQgGgiAAhMQAAhMAGgiQAFgjANgUQASgcAdgOQAegNAoAAQAqAAAdANQAdAOARAcQANAUAGAjQAGAiAABMQAABLgGAjQgGAjgNAUQgRAcgdANQgdAOgqAAQgoAAgegOgADeiZQgIASgBBFIAABKQABBFAIASQAJAQAXABQAXgBAJgQQAJgSAAhFIAAhKQAAhGgJgRQgJgQgXgBQgXAAgJARgAmaCxQgcgNgTgcQgMgTgGgkQgFgigBhMQABhMAFgiQAGgjAMgUQASgcAdgOQAdgNAoAAQBFAAAjAnQAiAlAABNIAAAEIhfAAQAAgrgKgRQgKgRgVgBQgYAAgJAWQgIAUgBBbIAAAIQABBhAIAWQAJAWAYAAQAWAAAJgQQAJgQgBgsIAAgSIBhAAIABAIIAAAOQAABMgiAmQghAlhGAAQgpAAgegOgAxNCxQgdgNgTgcQgMgTgFgkQgGgiAAhMQAAhMAGgiQAFgjAMgUQATgcAdgOQAdgNApAAQAqAAAdANQAdAOARAcQANAUAGAjQAFAiABBMQgBBLgFAjQgGAjgNAUQgRAcgdANQgdAOgqAAQgpAAgdgOgAwmiZQgJASAABFIAABKQAABFAJASQAIAQAXABQAXgBAJgQQAJgSAAhFIAAhKQAAhGgJgRQgJgQgXgBQgXAAgIARgA+BCzQgagNgQgbQgMgTgDgfQgGgeAAhHIAAjjIBkAAIAAEHQAAA8AIAOQAHAOATAAQAUAAAHgPQAHgPABg6IAAkHIBhAAIAAEMQAAAtgEAXQgDAWgKARQgPAdgcANQgdANgtAAQgqAAgbgMgEAi5AC1IAAlSIhPAAIAAhSIEFAAIAABSIhTAAIAAFSgAdmC1IAAmkIDmAAIAABSIiDAAIAABMIB6AAIAABUIh6AAIAACygAMVC1IAAmkIBtAAQA8AAAcALQAdAKARAZQAOASAFAfQAFAfAABUQAABUgFAfQgFAegOATQgRAZgdALQgcAKg8AAgAN4ihIAAEJIAGAAIAJAAQAPAAALgDQALgFAHgKQAFgIACgVQACgWAAg/IAAgTQAAg0gCgTQgCgTgFgHQgGgIgLgFQgLgEgRAAIgIAAIgGAAgAiKC1IAAmkIBiAAIAAFPIB2AAIAABVgA3KC1IAAmkIBhAAQA1AAAaAEQAZAFARALQAYAPAMAcQANAcAAAmQgBBAghAiQgjAfhAABIgjAAIAAChgA1ng6IAFABIAIAAQAdAAANgOQAOgOAAgeQAAgZgPgMQgNgLghAAIgIAAgA5yC1IAAmkIBjAAIAAGkgEgovAC1IAAmkIDlAAIAABSIiDAAIAABNIB4AAIAABUIh4AAIAABbICDAAIAABWgEAnqgA6IAAiZIBHAAIAACZgEAmHgA6IAAiZIBHAAIAACZgApUg6IAAiZIBGAAIAACZgAq3g6IAAiZIBGAAIAACZg");
	this.shape_130.setTransform(278.3,355.9);

	this.text = new cjs.Text("EQUIPO \"CLOUD SOFT\"", "56px 'Swis721 BlkCn BT'", "#000099");
	this.text.lineHeight = 69;
	this.text.lineWidth = 531;
	this.text.parent = this;
	this.text.setTransform(14.1,320.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.text}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(311.4,235.1,100,100.1);
p.frameBounds = [rect, new cjs.Rectangle(316.1,238.4,101.6,99.9), new cjs.Rectangle(320.7,241.7,103.2,99.9), new cjs.Rectangle(325.4,244.9,104.8,99.9), new cjs.Rectangle(330.1,248.1,106.3,100), new cjs.Rectangle(334.8,251.2,108,100.1), new cjs.Rectangle(339.5,254.3,109.5,100.4), new cjs.Rectangle(344.2,257.3,111,100.8), new cjs.Rectangle(348.9,260.4,112.7,101.2), new cjs.Rectangle(353.5,263.3,114.3,101.8), new cjs.Rectangle(358.2,266.2,115.9,102.5), new cjs.Rectangle(362.9,269,117.4,103.3), new cjs.Rectangle(367.6,271.8,119,104.1), new cjs.Rectangle(372.3,274.5,120.6,105.2), new cjs.Rectangle(377,277.1,122.2,106.4), new cjs.Rectangle(381.6,279.7,123.8,107.7), new cjs.Rectangle(386.3,282.2,125.4,109.1), new cjs.Rectangle(391,284.8,126.9,110.4), new cjs.Rectangle(395.7,287.3,128.5,111.8), new cjs.Rectangle(400.4,289.9,130.1,113.2), new cjs.Rectangle(405.1,292.4,131.6,114.6), new cjs.Rectangle(409.8,294.9,133.2,116), new cjs.Rectangle(414.4,297.5,134.9,117.3), new cjs.Rectangle(419.1,300,136.4,118.7), new cjs.Rectangle(423.8,302.5,138,120.1), new cjs.Rectangle(428.5,305.1,139.6,121.5), new cjs.Rectangle(433.2,307.6,141.2,122.8), new cjs.Rectangle(437.9,310.1,142.7,124.3), new cjs.Rectangle(442.6,312.7,144.3,125.6), new cjs.Rectangle(447.2,315.2,145.9,127), new cjs.Rectangle(451.9,317.7,147.5,128.4), new cjs.Rectangle(456.6,320.3,149,129.8), new cjs.Rectangle(461.3,322.8,150.6,131.1), new cjs.Rectangle(466,325.4,152.3,132.5), new cjs.Rectangle(470.7,327.9,153.8,133.9), new cjs.Rectangle(475.3,330.4,155.4,135.2), new cjs.Rectangle(480,333,157,136.6), new cjs.Rectangle(484.7,335.5,158.6,138.1), new cjs.Rectangle(489.4,338,160.2,139.4), new cjs.Rectangle(494.1,340.6,161.8,140.8), new cjs.Rectangle(498.8,343.1,163.3,142.2), new cjs.Rectangle(503.5,345.6,164.9,143.5), new cjs.Rectangle(508.1,348.2,166.5,144.9), new cjs.Rectangle(512.8,350.7,168.1,146.3), new cjs.Rectangle(517.5,353.2,169.6,147.7), new cjs.Rectangle(522.2,355.8,171.2,149.1), new cjs.Rectangle(526.9,358.3,172.8,150.5), new cjs.Rectangle(531.6,360.8,174.3,151.8), new cjs.Rectangle(536.2,363.4,176,153.2), new cjs.Rectangle(540.9,365.9,177.6,154.6), new cjs.Rectangle(545.6,368.5,179.1,156), new cjs.Rectangle(550.3,371,180.7,157.4), new cjs.Rectangle(555,373.5,182.3,158.8), new cjs.Rectangle(559.7,376.1,183.9,160.1), new cjs.Rectangle(564.4,378.6,185.5,161.5), new cjs.Rectangle(569,381.1,187.1,162.9), new cjs.Rectangle(573.7,383.7,188.6,164.2), new cjs.Rectangle(578.4,386.2,190.2,165.6), new cjs.Rectangle(583.1,388.7,191.8,167), new cjs.Rectangle(587.8,391.3,193.4,168.4), new cjs.Rectangle(592.5,393.8,195,169.8), new cjs.Rectangle(597.2,396.3,196.5,171.1), new cjs.Rectangle(601.8,398.9,198.2,172.5), new cjs.Rectangle(606.5,401.4,199.7,173.9), new cjs.Rectangle(611.2,404,201.3,175.3), new cjs.Rectangle(615.9,406.5,202.9,176.7), new cjs.Rectangle(620.6,409,204.5,178.1), new cjs.Rectangle(614.1,403,202.7,175.7), new cjs.Rectangle(607.6,395.9,200.8,175.5), new cjs.Rectangle(601.1,388.9,199,175.2), new cjs.Rectangle(594.6,381.8,197.2,175), new cjs.Rectangle(588.1,374.7,195.4,174.7), new cjs.Rectangle(581.6,367.7,193.6,174.5), new cjs.Rectangle(575.1,360.6,191.8,174.2), new cjs.Rectangle(568.6,353.5,190,174), new cjs.Rectangle(562.1,346.4,188.1,173.7), new cjs.Rectangle(555.6,339.4,186.3,173.5), new cjs.Rectangle(549.1,332.3,184.6,173.2), new cjs.Rectangle(542.6,325.2,182.8,173), new cjs.Rectangle(536.1,318.2,180.9,172.7), new cjs.Rectangle(529.6,311.1,179.1,172.5), new cjs.Rectangle(523,304,177.4,172.1), new cjs.Rectangle(516.5,296.9,175.6,172), new cjs.Rectangle(510,289.9,173.7,171.7), new cjs.Rectangle(503.5,282.8,171.9,171.4), new cjs.Rectangle(497,275.7,170.1,171.1), new cjs.Rectangle(490.5,268.7,168.3,170.9), new cjs.Rectangle(484,261.6,166.5,170.6), new cjs.Rectangle(477.5,254.5,164.6,170.4), new cjs.Rectangle(471,247.4,162.9,170.2), new cjs.Rectangle(464.5,240.4,161.1,169.9), new cjs.Rectangle(458,233.3,159.3,169.6), new cjs.Rectangle(451.5,226.2,157.5,169.4), new cjs.Rectangle(445,219.2,155.6,169.1), new cjs.Rectangle(438.5,212.1,153.8,168.9), rect=new cjs.Rectangle(432,205,152,168.6), rect, new cjs.Rectangle(428,214.3,162.6,165.1), new cjs.Rectangle(424.1,223.6,173.1,161.7), new cjs.Rectangle(420.1,233,183.7,158.1), new cjs.Rectangle(416.1,242.3,194.3,154.7), new cjs.Rectangle(412.1,251.6,204.8,151.2), new cjs.Rectangle(408.1,260.9,215.4,147.7), new cjs.Rectangle(404.1,270.2,226,144.3), new cjs.Rectangle(400.1,279.5,236.5,140.7), new cjs.Rectangle(396.1,288.8,247.1,137.3), new cjs.Rectangle(392.2,298.1,257.6,133.8), new cjs.Rectangle(388.2,307.4,268.2,130.3), new cjs.Rectangle(384.2,316.7,278.8,126.8), new cjs.Rectangle(380.2,326.1,289.4,123.3), new cjs.Rectangle(376.2,335.4,299.9,119.9), new cjs.Rectangle(372.2,344.7,310.5,116.4), new cjs.Rectangle(368.2,354,321,112.9), new cjs.Rectangle(364.2,363.3,331.6,109.4), new cjs.Rectangle(360.3,372.6,342.1,105.9), new cjs.Rectangle(356.3,381.9,352.7,102.4), new cjs.Rectangle(352.3,391.2,363.3,98.9), new cjs.Rectangle(348.3,400.5,373.9,95.4), new cjs.Rectangle(344.3,409.8,384.3,92), new cjs.Rectangle(340.3,419.2,395,88.5), new cjs.Rectangle(336.3,428.5,405.5,85), new cjs.Rectangle(332.3,437.8,416.1,81.6), new cjs.Rectangle(328.4,447.1,426.6,78.4), new cjs.Rectangle(324.4,456.4,437.2,75.3), new cjs.Rectangle(320.4,465.7,447.8,72.1), new cjs.Rectangle(316.4,475,458.4,68.9), new cjs.Rectangle(312.4,484.3,468.9,65.8), new cjs.Rectangle(308.4,493.6,479.5,62.6), new cjs.Rectangle(304.4,502.9,490,59.5), new cjs.Rectangle(300.4,512.3,500.6,56.3), new cjs.Rectangle(296.5,521.6,511.2,53.1), new cjs.Rectangle(292.5,530.9,521.7,50), new cjs.Rectangle(287.1,518.1,535,76.3)];
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;