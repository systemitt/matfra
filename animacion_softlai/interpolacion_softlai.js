(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



// stage content:
(lib.interpolacion_softlai = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0066FF").ss(1,1,1).p("AKKAAQAAB7goBrQgvB+hoBnQi+C/kNAAQkMAAi/i/QiAiAgqilQgUhPAAhXQAAhWAUhPQAqilCAiAQC/i/EMAAQENAAC+C/QBoBnAvB+QAoBrAAB6g");
	this.shape.setTransform(75.1,311.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0066FF").s().p("AnLHMQiAiBgqilQgUhPAAhXQAAhWAUhPQAqilCAiAQC/i/EMAAQENAAC/C/QBnBnAvB+QAoBrAAB6QAAB7goBrQgvB+hnBoQi/C+kNAAQkMAAi/i+g");
	this.shape_1.setTransform(75.1,311.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#0D61F2").ss(1,1,1).p("AqDgbQAFhSAXhLQAxicCDh6QDDi1EMAAQD/AOC0DDQBiBpAtB/QAmBsAAB6QgHB2gsBkQg0B4hpBhQjDC1kMAAQj/gPi0jCQh7iDgnilQgThQAAhWg");
	this.shape_2.setTransform(96.1,298.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0D61F2").s().p("AnOGzQh7iDgnilQgShQAAhWQAEhSAYhLQAwicCEh6QDCi1ELAAQEAAOC0DDQBiBpAtB/QAmBsAAB6QgHB2gsBkQg1B4hoBhQjDC1kMAAQj/gPi0jCg");
	this.shape_3.setTransform(96.1,298.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B5BE4").ss(1,1,1).p("Ap9g3QAKhOAbhGQA3iUCGhzQDHirELAAQDyAcCqDHQBcBrArCAQAkBsAAB7QgOBwgwBdQg5ByhrBcQjHCrkMAAQjxgdiqjGQh0iGglimQgShQAAhWg");
	this.shape_4.setTransform(117.2,286.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#1B5BE4").s().p("AnSGbQh0iGglimQgShPAAhXQAKhNAbhHQA4iUCFhzQDHiqELAAQDxAcCrDGQBcBsArB/QAkBsgBB7QgNBwgwBeQg6BxhqBcQjHCrkMgBQjxgciqjGg");
	this.shape_5.setTransform(117.2,286.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#2856D7").ss(1,1,1).p("Ap2hSQAOhKAehCQA/iLCJhsQDKihEMAAQDiAqChDKQBXBtAoCCQAhBtAAB6QgTBpg1BZQg+BqhtBXQjLCgkLAAQjjgqigjKQhtiIgjinQgRhQAAhWg");
	this.shape_6.setTransform(138.2,274);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2856D7").s().p("AnVGDQhtiIgjinQgRhQAAhXQANhJAfhCQA/iLCJhtQDKigELAAQDjAqCgDKQBYBtAnCCQAiBtAAB6QgTBog1BZQg/BrhtBXQjKCgkLAAQjjgqigjKg");
	this.shape_7.setTransform(138.2,274);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#3651C9").ss(1,1,1).p("ApwhvQAShEAig/QBGiBCMhmQDOiXELAAQDVA4CWDOQBSBwAlCCQAfBuAAB5QgZBjg5BTQhEBkhvBRQjOCXkMAAQjVg4iWjOQhliLghioQgQhPAAhXg");
	this.shape_8.setTransform(159.3,261.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3651C9").s().p("AnaFrQhliLghioQgPhPAAhXIAAgBQAShEAhg/QBHiBCLhmQDOiXELAAQDVA4CXDOQBRBwAlCCQAgBugBB5QgZBjg5BTQhDBkhwBRQjOCXkLAAQjWg4iWjOg");
	this.shape_9.setTransform(159.3,261.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#434BBC").ss(1,1,1).p("ApqiKQAXhAAmg6QBNh6COhfQDSiNELAAQDHBGCMDTQBMByAjCDQAeBtAAB6QghBdg9BNQhIBdhyBMQjSCNkLAAQjHhGiMjSQhfiOgfioQgPhQAAhXg");
	this.shape_10.setTransform(180.3,249.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#434BBC").s().p("AndFTQhfiOgeioQgPhQgBhXIAAAAQAYhAAlg7QBNh5COhfQDSiNELABQDGBGCNDSQBMByAjCCQAeBuAAB7QghBbg9BOQhJBdhxBMQjSCMkLAAQjHhGiMjRg");
	this.shape_11.setTransform(180.3,249.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#5146AE").ss(1,1,1).p("ApjinQAbg7Apg2QBUhxCRhYQDWiCELAAQC4BUCDDWQBHBzAgCEQAbBuAAB7QgnBWhBBHQhOBXhzBGQjWCDkLAAQi5hUiCjVQhYiRgdipQgNhRAAhWg");
	this.shape_12.setTransform(201.4,236.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#5146AE").s().p("AnhE7QhYiRgdipQgNhQAAhXIAAAAQAbg8Apg2QBVhwCQhZQDWiCELAAQC4BUCDDWQBHBzAgCFQAbBtAAB7QgnBWhBBHQhOBXhzBHQjWCCkLAAQi4hUiDjVg");
	this.shape_13.setTransform(201.4,236.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#5E40A1").ss(1,1,1).p("ApdjCQAgg3AsgyQBchoCThSQDah4ELAAQCqBiB4DaQBBB2AeCFQAaBuAAB7QgtBPhFBCQhUBQh1BBQjaB5kLAAQirhih4jZQhRiUgaioQgNhSAAhXg");
	this.shape_14.setTransform(222.4,224.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#5E40A1").s().p("AnkEjQhSiUgaioQgNhSAAhXIAAAAQAgg3AtgyQBbhoCThSQDah4ELAAQCqBiB5DaQBAB2AeCFQAaBuAAB7QgtBPhGBCQhTBQh1BBQjaB5kLAAQiqhih4jZg");
	this.shape_15.setTransform(222.4,224.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#6B3B94").ss(1,1,1).p("ApWjeQAkgzAwgtQBjhgCVhKQDfhvEKAAQCcBwBuDeQA8B4AcCGQAWBvAAB7QgzBIhKA9QhXBJh4A8QjeBukLAAQichvhujdQhLiWgYiqQgLhSAAhXg");
	this.shape_16.setTransform(243.5,212.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#6B3B94").s().p("AnoELQhLiWgYiqQgLhSAAhXIAAAAQAkgyAwguQBihfCWhLQDfhuEKAAQCcBvBuDfQA8B4AcCFQAWBwAAB6QgzBJhKA9QhXBJh4A7QjeBukKABQidhwhujdg");
	this.shape_17.setTransform(243.5,212.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#793686").ss(1,1,1).p("ApQj6QApguAzgpQBqhXCYhEQDjhkEKAAQCOB+BkDiQA2B6AZCFQAVByAAB6Qg5BChOA3QheBDh5A2QjjBkkKAAQiOh9hkjhQhEiZgWiqQgKhTAAhWg");
	this.shape_18.setTransform(264.5,199.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#793686").s().p("AnsDzQhEiZgViqQgLhTAAhWIAAgBQApguA0gpQBphXCYhEQDjhkEKAAQCNB+BlDiQA2B6AZCFQAVByAAB6Qg5BChPA3QhdBDh5A2QjjBkkJAAQiPh9hkjhg");
	this.shape_19.setTransform(264.5,199.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#863079").ss(1,1,1).p("ApKkWQAtgpA3gmQByhOCag9QDlhaELAAQCACMBaDmQAyB8AWCHQATBxAAB6QhAA8hSAyQhiA8h9AxQjkBakMAAQiAiLhajlQg9icgUirQgJhTAAhXg");
	this.shape_20.setTransform(285.6,187.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#863079").s().p("AnwDbQg8icgVirQgJhTAAhXIAAAAQAtgpA3gmQByhOCag9QDlhaELAAQCBCMBZDmQAxB8AXCHQATBxAAB6QhAA8hTAyQhhA8h8AxQjlBakMAAQh/iLhbjlg");
	this.shape_21.setTransform(285.6,187.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#942B6B").ss(1,1,1).p("ApEkyQAygkA7giQB4hFCdg3QDphQELAAQByCaBQDqQArB+AUCIQARByAAB6QhGA1hXAsQhmA2h/AsQjpBQkLAAQhyiZhQjqQg2idgRisQgJhTAAhYg");
	this.shape_22.setTransform(306.6,175);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#942B6B").s().p("AnzDDQg3iegRisQgJhUABhWIAAgBQAxgkA7giQB4hFCeg3QDohQELAAQByCbBRDpQAqB+AVCIQARBzAAB5QhHA1hXAtQhmA1h+AsQjqBPkLAAQhyiYhPjpg");
	this.shape_23.setTransform(306.6,175);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#A1265E").ss(1,1,1).p("Ao9lOQA3ggA9gdQCAg8CggwQDthGELAAQBjCoBGDtQAmCBASCIQAOB0AAB5QhMAvhbAnQhsAuiAAmQjuBGkKAAQhkinhGjsQgvihgQitQgHhTAAhXg");
	this.shape_24.setTransform(327.7,162.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#A1265E").s().p("An3CrQgwihgPitQgHhTAAhXIAAgBQA3ggA9gdQCAg8CfgwQDthGELAAQBkCoBGDtQAmCBARCIQAPB0AAB5QhMAvhcAnQhrAuiBAmQjtBGkKAAQhkinhGjsg");
	this.shape_25.setTransform(327.7,162.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#AE2051").ss(1,1,1).p("Ao3lqQA8gbBBgZQCHg0CigpQDxg8ELAAQBVC2A8DyQAgCDAPCIQANB1AAB5QhTAohgAiQhwAoiCAgQjyA8kKAAQhWi1g8jwQgoijgNiuQgHhUAAhXg");
	this.shape_26.setTransform(348.7,150.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#AE2051").s().p("An7CSQgpiigMiuQgHhUAAhXIAAAAQA8gcBBgZQCHg0CigoQDwg9ELAAQBWC2A7DyQAhCDAPCJQANBzAAB6QhTAohgAhQhwAoiDAhQjxA8kKAAQhWi1g8jxg");
	this.shape_27.setTransform(348.7,150.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#BC1B43").ss(1,1,1).p("AowmGQA/gXBFgVQCOgrCkgiQD2gyELAAQBGDEAyD1QAcCFAMCLQAKB0AAB5QhYAihkAcQh2AhiEAcQj2AykKAAQhHjDgyj0QgjilgKiwQgFhVAAhWg");
	this.shape_28.setTransform(369.8,137.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#BC1B43").s().p("An+B6QgjilgKivQgFhVgBhWIAAgBQBAgWBFgWQCOgrCkghQD2gzEKAAQBHDFAzD0QAbCEAMCMQAKB0AAB5QhYAihkAcQh2AiiFAbQj0AxkLAAQhHjCgyj1g");
	this.shape_29.setTransform(369.8,137.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#C91536").ss(1,1,1).p("AormhQBFgTBJgQQCUgjCngbQD6goEKAAQA5DSAoD5QAWCGAKCMQAJB1AAB6QhhAbhnAWQh7AaiHAWQj5AokKAAQg5jQgoj5QgcingIiwQgFhVAAhXg");
	this.shape_30.setTransform(390.8,125.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#C91536").s().p("AoCBiQgbingJiwQgEhVgBhXIAAAAQBFgTBJgQQCUgjCogbQD5goEKAAQA5DSAoD5QAWCGAKCMQAJB1gBB6QhgAbhnAWQh7AaiGAWQj6AokKAAQg5jQgoj5g");
	this.shape_31.setTransform(390.8,125.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#D71028").ss(1,1,1).p("Aokm+QBJgNBMgNQCcgaCpgUQD+geEKAAQArDgAeD9QAQCIAICOQAGB1AAB5QhmAVhsARQiAAUiJAQQj9AekKAAQgrjegej9QgUiqgHixQgDhVAAhXg");
	this.shape_32.setTransform(411.9,113.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#D71028").s().p("AoGBKQgUiqgHixQgDhVAAhXIAAgBQBJgNBMgNQCcgaCpgUQD+geEKAAQArDgAeD+QAQCHAICOQAGB1AAB5QhmAVhsAQQiAAViJAQQj8AekLAAQgrjegej9g");
	this.shape_33.setTransform(411.9,113.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#E40B1B").ss(1,1,1).p("AoenaQBOgJBPgIQCjgRCtgOQEBgUEKAAQAdDuAUEBQALCKAFCOQAEB4AAB4QhsAOhxALQiFANiLAMQkBAUkJAAQgejtgUkAQgNitgEixQgDhWAAhXg");
	this.shape_34.setTransform(432.9,100.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E40B1B").s().p("AoKAyQgNisgFiyQgChWAAhWIAAgBICdgSQCkgRCsgOQEBgTEKgBQAcDvAVEAQAKCLAFCNQAFB4AAB4QhtAOhwAMQiFANiLALQkBATkJABQgdjtgVkAg");
	this.shape_35.setTransform(432.9,100.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#F2050D").ss(1,1,1).p("AoYn1QBSgFBTgEQCrgJCvgGQEFgKEKAAQAOD8AKEFQAFCMADCPQACB3AAB5QhzAIh0AFQiKAHiNAFQkFAKkJAAQgQj6gKkEQgGivgCizQgChWAAhWg");
	this.shape_36.setTransform(454,88.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#F2050D").s().p("AoNAaQgHivgCizQgChWABhWIAAgBICkgJQCrgJCvgGQEFgKEKAAQAOD8ALEFQAECMADCPQACB3ABB5Qh0AIh0AFIkXAMQkFAKkJAAQgPj6gKkEg");
	this.shape_37.setTransform(454,88.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FF0000").ss(1,1,1).p("AoRoRIQjAAIAAQjIwjAAg");
	this.shape_38.setTransform(475,76.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF0000").s().p("AoRISIAAwjIQjAAIAAQjg");
	this.shape_39.setTransform(475,76.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FF000A").ss(1,1,1).p("AoUn5IM+gbIDSAEIAZJoIgCGkImVATIp3AGIgJjIg");
	this.shape_40.setTransform(473.6,86.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF000A").s().p("AoCFMIgStFIM+gbIDSAFIAZJmIgDGlImVATIp2AGg");
	this.shape_41.setTransform(473.6,86.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FF0014").ss(1,1,1).p("AoXnfIMtg3IDQAIIAyJTIgFGjImCAkIpyALIgSi8g");
	this.shape_42.setTransform(472.1,96.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF0014").s().p("AnzFbIgks6IMtg3IDQAIIAyJTIgFGjImCAkIpyALg");
	this.shape_43.setTransform(472.1,96.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FF001F").ss(1,1,1).p("AoZnHIMbhRIDNAMIBLI/IgGGfIlwA3IptAQIgbiug");
	this.shape_44.setTransform(470.7,107.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF001F").s().p("AnkFqIg1sxIMbhRIDNAMIBLI/IgGGgIlvA3IpuAPg");
	this.shape_45.setTransform(470.7,107.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FF0029").ss(1,1,1).p("AocmuIMKhsIDLAPIBkIrIgIGdIldBJIppAVIgkiig");
	this.shape_46.setTransform(469.3,117.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FF0029").s().p("AnVF5IhIsnIMLhsIDLAPIBlIrIgJGdIlcBJIpqAVg");
	this.shape_47.setTransform(469.3,117.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FF0033").ss(1,1,1).p("AofmWIL5iHIDJAUIB9IXIgLGaIlJBbIpkAbIguiXg");
	this.shape_48.setTransform(467.9,127.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FF0033").s().p("AnGGHIhasdIL6iHIDIAUIB+IXIgLGaIlKBbIpkAbg");
	this.shape_49.setTransform(467.9,127.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FF003D").ss(1,1,1).p("Aojl8ILpijIDHAXICXIEIgOGXIk3BuIpfAfIg3iKg");
	this.shape_50.setTransform(466.4,138.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF003D").s().p("Am3GWIhrsSILoijIDHAXICXIEIgOGXIk3BuIpfAfg");
	this.shape_51.setTransform(466.4,138.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FF0047").ss(1,1,1).p("AomlkILYi9IDEAbICxHwIgRGTIkjCBIpbAlIhAh+g");
	this.shape_52.setTransform(465,148.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FF0047").s().p("AmoGlIh+sJILYi+IDEAcICxHvIgRGUIkjCBIpbAlg");
	this.shape_53.setTransform(465,148.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FF0052").ss(1,1,1).p("AoolLILGjZIDCAgIDJHbIgSGRIkRCTIpWAqIhJhxg");
	this.shape_54.setTransform(463.5,158.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF0052").s().p("AmZG0IiPr/ILGjZIDCAgIDJHbIgSGRIkRCTIpWAqg");
	this.shape_55.setTransform(463.5,158.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FF005C").ss(1,1,1).p("AorkzIK1jzIDAAjIDiHIIgVGOIj9ClIpTAvIhRhkg");
	this.shape_56.setTransform(462.1,169.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF005C").s().p("AmKHCIihr1IK0jzIDBAjIDiHIIgVGOIj9ClIpTAvg");
	this.shape_57.setTransform(462.1,169.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FF0066").ss(1,1,1).p("AoukaIKkkOIC+AnID7GzIgXGLIjqC4IpOA0IhbhYg");
	this.shape_58.setTransform(460.7,179.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FF0066").s().p("Al7HRIizrrIKkkOIC+AnID7GzIgXGLIjrC4IpNA0g");
	this.shape_59.setTransform(460.7,179.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FF0070").ss(1,1,1).p("AoxkBIKTkqIC8ArIEUGgIgZGIIjYDKIpIA6IhlhNg");
	this.shape_60.setTransform(459.2,189.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF0070").s().p("AlsHfIjFrgIKTkqIC7ArIEVGgIgaGIIjXDKIpJA6g");
	this.shape_61.setTransform(459.2,189.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FF007A").ss(1,1,1).p("Ao0joIKClFIC5AvIEuGMIgcGFIjEDcIpFA/IhthAg");
	this.shape_62.setTransform(457.8,200.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FF007A").s().p("AldHuIjXrXIKClEIC5AvIEuGMIgcGFIjEDcIpFA/g");
	this.shape_63.setTransform(457.8,200.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FF0085").ss(1,1,1).p("Ao3jQIJxlfIC3AzIFHF3IgeGDIiyDuIpABEIh2gzg");
	this.shape_64.setTransform(456.4,210.5);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FF0085").s().p("AlOH9IjorNIJvlfIC4AzIFGF3IgdGDIiyDuIpABEg");
	this.shape_65.setTransform(456.4,210.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FF008F").ss(1,1,1).p("Ao6i3IJgl7IC1A3IFfFkIggF/IieEBIo8BKIh/gng");
	this.shape_66.setTransform(454.9,220.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FF008F").s().p("Ak/IMIj6rDIJfl7IC1A3IFfFkIgfF/IifEBIo7BKg");
	this.shape_67.setTransform(454.9,220.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FF0099").ss(1,1,1).p("Ao9ieIJPmWICyA7IF6FPIgjF9IiMETIo3BPIiIgbg");
	this.shape_68.setTransform(453.5,231.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF0099").s().p("AkwIaIkMq4IJNmWICzA7IF5FPIgiF9IiMETIo2BPg");
	this.shape_69.setTransform(453.5,231.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FF00A3").ss(1,1,1).p("Ao/iFII+myICwBAIGSE7IglF6Ih5EmIoyBUIiSgPg");
	this.shape_70.setTransform(452.1,241.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FF00A3").s().p("AkhIpIkfquII+mxICxA/IGRE7IglF6Ih4EmIozBUg");
	this.shape_71.setTransform(452.1,241.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FF00AD").ss(1,1,1).p("ApDhtIItnMICuBDIGrEnIgnF3IhmE4IotBaIibgDg");
	this.shape_72.setTransform(450.6,251.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FF00AD").s().p("AkSI3IkwqkIItnMICtBDIGsEnIgoF3IhlE4IouBag");
	this.shape_73.setTransform(450.6,251.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FF00B8").ss(1,1,1).p("ApFhaIIcnmICrBGIHEETIgpF1IhTFKIopBfIikAKg");
	this.shape_74.setTransform(449.2,262.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FF00B8").s().p("ApFhaIIcnmICrBGIHEETIgpF1IhTFKIopBfIikAKg");
	this.shape_75.setTransform(449.2,262.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FF00C2").ss(1,1,1).p("ApIhHIIKoCICqBLIHdD/IgsFyIg/FcIomBkIisAXg");
	this.shape_76.setTransform(447.8,273.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FF00C2").s().p("ApIhHIILoCICoBLIHeD/IgrFyIhAFcIomBkIisAXg");
	this.shape_77.setTransform(447.8,273.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FF00CC").ss(1,1,1).p("AHxHGIrWCMIlmqGIH5odIKeE5g");
	this.shape_78.setTransform(446.3,284.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FF00CC").s().p("ApLg0IH5odIKeE5IhbLeIrWCMg");
	this.shape_79.setTransform(446.3,284.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).to({state:[{t:this.shape_5},{t:this.shape_4}]},1).to({state:[{t:this.shape_7},{t:this.shape_6}]},1).to({state:[{t:this.shape_9},{t:this.shape_8}]},1).to({state:[{t:this.shape_11},{t:this.shape_10}]},1).to({state:[{t:this.shape_13},{t:this.shape_12}]},1).to({state:[{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_17},{t:this.shape_16}]},1).to({state:[{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_21},{t:this.shape_20}]},1).to({state:[{t:this.shape_23},{t:this.shape_22}]},1).to({state:[{t:this.shape_25},{t:this.shape_24}]},1).to({state:[{t:this.shape_27},{t:this.shape_26}]},1).to({state:[{t:this.shape_29},{t:this.shape_28}]},1).to({state:[{t:this.shape_31},{t:this.shape_30}]},1).to({state:[{t:this.shape_33},{t:this.shape_32}]},1).to({state:[{t:this.shape_35},{t:this.shape_34}]},1).to({state:[{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_39},{t:this.shape_38}]},1).to({state:[{t:this.shape_39},{t:this.shape_38}]},1).to({state:[{t:this.shape_39},{t:this.shape_38}]},1).to({state:[{t:this.shape_41},{t:this.shape_40}]},1).to({state:[{t:this.shape_43},{t:this.shape_42}]},1).to({state:[{t:this.shape_45},{t:this.shape_44}]},1).to({state:[{t:this.shape_47},{t:this.shape_46}]},1).to({state:[{t:this.shape_49},{t:this.shape_48}]},1).to({state:[{t:this.shape_51},{t:this.shape_50}]},1).to({state:[{t:this.shape_53},{t:this.shape_52}]},1).to({state:[{t:this.shape_55},{t:this.shape_54}]},1).to({state:[{t:this.shape_57},{t:this.shape_56}]},1).to({state:[{t:this.shape_59},{t:this.shape_58}]},1).to({state:[{t:this.shape_61},{t:this.shape_60}]},1).to({state:[{t:this.shape_63},{t:this.shape_62}]},1).to({state:[{t:this.shape_65},{t:this.shape_64}]},1).to({state:[{t:this.shape_67},{t:this.shape_66}]},1).to({state:[{t:this.shape_69},{t:this.shape_68}]},1).to({state:[{t:this.shape_71},{t:this.shape_70}]},1).to({state:[{t:this.shape_73},{t:this.shape_72}]},1).to({state:[{t:this.shape_75},{t:this.shape_74}]},1).to({state:[{t:this.shape_77},{t:this.shape_76}]},1).to({state:[{t:this.shape_79},{t:this.shape_78}]},1).to({state:[{t:this.shape_79},{t:this.shape_78}]},1).to({state:[{t:this.shape_79},{t:this.shape_78}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;