(function(lib, img, cjs, ss, an) {

    var p; // shortcut to reference prototypes
    lib.ssMetadata = [];


    // symbols:



    // stage content:
    (lib.circuloacuadro = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Capa 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#0066CC").s().p("AlxFyIAArjILjAAIAALjg");
        this.shape.setTransform(47.1, 50.1);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.rf(["#0368CD", "#0065CA"], [0, 1], 443.8, 296.3, 0, 443.8, 296.3, 43.6).s().p("AltFyIgDlyQgCi2AAi4IFpgCIF0gBIADAAIADFyQACC3AAC2IloADQi6ACi8gBIgCAAg");
        this.shape_1.setTransform(51.5, 53.1);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.rf(["#0569CD", "#0064C8"], [0, 1], 439.3, 293.3, 0, 439.3, 293.3, 43.6).s().p("AlqFzIgGlxQgCi1AAi2IFmgGQC5gDC7AAIADAAIAGFxQACC1AAC3IllAGQi6ACi8AAIgCAAg");
        this.shape_2.setTransform(56, 56);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.rf(["#086BCE", "#0063C6"], [0, 1], 434.8, 290.3, 0, 434.8, 290.3, 43.6).s().p("AlmFzQgFi1gEi5QgEi0AAi3QCwgFC0gEQC4gDC7AAIADAAQAGC1ADC5QAEC0AAC3QiwAFizAEQi6ADi7AAIgCAAg");
        this.shape_3.setTransform(60.5, 59);

        this.shape_4 = new cjs.Shape();
        this.shape_4.graphics.rf(["#0A6CCE", "#0062C4"], [0, 1], 430.3, 287.3, 0, 430.3, 287.3, 43.6).s().p("AljF0QgGi0gFi4QgFi0gBi2QCvgHCzgFQC4gFC6AAIADAAQAHC0AFC4QAFC0AAC2QiuAHiyAFQi5AFi8AAIgCAAg");
        this.shape_4.setTransform(65, 62);

        this.shape_5 = new cjs.Shape();
        this.shape_5.graphics.rf(["#0D6ECF", "#0061C2"], [0, 1], 425.8, 284.3, 0, 425.8, 284.3, 43.6).s().p("AlfF0QgJiygGi3QgGizAAi3QCsgICzgGQC3gGC7AAIACAAQAJCxAHC4QAGCzAAC2QisAJiyAGQi4AGi8AAIgCAAg");
        this.shape_5.setTransform(69.5, 65);

        this.shape_6 = new cjs.Shape();
        this.shape_6.graphics.rf(["#0F6FCF", "#0060C0"], [0, 1], 421.4, 281.3, 0, 421.4, 281.3, 43.5).s().p("AlbF0QgKivgIi3QgIiyAAi3IAAAAQCrgKCygIQC3gGC6AAIADAAQALCwAHC3QAICyAAC2QiqAKiyAIQi3AGi8AAIgCAAg");
        this.shape_6.setTransform(73.9, 68);

        this.shape_7 = new cjs.Shape();
        this.shape_7.graphics.rf(["#1271D0", "#005FBE"], [0, 1], 416.9, 278.3, 0, 416.9, 278.3, 43.5).s().p("AlXF1QgNiugIi3QgJixAAi2QCogMCygIQC2gJC6AAIADAAQANCuAJC3QAICxAAC2QioAMixAJQi3AIi7AAIgCAAg");
        this.shape_7.setTransform(78.4, 71);

        this.shape_8 = new cjs.Shape();
        this.shape_8.graphics.rf(["#1472D0", "#005EBC"], [0, 1], 412.4, 275.3, 0, 412.4, 275.3, 43.6).s().p("AlTF1QgPisgKi1QgKiyAAi1QCngOCxgKQC1gKC6AAIADAAQAPCtAKC1QAKCxAAC3QinANiwAJQi2ALi8gBIgBAAg");
        this.shape_8.setTransform(82.9, 74);

        this.shape_9 = new cjs.Shape();
        this.shape_9.graphics.rf(["#1774D1", "#005DBA"], [0, 1], 407.9, 272.3, 0, 407.9, 272.3, 43.6).s().p("AlQF2QgQiqgMi1QgLixABi2QCkgPCxgLQC1gLC6AAIADAAQAQCqALC1QALCxAAC2QikAPixALQi1ALi7AAIgCAAg");
        this.shape_9.setTransform(87.4, 77);

        this.shape_10 = new cjs.Shape();
        this.shape_10.graphics.rf(["#1A75D1", "#005CB8"], [0, 1], 403.4, 269.3, 0, 403.4, 269.3, 43.6).s().p("AlMF2QgSiogNi0QgMiwAAi2QCjgRCwgMQC0gMC6AAIADAAQASCoANC0QAMCwAAC2QijARivAMQi1AMi7AAIgCAAg");
        this.shape_10.setTransform(91.9, 80);

        this.shape_11 = new cjs.Shape();
        this.shape_11.graphics.rf(["#1C77D2", "#005BB6"], [0, 1], 399, 266.3, 0, 399, 266.3, 43.6).s().p("AlJF3QgTingOi0QgOivAAi1QCigTCvgNQC0gOC5AAIAEAAQATCnAOCzQAOCwAAC1QihATivANQi0AOi7AAIgDAAg");
        this.shape_11.setTransform(96.4, 83);

        this.shape_12 = new cjs.Shape();
        this.shape_12.graphics.rf(["#1F78D2", "#005AB4"], [0, 1], 394.5, 263.4, 0, 394.5, 263.4, 43.6).s().p("AlFF3QgVilgPizQgPiugBi2QCggUCvgPQCzgOC6AAIACAAQAWClAPCyQAPCvAAC2IAAAAQifAUiuAPQi0AOi7AAIgCAAg");
        this.shape_12.setTransform(100.9, 86);

        this.shape_13 = new cjs.Shape();
        this.shape_13.graphics.rf(["#217AD3", "#0059B1"], [0, 1], 390, 260.4, 0, 390, 260.4, 43.6).s().p("AlBF4QgXijgRiyQgQiuAAi2IAAAAQCegWCugQQCygQC6AAIADAAQAXCkARCxQAQCvAAC1QidAWiuAQQizAPi7ABIgCAAg");
        this.shape_13.setTransform(105.3, 89);

        this.shape_14 = new cjs.Shape();
        this.shape_14.graphics.rf(["#247BD3", "#0058AF"], [0, 1], 385.5, 257.4, 0, 385.5, 257.4, 43.6).s().p("Ak+F4QgZihgRiyQgSitAAi1IAAAAQCcgYCugRQCxgRC6AAIADAAQAZChASCxQASCuAAC1IAAABQicAXiuARQiyARi7AAIgCAAg");
        this.shape_14.setTransform(109.8, 92);

        this.shape_15 = new cjs.Shape();
        this.shape_15.graphics.rf(["#267DD4", "#0057AD"], [0, 1], 381, 254.4, 0, 381, 254.4, 43.6).s().p("Ak6F4QgaifgTixQgTisAAi1QCagaCtgSQCxgSC6AAIADAAQAbCfASCxQATCsAAC2QiZAZitASQiyASi7AAIgCAAg");
        this.shape_15.setTransform(114.3, 94.9);

        this.shape_16 = new cjs.Shape();
        this.shape_16.graphics.rf(["#297ED4", "#0056AB"], [0, 1], 376.6, 251.4, 0, 376.6, 251.4, 43.6).s().p("Ak2F5QgdidgUixQgUisAAi1QCZgbCtgTQCvgUC6AAIAEAAQAcCdAUCxQAUCsAAC1IAAAAQiYAbisAUQiyATi6AAIgCAAg");
        this.shape_16.setTransform(118.8, 97.9);

        this.shape_17 = new cjs.Shape();
        this.shape_17.graphics.rf(["#2B80D5", "#0055A9"], [0, 1], 372.1, 248.4, 0, 372.1, 248.4, 43.6).s().p("AkyF5QgeibgWiwQgVirAAi1IAAAAQCWgdCsgVQCwgUC6AAIACAAQAfCbAVCwQAWCrgBC1QiWAdisAVQiwAUi7AAIgBAAg");
        this.shape_17.setTransform(123.3, 100.9);

        this.shape_18 = new cjs.Shape();
        this.shape_18.graphics.rf(["#2E82D5", "#0054A7"], [0, 1], 367.6, 245.4, 0, 367.6, 245.4, 43.6).s().p("AkvF6QggiagWivQgXirAAi0IAAgBQCVgeCrgWQCvgWC6AAIADAAQAgCaAWCvQAXCqAAC1QiUAfirAWQiwAWi7AAIgCAAg");
        this.shape_18.setTransform(127.7, 103.9);

        this.shape_19 = new cjs.Shape();
        this.shape_19.graphics.rf(["#3083D6", "#0053A5"], [0, 1], 363.1, 242.4, 0, 363.1, 242.4, 43.6).s().p("AkrF6QgiiYgYiuQgYiqAAi1QCTggCrgXQCvgXC5AAIADAAQAiCXAYCvQAYCqAAC1QiTAhirAWQivAXi6AAIgCAAg");
        this.shape_19.setTransform(132.2, 106.9);

        this.shape_20 = new cjs.Shape();
        this.shape_20.graphics.rf(["#3385D6", "#0052A3"], [0, 1], 358.6, 239.4, 0, 358.6, 239.4, 43.5).s().p("AkoF7QgjiWgZiuQgaipABi1QCRgiCqgZQCugYC6ABIACAAQAkCVAZCuQAZCqABC0QiSAiipAYQivAZi6AAIgDAAg");
        this.shape_20.setTransform(136.7, 109.9);

        this.shape_21 = new cjs.Shape();
        this.shape_21.graphics.rf(["#3686D7", "#0051A1"], [0, 1], 354.1, 236.4, 0, 354.1, 236.4, 43.5).s().p("AkkF7QgliUgaitQgbipAAi0IAAgBQCPgjCqgZQCugaC5AAIADAAQAlCUAbCtQAaCpAAC0IAAAAQiPAkipAZQivAai6AAIgCAAg");
        this.shape_21.setTransform(141.2, 112.9);

        this.shape_22 = new cjs.Shape();
        this.shape_22.graphics.rf(["#3888D7", "#00509F"], [0, 1], 349.6, 233.4, 0, 349.6, 233.4, 43.5).s().p("AkgF7QgniSgcisQgcioAAi0QCOgmCpgbQCtgaC5AAIADAAQAnCSAcCsQAcCoAAC1QiOAlipAbQitAai6AAIgCAAg");
        this.shape_22.setTransform(145.7, 115.9);

        this.shape_23 = new cjs.Shape();
        this.shape_23.graphics.rf(["#3B89D8", "#004F9D"], [0, 1], 345.2, 230.4, 0, 345.2, 230.4, 43.6).s().p("AkdF8QgpiQgdisQgdioAAi0QCMgnCpgcQCtgcC4AAIAEAAQAoCQAdCsQAeCoAAC0IAAAAQiMAnipAcQitAci6AAIgCAAg");
        this.shape_23.setTransform(150.2, 118.9);

        this.shape_24 = new cjs.Shape();
        this.shape_24.graphics.rf(["#3D8BD8", "#004E9B"], [0, 1], 340.7, 227.4, 0, 340.7, 227.4, 43.6).s().p("AkZF8QgriOgeirQgeinAAi0IAAAAQCKgpCogdQCrgdC6AAIACAAQArCOAeCrQAfCnAAC0QiJApipAdQisAdi6AAIgCAAg");
        this.shape_24.setTransform(154.7, 121.9);

        this.shape_25 = new cjs.Shape();
        this.shape_25.graphics.rf(["#408CD9", "#004D99"], [0, 1], 336.2, 224.4, 0, 336.2, 224.4, 43.6).s().p("AkVF9QgtiNgfiqQgginAAizIAAgBQCIgqCogfQCrgeC5AAIADAAQAsCNAgCqQAgCmAAC0IAAABQiIAqioAeQirAfi6AAIgCAAg");
        this.shape_25.setTransform(159.1, 124.9);

        this.shape_26 = new cjs.Shape();
        this.shape_26.graphics.rf(["#428ED9", "#004B97"], [0, 1], 331.7, 221.5, 0, 331.7, 221.5, 43.6).s().p("AkRF9QguiLghipQghimAAizIAAgBQCGgsCnggQCrgfC5AAIACAAQAvCKAgCqQAhCmAAC0QiGAtimAfQirAfi6AAIgCAAg");
        this.shape_26.setTransform(163.6, 127.9);

        this.shape_27 = new cjs.Shape();
        this.shape_27.graphics.rf(["#458FDA", "#004A95"], [0, 1], 327.2, 218.5, 0, 327.2, 218.5, 43.6).s().p("AkOF+QgwiJgiipQgiimAAizQCFguCmghQCqggC5gBIADAAQAwCJAiCpQAiCmAACzIAAAAQiEAuinAhQiqAgi6ABIgCAAg");
        this.shape_27.setTransform(168.1, 130.9);

        this.shape_28 = new cjs.Shape();
        this.shape_28.graphics.rf(["#4791DA", "#004993"], [0, 1], 322.8, 215.5, 0, 322.8, 215.5, 43.6).s().p("AkKF+QgxiHgkipQgkikABizIAAgBQCCgvCngjQCoghC5AAIADAAQAyCHAjCoQAkClAACzQiDAwilAiQiqAii6AAIgCAAg");
        this.shape_28.setTransform(172.6, 133.9);

        this.shape_29 = new cjs.Shape();
        this.shape_29.graphics.rf(["#4A92DB", "#004891"], [0, 1], 318.3, 212.5, 0, 318.3, 212.5, 43.6).s().p("AkHF+QgziFgkinQglikAAizIAAgBQCBgxClgjQCogjC6AAIACAAQA0CFAkCnQAlCkAAC0IAAAAQiAAximAjQipAji5AAIgDAAg");
        this.shape_29.setTransform(177.1, 136.8);

        this.shape_30 = new cjs.Shape();
        this.shape_30.graphics.rf(["#4D94DB", "#00478F"], [0, 1], 313.8, 209.5, 0, 313.8, 209.5, 43.6).s().p("AkDF/Qg1iDgminQgmikAAizQB/gzClglQCogkC5AAIADAAQA1CDAmCnQAmCkAACzQh/AzilAkQioAli6AAIgCAAg");
        this.shape_30.setTransform(181.5, 139.8);

        this.shape_31 = new cjs.Shape();
        this.shape_31.graphics.rf(["#4F95DC", "#00468D"], [0, 1], 309.3, 206.5, 0, 309.3, 206.5, 43.6).s().p("Aj/F/Qg3iBgnimQgnijAAizQB9g1CkgmQCoglC4AAIADAAQA3CBAnCmQAnCjAACzIAAABQh9A0ikAmQinAli6AAIgCAAg");
        this.shape_31.setTransform(186, 142.8);

        this.shape_32 = new cjs.Shape();
        this.shape_32.graphics.rf(["#5297DC", "#00458B"], [0, 1], 304.8, 203.5, 0, 304.8, 203.5, 43.6).s().p("Aj8F/Qg5h+goimQgoijAAiyIAAAAQB7g3ClgnQCmgmC5AAIACAAQA5B/AoClQApCjAACyIAAABQh7A2ikAnQinAmi6AAIgCAAg");
        this.shape_32.setTransform(190.5, 145.8);

        this.shape_33 = new cjs.Shape();
        this.shape_33.graphics.rf(["#5498DD", "#004489"], [0, 1], 300.3, 200.5, 0, 300.3, 200.5, 43.6).s().p("Aj4GAQg6h+gqilQgqihAAizIAAAAQB6g4CjgoQCmgoC5AAIADAAQA6B9AqClQAqCiAACzIAAAAQh6A5ijAnQinAoi5AAIgCAAg");
        this.shape_33.setTransform(195, 148.8);

        this.shape_34 = new cjs.Shape();
        this.shape_34.graphics.rf(["#579ADD", "#004387"], [0, 1], 295.8, 197.5, 0, 295.8, 197.5, 43.6).s().p("Aj0GBQg9h8gqikQgrihAAizIAAgBQB3g5CjgqQCmgpC4AAIADAAQA8B8ArCkQArChAACzIAAAAQh3A7ijAoQimAqi5AAIgCAAg");
        this.shape_34.setTransform(199.5, 151.8);

        this.shape_35 = new cjs.Shape();
        this.shape_35.graphics.rf(["#599CDE", "#004285"], [0, 1], 291.4, 194.5, 0, 291.4, 194.5, 43.5).s().p("AjwGBQg/h6gsijQgsihAAiyQB2g9CigqQClgqC4AAIADAAQA+B5AsCkQAtCgAACzQh2A8iiArQimAqi4AAIgCAAg");
        this.shape_35.setTransform(204, 154.8);

        this.shape_36 = new cjs.Shape();
        this.shape_36.graphics.rf(["#5C9DDE", "#004183"], [0, 1], 286.9, 191.5, 0, 286.9, 191.5, 43.6).s().p("AjsGBQhBh4gtijQgtifgBiyIAAgBQB1g9ChgsQClgrC4AAIACAAQBBB4AtCiQAtCgABCzIAAAAQh0A9ihAsQilAri5AAIgCAAg");
        this.shape_36.setTransform(208.4, 157.8);

        this.shape_37 = new cjs.Shape();
        this.shape_37.graphics.rf(["#5E9FDF", "#004081"], [0, 1], 282.4, 188.5, 0, 282.4, 188.5, 43.6).s().p("AjpGCQhCh2guijQgvifAAiyIAAAAQByg/ChgtQCkgtC4AAIADAAQBBB2AvCiQAvCgAACyQhyA/ihAtQikAti5AAIgCAAg");
        this.shape_37.setTransform(212.9, 160.8);

        this.shape_38 = new cjs.Shape();
        this.shape_38.graphics.rf(["#61A0DF", "#003F7E"], [0, 1], 277.9, 185.5, 0, 277.9, 185.5, 43.6).s().p("AjlGCQhEh0gwiiQgwieAAiyQBxhBChguQCjguC4AAIACAAQBEB0AwChQAwCfAACyIAAABQhwBAihAuQijAui5AAIgCAAg");
        this.shape_38.setTransform(217.4, 163.8);

        this.shape_39 = new cjs.Shape();
        this.shape_39.graphics.rf(["#63A2E0", "#003E7C"], [0, 1], 273.4, 182.6, 0, 273.4, 182.6, 43.6).s().p("AjiGCQhFhxgxihQgxifAAixIAAgBQBuhDCgguQCjgwC4AAIACAAQBGBzAxChQAxCeAACyQhuBDigAuQijAvi5AAIgCAAg");
        this.shape_39.setTransform(221.9, 166.8);

        this.shape_40 = new cjs.Shape();
        this.shape_40.graphics.rf(["#66A3E0", "#003D7A"], [0, 1], 269, 179.6, 0, 269, 179.6, 43.6).s().p("AjeGDQhHhwgzihQgyidAAiyIAAAAQBthFCfgwQCigwC4AAIADAAQBHBwAyChQAzCdAACyIAAAAQhsBFigAvQiiAxi5AAIgCAAg");
        this.shape_40.setTransform(226.4, 169.8);

        this.shape_41 = new cjs.Shape();
        this.shape_41.graphics.rf(["#69A5E1", "#003C78"], [0, 1], 264.5, 176.6, 0, 264.5, 176.6, 43.6).s().p("AjaGEQhJhvg0igQg0icAAiyIAAAAQBshHCfgxQChgyC4ABIACAAQBJBuA0CgQA0CcAACyIAAAAQhrBGifAyQiiAyi4AAIgCAAg");
        this.shape_41.setTransform(230.8, 172.8);

        this.shape_42 = new cjs.Shape();
        this.shape_42.graphics.rf(["#6BA6E1", "#003B76"], [0, 1], 260, 173.6, 0, 260, 173.6, 43.6).s().p("AjXGEQhKhtg1ifQg1icAAixIAAgBQBphHCegzQChgzC4AAIACAAQBLBtA1CfQA1CcAACyIAAAAQhpBIieAyQihAzi5AAIgCAAg");
        this.shape_42.setTransform(235.3, 175.7);

        this.shape_43 = new cjs.Shape();
        this.shape_43.graphics.rf(["#6EA8E2", "#003A74"], [0, 1], 255.5, 170.6, 0, 255.5, 170.6, 43.6).s().p("AjTGEQhNhrg1ieQg3ibAAixIAAgBQBnhJCfg0QCfg0C4AAIACAAQBNBrA2CeQA3CbAACyIAAAAQhoBJidA0QigA0i5AAIgCAAg");
        this.shape_43.setTransform(239.8, 178.7);

        this.shape_44 = new cjs.Shape();
        this.shape_44.graphics.rf(["#70A9E2", "#003972"], [0, 1], 251, 167.6, 0, 251, 167.6, 43.6).s().p("AjPGFQhOhpg4ieQg4ibABixIAAAAQBlhLCeg2QCeg1C5AAIACAAQBOBpA4CeQA3CaAACyIAAABQhlBKieA1QifA2i5AAIgBAAg");
        this.shape_44.setTransform(244.3, 181.7);

        this.shape_45 = new cjs.Shape();
        this.shape_45.graphics.rf(["#73ABE3", "#003870"], [0, 1], 246.6, 164.6, 0, 246.6, 164.6, 43.6).s().p("AjLGFQhRhng4idQg5iaAAixIAAgBQBkhNCdg2QCfg2C3AAIACAAQBRBnA4CdQA5CaAACxIAAABQhkBNicA2QifA2i4AAIgCAAg");
        this.shape_45.setTransform(248.8, 184.7);

        this.shape_46 = new cjs.Shape();
        this.shape_46.graphics.rf(["#75ACE3", "#00376E"], [0, 1], 242.1, 161.6, 0, 242.1, 161.6, 43.6).s().p("AjHGFQhShkg6idQg7iZAAixQBihPCdg4QCeg4C3ABIADAAQBSBkA6CdQA6CZAACyIAAAAQhiBPicA3QifA3i3AAIgCAAg");
        this.shape_46.setTransform(253.2, 187.7);

        this.shape_47 = new cjs.Shape();
        this.shape_47.graphics.rf(["#78AEE4", "#00366C"], [0, 1], 237.6, 158.6, 0, 237.6, 158.6, 43.6).s().p("AjEGGQhUhkg7ibQg7iZAAiwIAAgBQBghQCcg6QCdg4C4AAIACAAQBTBjA8CcQA7CZAACxIAAAAQhgBRibA4QieA5i4AAIgCAAg");
        this.shape_47.setTransform(257.7, 190.7);

        this.shape_48 = new cjs.Shape();
        this.shape_48.graphics.rf(["#7AAFE4", "#00356A"], [0, 1], 233.1, 155.6, 0, 233.1, 155.6, 43.5).s().p("AjBGGQhVhhg8icQg9iXAAixIAAgBQBfhSCag5QCeg7C3AAIACAAQBWBiA8CbQA9CYAACxIAAABQheBSibA5QieA6i3AAIgDAAg");
        this.shape_48.setTransform(262.2, 193.7);

        this.shape_49 = new cjs.Shape();
        this.shape_49.graphics.rf(["#7DB1E5", "#003468"], [0, 1], 228.6, 152.6, 0, 228.6, 152.6, 43.5).s().p("Ai8GHQhYhgg+iaQg9iXgBixIAAgBQBdhTCbg8QCcg7C3AAIADAAQBWBgA+CaQA+CYAACxIAAAAQhbBUibA7QidA7i4AAIgBAAg");
        this.shape_49.setTransform(266.7, 196.7);

        this.shape_50 = new cjs.Shape();
        this.shape_50.graphics.rf(["#80B3E6", "#003366"], [0, 1], 224.1, 149.6, 0, 224.1, 149.6, 43.5).s().p("Ai5GHQhZhdg/iaQg/iXAAiwIAAgBQBbhVCag9QCbg8C4AAIACAAQBZBeA/CZQA/CXAACxIAAABQhaBViaA8QidA8i3AAIgCAAg");
        this.shape_50.setTransform(271.2, 199.7);

        this.shape_51 = new cjs.Shape();
        this.shape_51.graphics.rf(["#82B4E6", "#003264"], [0, 1], 219.7, 146.6, 0, 219.7, 146.6, 43.5).s().p("Ai2GIQhahchAiZQhBiXAAiwIAAAAQBZhYCag9QCbg+C3AAIADAAQBaBcBACZQBBCWAACxIAAABQhZBWiZA+QidA+i3AAIgCAAg");
        this.shape_51.setTransform(275.7, 202.7);

        this.shape_52 = new cjs.Shape();
        this.shape_52.graphics.rf(["#85B6E7", "#003162"], [0, 1], 215.2, 143.7, 0, 215.2, 143.7, 43.5).s().p("AiyGIQhchahCiYQhCiWABiwIAAAAQBXhZCYg/QCcg/C2AAIACAAQBdBaBCCYQBCCWgBCwIAAABQhWBYiaA/QicA/i1AAIgDAAg");
        this.shape_52.setTransform(280.2, 205.7);

        this.shape_53 = new cjs.Shape();
        this.shape_53.graphics.rf(["#87B7E7", "#003060"], [0, 1], 210.7, 140.7, 0, 210.7, 140.7, 43.5).s().p("AiuGIQhehXhDiYQhDiWAAivIAAgBQBVhbCYhAQCbg/C2gBIADAAQBeBZBDCXQBDCWAACwIAAAAQhVBbiYA/QicBAi2AAIgCAAg");
        this.shape_53.setTransform(284.6, 208.7);

        this.shape_54 = new cjs.Shape();
        this.shape_54.graphics.rf(["#8AB9E8", "#002F5E"], [0, 1], 206.2, 137.7, 0, 206.2, 137.7, 43.5).s().p("AirGJQhghWhEiYQhEiVAAivIAAgBQBUhcCXhBQCbhBC2AAIACAAQBgBWBECXQBFCVAACwIAAAAQhUBdiYBBQiaBBi3AAIgCAAg");
        this.shape_54.setTransform(289.1, 211.7);

        this.shape_55 = new cjs.Shape();
        this.shape_55.graphics.rf(["#8CBAE8", "#002E5C"], [0, 1], 201.7, 134.7, 0, 201.7, 134.7, 43.6).s().p("AinGJQhhhUhGiXQhFiUAAivIAAgBQBRhdCYhDQCZhCC2AAIACAAQBiBUBGCWQBGCVAACwIAAAAQhSBdiXBDQibBDi2gBIgCAAg");
        this.shape_55.setTransform(293.6, 214.7);

        this.shape_56 = new cjs.Shape();
        this.shape_56.graphics.rf(["#8FBCE9", "#002D5A"], [0, 1], 197.3, 131.7, 0, 197.3, 131.7, 43.6).s().p("AijGKQhjhThHiVQhHiUAAivIAAgBQBQhgCXhDQCZhEC1AAIADAAQBkBTBGCVQBHCVAACvIAAAAQhQBgiWBDQiaBEi2AAIgCAAg");
        this.shape_56.setTransform(298.1, 217.6);

        this.shape_57 = new cjs.Shape();
        this.shape_57.graphics.rf(["#91BDE9", "#002C58"], [0, 1], 192.8, 128.7, 0, 192.8, 128.7, 43.6).s().p("AifGKQhmhQhIiWQhHiTgBivIAAAAQBOhiCWhFQCZhEC2AAIADAAQBkBQBICVQBJCUgBCvIAAABQhNBhiWBEQiZBFi2AAIgCAAg");
        this.shape_57.setTransform(302.6, 220.6);

        this.shape_58 = new cjs.Shape();
        this.shape_58.graphics.rf(["#94BFEA", "#002B56"], [0, 1], 188.3, 125.7, 0, 188.3, 125.7, 43.6).s().p("AibGLQhohPhJiVQhJiSAAivIAAAAQBMhkCWhGQCYhGC1AAIADAAQBnBPBJCUQBJCTAACvIAAABQhMBiiVBGQiZBHi1AAIgCAAg");
        this.shape_58.setTransform(307, 223.6);

        this.shape_59 = new cjs.Shape();
        this.shape_59.graphics.rf(["#96C0EA", "#002A54"], [0, 1], 183.8, 122.7, 0, 183.8, 122.7, 43.6).s().p("AiYGLQhphNhKiUQhLiSAAiuIAAgBQBKhkCVhIQCYhHC1AAIADAAQBpBNBKCTQBLCTAACvIAAAAQhKBliWBGQiXBIi2AAIgCAAg");
        this.shape_59.setTransform(311.5, 226.6);

        this.shape_60 = new cjs.Shape();
        this.shape_60.graphics.rf(["#99C2EB", "#002952"], [0, 1], 179.3, 119.7, 0, 179.3, 119.7, 43.5).s().p("AiUGMQhrhLhLiTQhNiSAAivIAAAAQBJhmCVhJQCXhIC1AAIACAAQBrBKBLCTQBNCSAACvIAAAAQhJBniVBIQiXBIi1ABIgCAAg");
        this.shape_60.setTransform(316, 229.6);

        this.shape_61 = new cjs.Shape();
        this.shape_61.graphics.rf(["#9CC3EB", "#002850"], [0, 1], 174.8, 116.7, 0, 174.8, 116.7, 43.5).s().p("AiQGMQhthJhNiTQhNiRAAiuIAAAAQBHhoCUhLQCWhJC1AAIADAAQBsBJBNCSQBNCRAACvIAAABQhGBniUBKQiXBKi1AAIgCAAg");
        this.shape_61.setTransform(320.5, 232.6);

        this.shape_62 = new cjs.Shape();
        this.shape_62.graphics.rf(["#9EC5EC", "#00274E"], [0, 1], 170.3, 113.7, 0, 170.3, 113.7, 43.5).s().p("AiNGMQhuhHhOiSQhPiQAAiuIAAgBQBFhqCThLQCWhKC1AAIADAAQBuBHBOCSQBPCQAACvIAAAAQhFBqiUBKQiWBLi1AAIgCAAg");
        this.shape_62.setTransform(325, 235.6);

        this.shape_63 = new cjs.Shape();
        this.shape_63.graphics.rf(["#A1C6EC", "#00264B"], [0, 1], 165.9, 110.7, 0, 165.9, 110.7, 43.6).s().p("AiJGNQhwhGhQiRQhQiPABiuIAAgBQBDhsCShMQCVhMC2AAIACAAQBwBGBQCRQBQCPAACvIAAABQhDBriTBMQiWBMi1AAIgCAAg");
        this.shape_63.setTransform(329.5, 238.6);

        this.shape_64 = new cjs.Shape();
        this.shape_64.graphics.rf(["#A3C8ED", "#002549"], [0, 1], 161.4, 107.7, 0, 161.4, 107.7, 43.6).s().p("AiGGNQhxhDhRiRQhRiPAAiuIAAAAQBBhuCThNQCUhNC1AAIACAAQByBDBRCRQBRCPAACuIAAABQhCBtiSBNQiUBNi2AAIgCAAg");
        this.shape_64.setTransform(334, 241.6);

        this.shape_65 = new cjs.Shape();
        this.shape_65.graphics.rf(["#A6C9ED", "#002447"], [0, 1], 156.9, 104.8, 0, 156.9, 104.8, 43.5).s().p("AiBGOQh1hChSiQQhRiOAAiuIAAgBQA/huCRhPQCUhOC2AAIACAAQBzBBBSCQQBTCOgBCuIAAABQg/BviRBOQiUBOi2ABIgBAAg");
        this.shape_65.setTransform(338.4, 244.6);

        this.shape_66 = new cjs.Shape();
        this.shape_66.graphics.rf(["#A8CBEE", "#002345"], [0, 1], 152.4, 101.8, 0, 152.4, 101.8, 43.5).s().p("Ah+GOQh1hAhUiPQhTiOAAitIAAgBQA+hwCRhQQCThQC1AAIACAAQB1BABUCPQBTCOAACuIAAAAQg9BxiRBPQiUBQi1AAIgCAAg");
        this.shape_66.setTransform(342.9, 247.6);

        this.shape_67 = new cjs.Shape();
        this.shape_67.graphics.rf(["#ABCDEE", "#002243"], [0, 1], 147.9, 98.8, 0, 147.9, 98.8, 43.5).s().p("Ah6GPQh4g/hUiOQhViNAAitIAAgBQA8hyCRhSQCShQC1gBIACAAQB3A/BVCOQBVCNAACuIAAAAQg8ByiRBSQiSBRi1AAIgCAAg");
        this.shape_67.setTransform(347.4, 250.6);

        this.shape_68 = new cjs.Shape();
        this.shape_68.graphics.rf(["#ADCEEF", "#002141"], [0, 1], 143.5, 95.8, 0, 143.5, 95.8, 43.6).s().p("Ah2GPQh5g8hWiOQhXiMAAiuIAAAAQA6h1CRhSQCRhSC2AAIACAAQB4A8BWCOQBWCMABCvIAAAAQg7B0iQBSQiSBSi0AAIgCAAg");
        this.shape_68.setTransform(351.9, 253.6);

        this.shape_69 = new cjs.Shape();
        this.shape_69.graphics.rf(["#B0D0EF", "#00203F"], [0, 1], 139, 92.8, 0, 139, 92.8, 43.6).s().p("AhzGPQh7g6hXiNQhXiMAAitIAAgBQA4h2CPhTQCShTC1AAIACAAQB7A6BWCNQBYCMAACuIAAAAQg4B2iQBTQiSBTi0AAIgCAAg");
        this.shape_69.setTransform(356.4, 256.5);

        this.shape_70 = new cjs.Shape();
        this.shape_70.graphics.rf(["#B2D1F0", "#001F3D"], [0, 1], 134.5, 89.8, 0, 134.5, 89.8, 43.6).s().p("AhvGQQh9g5hYiMQhZiLAAitIAAgBQA3h4CPhUQCQhVC1AAIACAAQB9A5BYCMQBZCLAACuIAAABQg3B3iPBUQiRBVi0AAIgCAAg");
        this.shape_70.setTransform(360.8, 259.5);

        this.shape_71 = new cjs.Shape();
        this.shape_71.graphics.rf(["#B5D3F0", "#001E3B"], [0, 1], 130, 86.8, 0, 130, 86.8, 43.6).s().p("AhsGQQh+g2haiMQhZiLgBitIAAAAQA1h6CPhWQCQhVC0AAIADAAQB+A2BZCMQBaCLAACtIAAABQg0B5iOBVQiRBWi0AAIgDAAg");
        this.shape_71.setTransform(365.3, 262.5);

        this.shape_72 = new cjs.Shape();
        this.shape_72.graphics.rf(["#B8D4F1", "#001D39"], [0, 1], 125.5, 83.8, 0, 125.5, 83.8, 43.6).s().p("AhoGQQiAg0hbiLQhbiKAAitIAAgBQAyh7CPhWQCPhYC1AAIACAAQCAA1BaCLQBcCKAACuIAAAAQgzB7iOBWQiPBYi1gBIgCAAg");
        this.shape_72.setTransform(369.8, 265.5);

        this.shape_73 = new cjs.Shape();
        this.shape_73.graphics.rf(["#BAD6F1", "#001C37"], [0, 1], 121, 80.8, 0, 121, 80.8, 43.6).s().p("AhkGRQiCgzhciLQhdiJAAisIAAgBQAxh9COhYQCPhYC0AAIACAAQCCAzBcCKQBdCKAACtIAAAAQgxB8iOBZQiPBYi0AAIgCAAg");
        this.shape_73.setTransform(374.3, 268.5);

        this.shape_74 = new cjs.Shape();
        this.shape_74.graphics.rf(["#BDD7F2", "#001B35"], [0, 1], 116.5, 77.8, 0, 116.5, 77.8, 43.6).s().p("AhhGSQiEgyhdiKQhdiIAAisIAAgBQAvh/CNhaQCOhYC0AAIADAAQCDAwBdCKQBeCJAACtIAAABQgvB+iNBZQiPBai0AAIgCAAg");
        this.shape_74.setTransform(378.8, 271.5);

        this.shape_75 = new cjs.Shape();
        this.shape_75.graphics.rf(["#BFD9F2", "#001A33"], [0, 1], 112.1, 74.8, 0, 112.1, 74.8, 43.6).s().p("AhdGSQiFgwhfiIQhfiIAAitIAAgBQAtiACMhaQCPhbCzAAIADAAQCFAvBfCJQBfCJAACtIAAAAQguCAiMBaQiOBbi0AAIgCAAg");
        this.shape_75.setTransform(383.3, 274.5);

        this.shape_76 = new cjs.Shape();
        this.shape_76.graphics.rf(["#C2DAF3", "#001831"], [0, 1], 107.6, 71.8, 0, 107.6, 71.8, 43.6).s().p("AhZGSQiHgthgiIQhgiIAAisIAAgBQAriCCMhcQCNhbC0AAIACAAQCHAtBgCIQBhCIAACtIAAABQgsCBiMBbQiNBci0AAIgCAAg");
        this.shape_76.setTransform(387.7, 277.5);

        this.shape_77 = new cjs.Shape();
        this.shape_77.graphics.rf(["#C4DCF3", "#00172F"], [0, 1], 103.1, 68.8, 0, 103.1, 68.8, 43.6).s().p("AhVGTQiKgshgiHQhiiHAAisIAAgBQApiECMhdQCMhdC1AAIACAAQCJAsBhCHQBhCHAACtIAAABQgqCDiLBdQiMBdi0AAIgCAAg");
        this.shape_77.setTransform(392.2, 280.5);

        this.shape_78 = new cjs.Shape();
        this.shape_78.graphics.rf(["#C7DDF4", "#00162D"], [0, 1], 98.6, 65.8, 0, 98.6, 65.8, 43.6).s().p("AhSGTQiLgphiiIQhjiGAAisIAAgBQAoiFCLheQCMheC0AAIACAAQCLApBiCHQBjCHAACsIAAABQgoCFiLBeQiMBei0AAIgCAAg");
        this.shape_78.setTransform(396.7, 283.5);

        this.shape_79 = new cjs.Shape();
        this.shape_79.graphics.rf(["#C9DFF4", "#00152B"], [0, 1], 94.1, 62.9, 0, 94.1, 62.9, 43.6).s().p("AhOGTQiNgnhkiGQhjiGAAisIAAAAQAmiHCKhgQCLhgC0AAIACAAQCMAoBkCGQBkCGAACtIAAAAQgmCHiKBfQiLBfi0AAIgCAAg");
        this.shape_79.setTransform(401.2, 286.5);

        this.shape_80 = new cjs.Shape();
        this.shape_80.graphics.rf(["#CCE0F5", "#001429"], [0, 1], 89.7, 59.9, 0, 89.7, 59.9, 43.5).s().p("AhLGUQiOgmhliGQhliEAAisIAAgBQAkiJCKhhQCKhgC1AAIABAAQCPAmBkCFQBmCFAACtIAAABQgkCIiKBgQiLBhizAAIgDAAg");
        this.shape_80.setTransform(405.7, 289.5);

        this.shape_81 = new cjs.Shape();
        this.shape_81.graphics.rf(["#CFE2F5", "#001327"], [0, 1], 85.2, 56.9, 0, 85.2, 56.9, 43.5).s().p("AhHGVQiQglhmiFQhniEAAisIAAgBQAjiKCJhiQCKhiC0ABIACAAQCQAjBmCFQBnCFAACsIAAABQgjCKiJBiQiKBhi0ABIgCAAg");
        this.shape_81.setTransform(410.1, 292.5);

        this.shape_82 = new cjs.Shape();
        this.shape_82.graphics.rf(["#D1E3F6", "#001225"], [0, 1], 80.7, 53.9, 0, 80.7, 53.9, 43.6).s().p("AhDGVQiSgihoiFQhniDAAisIAAAAQAgiNCJhjQCJhjC0AAIACAAQCSAiBnCEQBoCFAACsIAAABQggCLiJBjQiKBjizAAIgCAAg");
        this.shape_82.setTransform(414.6, 295.4);

        this.shape_83 = new cjs.Shape();
        this.shape_83.graphics.rf(["#D4E5F6", "#001123"], [0, 1], 76.2, 50.9, 0, 76.2, 50.9, 43.6).s().p("AhAGVQiUgghoiEQhpiDAAirIAAgBQAfiOCIhkQCIhkC0AAIACAAQCUAgBoCDQBqCEAACsIAAABQgfCNiIBkQiJBki0AAIgCAAg");
        this.shape_83.setTransform(419.1, 298.4);

        this.shape_84 = new cjs.Shape();
        this.shape_84.graphics.rf(["#D6E7F7", "#001021"], [0, 1], 71.7, 47.9, 0, 71.7, 47.9, 43.6).s().p("Ag8GWQiVgfhriDQhqiCAAirIAAgBQAdiQCHhmQCJhlC0AAIACAAQCVAfBpCCQBrCDAACsIAAABQgcCPiIBlQiJBmizAAIgCAAg");
        this.shape_84.setTransform(423.6, 301.4);

        this.shape_85 = new cjs.Shape();
        this.shape_85.graphics.rf(["#D9E8F7", "#000F1F"], [0, 1], 67.2, 44.9, 0, 67.2, 44.9, 43.6).s().p("Ag4GWQiYgchriDQhriCAAirIAAgBQAbiRCHhnQCHhmC0AAIACAAQCXAcBrCCQBsCDAACrIAAABQgbCRiHBmQiIBnizAAIgCAAg");
        this.shape_85.setTransform(428.1, 304.4);

        this.shape_86 = new cjs.Shape();
        this.shape_86.graphics.rf(["#DBEAF8", "#000E1D"], [0, 1], 62.8, 41.9, 0, 62.8, 41.9, 43.6).s().p("Ag0GWQiZgahtiCQhtiBAAirIAAgBQAaiTCGhoQCHhoC0ABIACAAQCYAaBtCBQBtCCAACsIAAABQgZCSiHBoQiHBnizAAIgCAAg");
        this.shape_86.setTransform(432.5, 307.4);

        this.shape_87 = new cjs.Shape();
        this.shape_87.graphics.rf(["#DEEBF8", "#000D1B"], [0, 1], 58.3, 38.9, 0, 58.3, 38.9, 43.6).s().p("AgxGXQibgZhtiBQhuiBAAirIAAAAQAXiVCGhpQCGhpC0AAIACAAQCaAYBuCBQBuCBAACsIAAABQgXCUiGBpQiHBpizAAIgCAAg");
        this.shape_87.setTransform(437, 310.4);

        this.shape_88 = new cjs.Shape();
        this.shape_88.graphics.rf(["#E0EDF9", "#000C18"], [0, 1], 53.8, 35.9, 0, 53.8, 35.9, 43.6).s().p("AgtGXQidgXhviAQhviAAAiqIAAgBQAWiXCFhrQCGhqCzAAIACAAQCdAYBvB/QBvCBAACsIAAAAQgWCWiFBqQiGBrizgBIgCAAg");
        this.shape_88.setTransform(441.5, 313.4);

        this.shape_89 = new cjs.Shape();
        this.shape_89.graphics.rf(["#E3EEF9", "#000B16"], [0, 1], 49.3, 32.9, 0, 49.3, 32.9, 43.6).s().p("AgqGYQiegVhwiAQhxh/ABirIAAgBQATiYCFhsQCFhrC0AAIACAAQCdAVBxCAQBwB/AACsIAAABQgTCXiGBrQiFBsiyAAIgDAAg");
        this.shape_89.setTransform(446, 316.4);

        this.shape_90 = new cjs.Shape();
        this.shape_90.graphics.rf(["#E5F0FA", "#000A14"], [0, 1], 44.8, 29.9, 0, 44.8, 29.9, 43.6).s().p("AgmGYQiggThxh/Qhyh/AAiqIAAgBQASiaCEhtQCEhsC0AAIACAAQCgATBxB/QByB/AACrIAAABQgSCZiEBsQiFBtizAAIgCAAg");
        this.shape_90.setTransform(450.5, 319.4);

        this.shape_91 = new cjs.Shape();
        this.shape_91.graphics.rf(["#E8F1FA", "#000912"], [0, 1], 40.4, 26.9, 0, 40.4, 26.9, 43.6).s().p("AgiGZQiigShyh+Qh0h+AAiqIAAgBQAQicCEhuQCEhuCzAAIACAAQCiASByB+QB0B+AACrIAAABQgQCbiEBuQiEBuizAAIgCAAg");
        this.shape_91.setTransform(455, 322.4);

        this.shape_92 = new cjs.Shape();
        this.shape_92.graphics.rf(["#EBF3FB", "#000810"], [0, 1], 35.9, 24, 0, 35.9, 24, 43.6).s().p("AgfGZQijgPh0h+Qh0h+gBiqIAAgBQAPidCDhvQCDhvC0AAIACAAQCiAQB1B9QB1B9AACsIAAABQgPCciDBvQiEBvizAAIgCAAg");
        this.shape_92.setTransform(459.5, 325.4);

        this.shape_93 = new cjs.Shape();
        this.shape_93.graphics.rf(["#EDF4FB", "#00070E"], [0, 1], 31.4, 21, 0, 31.4, 21, 43.6).s().p("AgbGZQilgNh2h9Qh1h9AAiqIAAgBQANifCChwQCDhxCzAAIACAAQClAOB1B9QB2B9AACrIAAABQgNCeiCBwQiDBxizgBIgCAAg");
        this.shape_93.setTransform(463.9, 328.4);

        this.shape_94 = new cjs.Shape();
        this.shape_94.graphics.rf(["#F0F6FC", "#00060C"], [0, 1], 26.9, 18, 0, 26.9, 18, 43.6).s().p("AgXGaQiogMh2h8Qh3h9AAipIAAgBQALihCChyQCChxCzAAIACAAQCnALB2B9QB4B8AACrIAAABQgLCgiDBxQiCByiyAAIgCAAg");
        this.shape_94.setTransform(468.4, 331.4);

        this.shape_95 = new cjs.Shape();
        this.shape_95.graphics.rf(["#F2F7FC", "#00050A"], [0, 1], 22.4, 15, 0, 22.4, 15, 43.6).s().p("AgTGaQipgJh4h8Qh4h8AAipIAAgBQAJijCBhzQCChyCyAAIADAAQCoAKB4B7QB4B8AACrIAAABQgJChiBBzQiCByiyAAIgCAAg");
        this.shape_95.setTransform(472.9, 334.3);

        this.shape_96 = new cjs.Shape();
        this.shape_96.graphics.rf(["#F5F9FD", "#000408"], [0, 1], 18, 12, 0, 18, 12, 43.6).s().p("AgPGbQisgIh5h7Qh5h7AAiqIAAgBQAIikCAh0QCBh0CzAAIACAAQCqAIB6B7QB5B7AACrIAAABQgHCjiBB0QiBB0iyAAIgCAAg");
        this.shape_96.setTransform(477.4, 337.3);

        this.shape_97 = new cjs.Shape();
        this.shape_97.graphics.rf(["#F7FAFD", "#000306"], [0, 1], 13.5, 9, 0, 13.5, 9, 43.6).s().p("AgMGbQisgGh7h6Qh6h7AAipIAAgBQAFimCAh1QCAh1CzAAIACAAQCsAGB7B6QB7B7gBCqIAAABQgFCliBB1Qh/B1izAAIgCAAg");
        this.shape_97.setTransform(481.9, 340.3);

        this.shape_98 = new cjs.Shape();
        this.shape_98.graphics.rf(["#FAFCFE", "#000204"], [0, 1], 9, 6, 0, 9, 6, 43.6).s().p("AgIGcQivgFh7h6Qh8h5AAiqIAAgBQADinCAh3QCAh2CyAAIACAAQCuAEB8B6QB8B6AACqIAAABQgDCniAB2QiAB3iyAAIgCAAg");
        this.shape_98.setTransform(486.3, 343.3);

        this.shape_99 = new cjs.Shape();
        this.shape_99.graphics.rf(["#FCFDFE", "#000102"], [0, 1], 4.5, 3, 0, 4.5, 3, 43.6).s().p("AgFGcQiwgCh9h6Qh9h4AAiqIAAAAQACiqB/h3QB/h4CzAAIACAAQCvACB9B5QB+B6AACqIAAABQgCCoiAB4Qh/B3iyAAIgCAAg");
        this.shape_99.setTransform(490.8, 346.3);

        this.shape_100 = new cjs.Shape();
        this.shape_100.graphics.rf(["#FFFFFF", "#000000"], [0, 1], 0, 0, 0, 0, 0, 43.6).s().p("AkxEjQh+h4AAirQAAiqB+h5QB/h4CyAAQCzAAB/B4QB+B5ABCqQgBCrh+B4Qh/B5izAAQiyAAh/h5g");
        this.shape_100.setTransform(495.3, 349.3);

        this.shape_101 = new cjs.Shape();
        this.shape_101.graphics.rf(["#FCFCFF", "#000000"], [0, 1], 0, 0.1, 0, 0, 0.1, 43.7).s().p("AgFGcQiwgDh9h5Qh9h5AAiqQACipB/h4QB/h4CzABIACAAQCwACB8B5QB+B6AACqIAAABQgCCoh/B4QiAB4iygBIgCAAg");
        this.shape_101.setTransform(490.9, 346.4);

        this.shape_102 = new cjs.Shape();
        this.shape_102.graphics.rf(["#FAFAFF", "#000000"], [0, 1], 0.1, 0.1, 0, 0.1, 0.1, 43.9).s().p("AgJGcQivgFh6h5Qh8h7gBiqQAEioCAh2QCAh2CzAAIACAAQCuAEB7B6QB8B6AACrIAAABQgDCmiBB3Qh/B2izAAIgCAAg");
        this.shape_102.setTransform(486.5, 343.5);

        this.shape_103 = new cjs.Shape();
        this.shape_103.graphics.rf(["#F7F7FF", "#000000"], [0, 1], 0.1, 0.1, 0, 0.1, 0.1, 44).s().p("AgMGcQiugHh5h7Qh7h6gBirQAHimCAh1QCAh1CzAAIADAAQCsAGB6B7QB7B7AACrIAAAAQgFCmiCB1QiAB1iyAAIgCAAg");
        this.shape_103.setTransform(482.1, 340.6);

        this.shape_104 = new cjs.Shape();
        this.shape_104.graphics.rf(["#F5F5FF", "#000000"], [0, 1], 0.1, 0.1, 0, 0.1, 0.1, 44.2).s().p("AgRGbQirgIh4h7Qh6h8AAirQAIikCBh0QCChzCzgBIABAAQCrAJB5B7QB6B8AACqIAAACQgHCjiCB0QiCBziyAAIgDAAg");
        this.shape_104.setTransform(477.7, 337.7);

        this.shape_105 = new cjs.Shape();
        this.shape_105.graphics.rf(["#F2F2FF", "#000000"], [0, 1], 0.1, 0.2, 0, 0.1, 0.2, 44.3).s().p("AgVGbQipgLh3h8Qh4h7AAisQAJiiCChzQCChyC0AAIACAAQCoAKB4B8QB4B8AACsIAAABQgJChiCByQiCB0i0gBIgCAAg");
        this.shape_105.setTransform(473.3, 334.8);

        this.shape_106 = new cjs.Shape();
        this.shape_106.graphics.rf(["#F0F0FF", "#000000"], [0, 1], 0.1, 0.2, 0, 0.1, 0.2, 44.4).s().p("AgYGbQiogNh2h8Qh3h9AAirQALiiCDhxQCDhyCzABIACAAQCoAMB1B9QB4B9AACsIAAABQgLCgiEBwQiCBzizgBIgCAAg");
        this.shape_106.setTransform(468.9, 331.9);

        this.shape_107 = new cjs.Shape();
        this.shape_107.graphics.rf(["#EDEDFF", "#000000"], [0, 1], 0.1, 0.2, 0, 0.1, 0.2, 44.6).s().p("AgdGbQilgPh1h9Qh1h+AAisQANieCDhxQCEhwCzAAIADAAQClAOB0B+QB2B+AACrIAAABQgNCfiEBvQiDBxizAAIgDAAg");
        this.shape_107.setTransform(464.5, 329);

        this.shape_108 = new cjs.Shape();
        this.shape_108.graphics.rf(["#EBEBFF", "#000000"], [0, 1], 0.2, 0.2, 0, 0.2, 0.2, 44.8).s().p("AggGbQilgRhzh+Qh0h+gBitQAQicCEhwQCEhvC0AAIACAAQCkARBzB9QB1CAAACrIAAABQgPCdiFBuQiEBwizAAIgCAAg");
        this.shape_108.setTransform(460.1, 326.1);

        this.shape_109 = new cjs.Shape();
        this.shape_109.graphics.rf(["#E8E8FF", "#000000"], [0, 1], 0.2, 0.3, 0, 0.2, 0.3, 44.9).s().p("AgkGaQiigShzh/Qhzh/AAisQASicCEhuQCFhtC0gBIACAAQChASB0B/QBzCBAACsIAAAAQgRCbiFBuQiFBti0AAIgCAAg");
        this.shape_109.setTransform(455.7, 323.2);

        this.shape_110 = new cjs.Shape();
        this.shape_110.graphics.rf(["#E5E5FF", "#000000"], [0, 1], 0.2, 0.3, 0, 0.2, 0.3, 45.1).s().p("AgoGbQihgWhwh/QhyiAgBisQAUiaCFhtQCFhtC1ABIACAAQCgAUBxB/QBzCBAACsIAAABQgUCZiFBtQiGBti0AAIgCAAg");
        this.shape_110.setTransform(451.3, 320.3);

        this.shape_111 = new cjs.Shape();
        this.shape_111.graphics.rf(["#E3E3FF", "#000000"], [0, 1], 0.2, 0.3, 0, 0.2, 0.3, 45.2).s().p("AgsGbQifgXhviBQhxiAAAitQAViYCGhsQCHhrC0AAIACAAQCeAWBwCAQBxCCAACsIAAABQgVCYiGBrQiHBsi0AAIgCAAg");
        this.shape_111.setTransform(446.9, 317.4);

        this.shape_112 = new cjs.Shape();
        this.shape_112.graphics.rf(["#E0E0FF", "#000000"], [0, 1], 0.2, 0.4, 0, 0.2, 0.4, 45.3).s().p("AgwGaQidgZhuiBQhviCAAisQAWiWCHhrQCHhqC1AAIACAAQCcAYBvCBQBwCDAACsIAAABQgXCWiHBqQiIBqizAAIgDAAg");
        this.shape_112.setTransform(442.5, 314.5);

        this.shape_113 = new cjs.Shape();
        this.shape_113.graphics.rf(["#DEDEFF", "#000000"], [0, 1], 0.3, 0.4, 0, 0.3, 0.4, 45.6).s().p("Ag0GaQibgbhtiCQhuiCAAitQAZiVCHhpQCHhpC2AAIABAAQCbAbBuCCQBuCCABCtIAAABQgaCUiHBpQiIBpi0AAIgDAAg");
        this.shape_113.setTransform(438.1, 311.6);

        this.shape_114 = new cjs.Shape();
        this.shape_114.graphics.rf(["#DBDBFF", "#000000"], [0, 1], 0.3, 0.4, 0, 0.3, 0.4, 45.7).s().p("Ag4GZQiZgchsiDQhtiDAAitQAbiTCIhoQCJhoC0ABIADAAQCZAbBsCEQBtCCAACuIAAABQgbCSiIBnQiIBoi2AAIgCAAg");
        this.shape_114.setTransform(433.7, 308.7);

        this.shape_115 = new cjs.Shape();
        this.shape_115.graphics.rf(["#D9D9FF", "#000000"], [0, 1], 0.3, 0.4, 0, 0.3, 0.4, 45.9).s().p("Ag7GaQiZgfhqiEQhriEgBitQAdiRCJhnQCJhnC1ABIACAAQCXAdBsCFQBsCEAACtIAAABQgdCQiJBmQiKBoi1AAIgBAAg");
        this.shape_115.setTransform(429.3, 305.8);

        this.shape_116 = new cjs.Shape();
        this.shape_116.graphics.rf(["#D6D6FF", "#000000"], [0, 1], 0.3, 0.5, 0, 0.3, 0.5, 46).s().p("AhAGaQiWgihpiEQhqiEAAiuQAfiPCJhmQCKhlC2AAIACAAQCUAgBqCFQBrCEAACuIAAAAQgfCQiJBkQiKBni1AAIgDAAg");
        this.shape_116.setTransform(424.9, 302.9);

        this.shape_117 = new cjs.Shape();
        this.shape_117.graphics.rf(["#D4D4FF", "#000000"], [0, 1], 0.3, 0.5, 0, 0.3, 0.5, 46.2).s().p("AhEGZQiTgjhpiFQhpiFAAiuQAhiOCKhkQCKhkC2AAIACAAQCUAiBoCGQBpCFAACtIAAABQggCOiKBjQiLBli1AAIgDAAg");
        this.shape_117.setTransform(420.5, 300);

        this.shape_118 = new cjs.Shape();
        this.shape_118.graphics.rf(["#D1D1FF", "#000000"], [0, 1], 0.3, 0.5, 0, 0.3, 0.5, 46.3).s().p("AhHGZQiSgkhoiHQhoiFABiuQAiiNCLhjQCLhjC2AAIADAAQCRAkBnCGQBpCGgBCuIAAACQgiCLiLBiQiMBki1AAIgCAAg");
        this.shape_118.setTransform(416.1, 297.1);

        this.shape_119 = new cjs.Shape();
        this.shape_119.graphics.rf(["#CFCFFF", "#000000"], [0, 1], 0.4, 0.5, 0, 0.4, 0.5, 46.4).s().p("AhLGZQiRgnhmiGQhmiHAAivQAliKCKhiQCNhiC2ABIACAAQCQAmBmCGQBnCHAACvIAAABQglCJiLBiQiMBhi2ABIgCAAg");
        this.shape_119.setTransform(411.7, 294.2);

        this.shape_120 = new cjs.Shape();
        this.shape_120.graphics.rf(["#CCCCFF", "#000000"], [0, 1], 0.4, 0.6, 0, 0.4, 0.6, 46.6).s().p("AhPGZQiPgphliIQhkiHAAivQAmiJCMhgQCNhgC2AAIACAAQCOAoBlCHQBmCIgBCvIAAABQgmCIiMBgQiNBhi2AAIgCAAg");
        this.shape_120.setTransform(407.3, 291.3);

        this.shape_121 = new cjs.Shape();
        this.shape_121.graphics.rf(["#C9C9FF", "#000000"], [0, 1], 0.4, 0.6, 0, 0.4, 0.6, 46.8).s().p("AhTGYQiNgqhjiJQhkiIAAiuQApiHCLhgQCPhfC2AAIACAAQCMAqBjCIQBlCIAACvIAAABQgpCHiMBfQiOBfi2AAIgCAAg");
        this.shape_121.setTransform(402.9, 288.4);

        this.shape_122 = new cjs.Shape();
        this.shape_122.graphics.rf(["#C7C7FF", "#000000"], [0, 1], 0.4, 0.6, 0, 0.4, 0.6, 46.9).s().p("AhXGYQiLgthiiIQhjiJAAiwQAqiFCOheQCOheC3AAIACAAQCLAsBhCJQBkCJgBCvIAAABQgpCFiOBeQiPBei1AAIgDAAg");
        this.shape_122.setTransform(398.5, 285.5);

        this.shape_123 = new cjs.Shape();
        this.shape_123.graphics.rf(["#C4C4FF", "#000000"], [0, 1], 0.4, 0.7, 0, 0.4, 0.7, 47.1).s().p("AhbGYQiJguhhiLQhiiJAAiwQAtiDCOhdQCPhdC2AAIADAAQCIAuBiCKQBhCKAACwIAAAAQgsCDiOBcQiPBei3AAIgCAAg");
        this.shape_123.setTransform(394.1, 282.6);

        this.shape_124 = new cjs.Shape();
        this.shape_124.graphics.rf(["#C2C2FF", "#000000"], [0, 1], 0.4, 0.7, 0, 0.4, 0.7, 47.2).s().p("AheGYQiIgxhgiKQhgiKAAiwQAuiCCOhcQCQhcC4AAIABAAQCIAxBgCKQBgCKAACwIAAABQguCBiPBbQiQBdi3AAIgBAAg");
        this.shape_124.setTransform(389.7, 279.7);

        this.shape_125 = new cjs.Shape();
        this.shape_125.graphics.rf(["#BFBFFF", "#000000"], [0, 1], 0.5, 0.7, 0, 0.5, 0.7, 47.3).s().p("AhiGXQiGgyhfiMQheiKAAiwQAviBCQhaQCQhaC3AAIADAAQCFAxBeCMQBgCMAACvIAAABQgwB/iQBbQiQBai3AAIgCAAg");
        this.shape_125.setTransform(385.3, 276.8);

        this.shape_126 = new cjs.Shape();
        this.shape_126.graphics.rf(["#BDBDFF", "#000000"], [0, 1], 0.5, 0.7, 0, 0.5, 0.7, 47.5).s().p("AhnGXQiEg0hdiMQhdiMAAiwQAyh+CPhaQCShaC3ABIACAAQCEAzBdCNQBeCMAACwIAAABQgyB9iQBaQiRBZi3AAIgDAAg");
        this.shape_126.setTransform(380.9, 273.9);

        this.shape_127 = new cjs.Shape();
        this.shape_127.graphics.rf(["#BABAFF", "#000000"], [0, 1], 0.5, 0.8, 0, 0.5, 0.8, 47.7).s().p("AhqGXQiDg2hciOQhciMABiwQAzh9CRhYQCRhYC4AAIADAAQCBA2BcCNQBdCNAACwIAAAAQg0B9iQBYQiTBYi3AAIgCAAg");
        this.shape_127.setTransform(376.5, 271);

        this.shape_128 = new cjs.Shape();
        this.shape_128.graphics.rf(["#B8B8FF", "#000000"], [0, 1], 0.5, 0.8, 0, 0.5, 0.8, 47.8).s().p("AhuGXQiBg5haiNQhciOAAiwQA2h7CRhXQCThXC4AAIACAAQCAA4BbCOQBcCNAACxIAAABQg3B6iQBXQiTBXi4AAIgCAAg");
        this.shape_128.setTransform(372.1, 268.1);

        this.shape_129 = new cjs.Shape();
        this.shape_129.graphics.rf(["#B5B5FF", "#000000"], [0, 1], 0.5, 0.8, 0, 0.5, 0.8, 48).s().p("AhyGWQh/g5hZiQQhaiNAAixQA4h5CShWQCThVC4AAIADAAQB+A5BZCPQBaCOAACxIAAABQg4B5iSBVQiTBWi4gBIgCAAg");
        this.shape_129.setTransform(367.7, 265.2);

        this.shape_130 = new cjs.Shape();
        this.shape_130.graphics.rf(["#B2B2FF", "#000000"], [0, 1], 0.5, 0.8, 0, 0.5, 0.8, 48.1).s().p("Ah2GWQh9g8hYiQQhZiOAAixQA6h4CShUQCUhVC5ABIACAAQB8A7BYCQQBaCPAACwIAAABQg6B4iTBUQiUBUi4AAIgCAAg");
        this.shape_130.setTransform(363.3, 262.3);

        this.shape_131 = new cjs.Shape();
        this.shape_131.graphics.rf(["#B0B0FF", "#000000"], [0, 1], 0.6, 0.9, 0, 0.6, 0.9, 48.3).s().p("Ah6GXQh7g/hXiQQhXiQAAixQA8h1CThUQCUhTC5AAIACAAQB7A9BWCRQBYCQAACwIAAABQg7B2iTBTQiVBTi4ABIgDAAg");
        this.shape_131.setTransform(358.9, 259.4);

        this.shape_132 = new cjs.Shape();
        this.shape_132.graphics.rf(["#ADADFF", "#000000"], [0, 1], 0.6, 0.9, 0, 0.6, 0.9, 48.4).s().p("Ah+GWQh5hAhWiRQhViRAAiwQA9h1CThSQCWhSC4AAIADAAQB5BABVCRQBXCQgBCxIAAACQg9BziTBSQiXBSi3AAIgDAAg");
        this.shape_132.setTransform(354.5, 256.5);

        this.shape_133 = new cjs.Shape();
        this.shape_133.graphics.rf(["#ABABFF", "#000000"], [0, 1], 0.6, 0.9, 0, 0.6, 0.9, 48.6).s().p("AiCGWQh3hDhViRQhUiRAAixQA/hzCUhRQCXhRC5AAIACAAQB3BCBUCSQBVCRAACyIAAAAQg/ByiUBRQiXBRi5AAIgCAAg");
        this.shape_133.setTransform(350.1, 253.6);

        this.shape_134 = new cjs.Shape();
        this.shape_134.graphics.rf(["#A8A8FF", "#000000"], [0, 1], 0.6, 1, 0, 0.6, 1, 48.7).s().p("AiGGVQh2hDhTiTQhTiSAAiyQBBhwCWhQQCXhQC4AAIADAAQB1BEBTCTQBUCSAACyIAAAAQhCBxiVBPQiWBQi5gBIgDAAg");
        this.shape_134.setTransform(345.7, 250.7);

        this.shape_135 = new cjs.Shape();
        this.shape_135.graphics.rf(["#A6A6FF", "#000000"], [0, 1], 0.6, 1, 0, 0.6, 1, 48.9).s().p("AiKGWQhzhGhTiUQhSiTAAixQBEhwCVhOQCYhPC6ABIABAAQB0BGBSCTQBTCTgBCxIAAABQhDBviWBOQiYBOi4ABIgDAAg");
        this.shape_135.setTransform(341.3, 247.8);

        this.shape_136 = new cjs.Shape();
        this.shape_136.graphics.rf(["#A3A3FF", "#000000"], [0, 1], 0.7, 1, 0, 0.7, 1, 49).s().p("AiOGWQhyhJhQiUQhRiTAAiyQBFhuCXhNQCYhNC5AAIADAAQBxBHBRCUQBRCUAACyIAAABQhFBtiWBNQiZBNi5ABIgDAAg");
        this.shape_136.setTransform(336.9, 244.9);

        this.shape_137 = new cjs.Shape();
        this.shape_137.graphics.rf(["#A1A1FF", "#000000"], [0, 1], 0.7, 1, 0, 0.7, 1, 49.2).s().p("AiRGVQhxhKhPiVQhPiUAAiyQBHhsCWhMQCahMC5AAIACAAQBxBJBOCVQBQCVAACyIAAABQhGBriXBMQiaBMi5AAIgCAAg");
        this.shape_137.setTransform(332.5, 242);

        this.shape_138 = new cjs.Shape();
        this.shape_138.graphics.rf(["#9E9EFF", "#000000"], [0, 1], 0.7, 1.1, 0, 0.7, 1.1, 49.3).s().p("AiWGVQhuhMhOiWQhOiVAAiyQBJhqCYhLQCZhLC6AAIADAAQBtBMBPCVQBPCVgBCzIAAABQhIBpiZBLQiZBLi6AAIgDAAg");
        this.shape_138.setTransform(328.1, 239.1);

        this.shape_139 = new cjs.Shape();
        this.shape_139.graphics.rf(["#9C9CFF", "#000000"], [0, 1], 0.7, 1.1, 0, 0.7, 1.1, 49.4).s().p("AiZGVQhthPhNiWQhNiVAAizQBLhoCYhKQCbhKC6ABIACAAQBsBNBOCWQBNCWAACzIAAABQhLBoiZBJQiaBKi6AAIgCAAg");
        this.shape_139.setTransform(323.7, 236.2);

        this.shape_140 = new cjs.Shape();
        this.shape_140.graphics.rf(["#9999FF", "#000000"], [0, 1], 0.7, 1.1, 0, 0.7, 1.1, 49.6).s().p("AidGVQhshQhLiYQhLiWgBizQBNhmCahJQCahIC7AAIACAAQBqBPBMCXQBNCXAACzIAAAAQhOBniZBIQibBIi6ABIgCAAg");
        this.shape_140.setTransform(319.3, 233.3);

        this.shape_141 = new cjs.Shape();
        this.shape_141.graphics.rf(["#9696FF", "#000000"], [0, 1], 0.7, 1.2, 0, 0.7, 1.2, 49.8).s().p("AihGUQhphShLiYQhKiXAAizQBPhlCZhHQCdhHC5AAIADAAQBpBRBKCZQBLCXAACzIAAAAQhPBliZBHQidBHi5AAIgDAAg");
        this.shape_141.setTransform(314.9, 230.4);

        this.shape_142 = new cjs.Shape();
        this.shape_142.graphics.rf(["#9494FF", "#000000"], [0, 1], 0.8, 1.2, 0, 0.8, 1.2, 49.9).s().p("AilGUQhnhUhKiZQhJiXAAi0QBRhjCahGQCdhGC7AAIACAAQBnBTBJCaQBJCXABC0IAAABQhRBiiaBGQidBGi7AAIgCAAg");
        this.shape_142.setTransform(310.5, 227.5);

        this.shape_143 = new cjs.Shape();
        this.shape_143.graphics.rf(["#9191FF", "#000000"], [0, 1], 0.8, 1.2, 0, 0.8, 1.2, 50.1).s().p("AipGUQhlhWhIiaQhIiYgBi0QBUhhCbhFQCdhFC6AAIADAAQBlBWBICaQBJCYAAC0IAAABQhTBhibBEQieBFi6AAIgDAAg");
        this.shape_143.setTransform(306.1, 224.6);

        this.shape_144 = new cjs.Shape();
        this.shape_144.graphics.rf(["#8F8FFF", "#000000"], [0, 1], 0.8, 1.2, 0, 0.8, 1.2, 50.2).s().p("AitGUQhkhYhGiaQhHiaAAizQBVhgCbhEQCfhEC7AAIACAAQBjBYBHCbQBHCZAACzIAAABQhVBgibBDQieBEi7AAIgDAAg");
        this.shape_144.setTransform(301.7, 221.7);

        this.shape_145 = new cjs.Shape();
        this.shape_145.graphics.rf(["#8C8CFF", "#000000"], [0, 1], 0.8, 1.3, 0, 0.8, 1.3, 50.4).s().p("AixGTQhihZhFicQhFiaAAi0QBWhdCdhDQCehCC7AAIADAAQBiBZBECbQBGCaABC0IAAABQhXBeicBCQifBCi8AAIgCAAg");
        this.shape_145.setTransform(297.3, 218.8);

        this.shape_146 = new cjs.Shape();
        this.shape_146.graphics.rf(["#8A8AFF", "#000000"], [0, 1], 0.8, 1.3, 0, 0.8, 1.3, 50.5).s().p("Ai0GTQhhhchEibQhEibAAi1QBZhcCchBQCghBC7AAIACAAQBgBbBECcQBFCbAAC0IAAABQhYBcidBBQigBBi7AAIgCAAg");
        this.shape_146.setTransform(292.9, 215.9);

        this.shape_147 = new cjs.Shape();
        this.shape_147.graphics.rf(["#8787FF", "#000000"], [0, 1], 0.8, 1.3, 0, 0.8, 1.3, 50.7).s().p("Ai5GTQhehdhCieQhDiZgBi2QBbhbCehAQCghAC7AAIADAAQBeBeBDCcQBDCbAAC2IAAAAQhbBaidBAQihBAi7AAIgDAAg");
        this.shape_147.setTransform(288.5, 213);

        this.shape_148 = new cjs.Shape();
        this.shape_148.graphics.rf(["#8585FF", "#000000"], [0, 1], 0.9, 1.3, 0, 0.9, 1.3, 50.8).s().p("Ai9GTQhchghBidQhCicgBi2QBdhZCeg+QChg/C8AAIACAAQBdBgBCCdQBCCbgBC3IAAAAQhcBZieA+QihA/i8AAIgDAAg");
        this.shape_148.setTransform(284.1, 210.1);

        this.shape_149 = new cjs.Shape();
        this.shape_149.graphics.rf(["#8282FF", "#000000"], [0, 1], 0.9, 1.4, 0, 0.9, 1.4, 51).s().p("AjAGSQhchhg/ifQhBibAAi3QBehXCfg9QChg+C9AAIADAAQBaBiBACeQBBCcAAC2IAAABQheBXifA9QihA+i9gBIgCAAg");
        this.shape_149.setTransform(279.7, 207.2);

        this.shape_150 = new cjs.Shape();
        this.shape_150.graphics.rf(["#8080FF", "#000000"], [0, 1], 0.9, 1.4, 0, 0.9, 1.4, 51.1).s().p("AjEGSQhahjg+igQhAibABi3QBghWCfg8QCig8C9AAIACAAQBZBjA/CgQA/CcAAC2IAAABQhgBVifA8QiiA8i8AAIgDAAg");
        this.shape_150.setTransform(275.3, 204.3);

        this.shape_151 = new cjs.Shape();
        this.shape_151.graphics.rf(["#7D7DFF", "#000000"], [0, 1], 0.9, 1.4, 0, 0.9, 1.4, 51.3).s().p("AjIGSQhXhmg+ifQg+ieAAi2QBihUCgg7QCig8C+ABIACAAQBXBlA+CgQA+CdAAC3IAAABQhiBTigA7QijA7i9AAIgCAAg");
        this.shape_151.setTransform(270.9, 201.4);

        this.shape_152 = new cjs.Shape();
        this.shape_152.graphics.rf(["#7A7AFF", "#000000"], [0, 1], 0.9, 1.5, 0, 0.9, 1.5, 51.4).s().p("AjMGSQhWhng8iiQg8idAAi3QBjhSChg6QCjg6C9AAIADAAQBVBoA9CgQA8CeAAC3IAAABQhjBRihA6QijA6i+AAIgCAAg");
        this.shape_152.setTransform(266.5, 198.5);

        this.shape_153 = new cjs.Shape();
        this.shape_153.graphics.rf(["#7878FF", "#000000"], [0, 1], 1, 1.5, 0, 1, 1.5, 51.6).s().p("AjQGSQhUhqg7ihQg7ifgBi3QBnhQChg6QCkg4C+AAIACAAQBUBpA7CiQA7CfAAC3IAAAAQhlBQiiA5QikA5i9AAIgDAAg");
        this.shape_153.setTransform(262.1, 195.6);

        this.shape_154 = new cjs.Shape();
        this.shape_154.graphics.rf(["#7575FF", "#000000"], [0, 1], 1, 1.5, 0, 1, 1.5, 51.7).s().p("AjUGRQhShrg5ijQg7ifAAi3QBohOCig4QCkg3C+AAIADAAQBSBqA5CjQA7CfAAC4IAAAAQhoBOiiA4QikA4i/gBIgCAAg");
        this.shape_154.setTransform(257.7, 192.7);

        this.shape_155 = new cjs.Shape();
        this.shape_155.graphics.rf(["#7373FF", "#000000"], [0, 1], 1, 1.5, 0, 1, 1.5, 51.9).s().p("AjYGRQhQhtg4ijQg6igAAi4QBrhNCig2QClg2C+AAIADAAQBPBtA5CjQA5CgABC4IAAAAQhqBNijA2QilA2i+AAIgDAAg");
        this.shape_155.setTransform(253.3, 189.8);

        this.shape_156 = new cjs.Shape();
        this.shape_156.graphics.rf(["#7070FF", "#000000"], [0, 1], 1, 1.6, 0, 1, 1.6, 52.1).s().p("AjbGSQhPhxg3ikQg4igAAi4QBshLCjg2QCmg0C+AAIADAAQBOBvA3CkQA4ChAAC4QhsBLijA1QimA1i+ABIgCAAg");
        this.shape_156.setTransform(248.9, 186.9);

        this.shape_157 = new cjs.Shape();
        this.shape_157.graphics.rf(["#6E6EFF", "#000000"], [0, 1], 1, 1.6, 0, 1, 1.6, 52.2).s().p("AjgGRQhMhxg2ilQg2ihAAi5QBthJCkg1QCmgzC/AAIACAAQBNBxA2ClQA3ChAAC5IAAAAQhuBJikA0QinA0i+AAIgDAAg");
        this.shape_157.setTransform(244.5, 184);

        this.shape_158 = new cjs.Shape();
        this.shape_158.graphics.rf(["#6B6BFF", "#000000"], [0, 1], 1, 1.6, 0, 1, 1.6, 52.4).s().p("AjjGRQhMh0g0ilQg2ijABi4QBwhHCkgzQCngzC/AAIADAAQBKB0A1ClQA1CiABC4IAAABQhwBHilAzQioAzi+AAIgCAAg");
        this.shape_158.setTransform(240.1, 181.1);

        this.shape_159 = new cjs.Shape();
        this.shape_159.graphics.rf(["#6969FF", "#000000"], [0, 1], 1.1, 1.6, 0, 1.1, 1.6, 52.5).s().p("AjnGRQhKh2gzimQg0ijAAi4QByhGClgzQCogwC/gBIACAAQBJB2A0CmQA0CjAAC4IAAABQhyBFikAyQipAyi/AAIgCAAg");
        this.shape_159.setTransform(235.7, 178.2);

        this.shape_160 = new cjs.Shape();
        this.shape_160.graphics.rf(["#6666FF", "#000000"], [0, 1], 1.1, 1.7, 0, 1.1, 1.7, 52.7).s().p("AjsGRQhGh4gzinQgzijAAi6QB0hECmgxQCpgvC+AAIAEAAQBHB3AxCnQA0CkAAC4IAAABQh0BEilAwQiqAwi+ABIgEAAg");
        this.shape_160.setTransform(231.3, 175.3);

        this.shape_161 = new cjs.Shape();
        this.shape_161.graphics.rf(["#6363FF", "#000000"], [0, 1], 1.1, 1.7, 0, 1.1, 1.7, 52.8).s().p("AjvGRQhGh6gxioQgxikAAi6QB1hCCngvQCqgwC+ABIADAAQBGB5AwCoQAyCkAAC6IAAAAQh1BCinAvQipAvjAABIgCAAg");
        this.shape_161.setTransform(226.9, 172.4);

        this.shape_162 = new cjs.Shape();
        this.shape_162.graphics.rf(["#6161FF", "#000000"], [0, 1], 1.1, 1.7, 0, 1.1, 1.7, 53).s().p("AjzGQQhEh7gvipQgxilAAi5QB3hBCogvQCqgtDAAAIACAAQBDB7AxCpQAvClABC6Qh3BAioAuQiqAui/AAIgDAAg");
        this.shape_162.setTransform(222.5, 169.5);

        this.shape_163 = new cjs.Shape();
        this.shape_163.graphics.rf(["#5E5EFF", "#000000"], [0, 1], 1.1, 1.8, 0, 1.1, 1.8, 53.1).s().p("Aj3GQQhCh+guipQgwimABi6QB5g/CogtQCrgsC/AAIADAAQBBB+AwCpQAuCmAAC5Qh5A/ioAtQirAti/AAIgDAAg");
        this.shape_163.setTransform(218.1, 166.6);

        this.shape_164 = new cjs.Shape();
        this.shape_164.graphics.rf(["#5C5CFF", "#000000"], [0, 1], 1.1, 1.8, 0, 1.1, 1.8, 53.3).s().p("Aj7GQQhAiAgtiqQguimAAi6QB7g+CpgrQCrgrDBAAIACAAQBAB+AtCrQAuCmAAC6IAAAAQh7A+ioArQitAsjAAAIgCAAg");
        this.shape_164.setTransform(213.7, 163.7);

        this.shape_165 = new cjs.Shape();
        this.shape_165.graphics.rf(["#5959FF", "#000000"], [0, 1], 1.2, 1.8, 0, 1.2, 1.8, 53.4).s().p("Aj/GQQg+iCgsirQgsinAAi6QB9g8CpgrQCsgqDAABIADAAQA+CAAsCrQAsCoAAC6IAAABQh9A7ioAqQitAqjAABIgDAAg");
        this.shape_165.setTransform(209.3, 160.8);

        this.shape_166 = new cjs.Shape();
        this.shape_166.graphics.rf(["#5757FF", "#000000"], [0, 1], 1.2, 1.8, 0, 1.2, 1.8, 53.6).s().p("AkDGPQg9iDgqisQgrioAAi6QB/g6CqgqQCtgoDAAAIACAAQA9CDArCrQArCpAAC6IAAABQh/A5iqApQitApjAAAIgDAAg");
        this.shape_166.setTransform(204.9, 157.9);

        this.shape_167 = new cjs.Shape();
        this.shape_167.graphics.rf(["#5454FF", "#000000"], [0, 1], 1.2, 1.9, 0, 1.2, 1.9, 53.7).s().p("AkHGPQg6iFgqitQgqioAAi7QCBg4CrgoQCugoDAAAIADAAQA6CFAqCsQApCqAAC6IAAAAQiAA4irAoQiuAojAAAIgDAAg");
        this.shape_167.setTransform(200.5, 155);

        this.shape_168 = new cjs.Shape();
        this.shape_168.graphics.rf(["#5252FF", "#000000"], [0, 1], 1.2, 1.9, 0, 1.2, 1.9, 53.9).s().p("AkKGPQg6iIgoitQgpipAAi7QCEg3CrgnQCugmDBAAIACAAQA5CIApCsQAoCrAAC6QiCA3irAmQivAnjBAAIgCAAg");
        this.shape_168.setTransform(196.1, 152.1);

        this.shape_169 = new cjs.Shape();
        this.shape_169.graphics.rf(["#4F4FFF", "#000000"], [0, 1], 1.2, 1.9, 0, 1.2, 1.9, 54).s().p("AkPGPQg3iKgmiuQgoiqAAi7QCFg1CsgmQCvglDAAAIAEAAQA2CKAnCuQAoCqAAC7QiFA1irAlQiwAmjBAAIgDAAg");
        this.shape_169.setTransform(191.7, 149.2);

        this.shape_170 = new cjs.Shape();
        this.shape_170.graphics.rf(["#4D4DFF", "#000000"], [0, 1], 1.3, 2, 0, 1.3, 2, 54.2).s().p("AkTGOQg1iLglivQgmirAAi6QCHg0CsgkQCwglDBABIACAAQA2CLAlCvQAnCqAAC8QiHAzitAkQivAkjCAAIgDAAg");
        this.shape_170.setTransform(187.3, 146.3);

        this.shape_171 = new cjs.Shape();
        this.shape_171.graphics.rf(["#4A4AFF", "#000000"], [0, 1], 1.3, 2, 0, 1.3, 2, 54.3).s().p("AkWGOQg0iNgkivQglisAAi7QCIgzCugjQCwgiDBAAIADAAQAzCNAlCwQAlCsAAC7IAAAAQiIAxiuAjQixAjjBAAIgCAAg");
        this.shape_171.setTransform(182.9, 143.4);

        this.shape_172 = new cjs.Shape();
        this.shape_172.graphics.rf(["#4747FF", "#000000"], [0, 1], 1.3, 2, 0, 1.3, 2, 54.5).s().p("AkaGOQgyiPgjixQgkisAAi8QCLgvCugjQCxghDCAAIACAAQAyCPAjCxQAkCsAAC8IAAAAQiLAviuAiQixAijBAAIgDAAg");
        this.shape_172.setTransform(178.5, 140.5);

        this.shape_173 = new cjs.Shape();
        this.shape_173.graphics.rf(["#4545FF", "#000000"], [0, 1], 1.3, 2, 0, 1.3, 2, 54.6).s().p("AkeGOQgwiSgiiwQgiiugBi7QCNgvCvggQCyghDCAAIACAAQAwCRAiCyQAjCsAAC8IAAABQiNAtiuAhQiyAhjDAAIgCAAg");
        this.shape_173.setTransform(174.1, 137.6);

        this.shape_174 = new cjs.Shape();
        this.shape_174.graphics.rf(["#4242FF", "#000000"], [0, 1], 1.3, 2.1, 0, 1.3, 2.1, 54.8).s().p("AkiGNQguiTghiyQghiuAAi7QCOgtCwggQCygfDCAAIADAAQAuCUAhCxQAhCvAAC7QiOAtivAfQi0AgjCgBIgCAAg");
        this.shape_174.setTransform(169.7, 134.7);

        this.shape_175 = new cjs.Shape();
        this.shape_175.graphics.rf(["#4040FF", "#000000"], [0, 1], 1.3, 2.1, 0, 1.3, 2.1, 54.9).s().p("AkmGNQgsiVggiyQggivAAi9QCRgqCwgfQC0gdDBAAIADAAQAtCVAfCyQAfCvABC9QiRAqivAfQi0AdjCAAIgDAAg");
        this.shape_175.setTransform(165.3, 131.8);

        this.shape_176 = new cjs.Shape();
        this.shape_176.graphics.rf(["#3D3DFF", "#000000"], [0, 1], 1.4, 2.1, 0, 1.4, 2.1, 55.1).s().p("AkqGOQgriYgei0QgeivAAi8QCSgpCxgdQC0gdDCAAIADAAQAqCXAfCzQAeCwAAC9QiSApiwAdQi1AdjCAAIgDAAg");
        this.shape_176.setTransform(160.9, 128.9);

        this.shape_177 = new cjs.Shape();
        this.shape_177.graphics.rf(["#3B3BFF", "#000000"], [0, 1], 1.4, 2.1, 0, 1.4, 2.1, 55.2).s().p("AktGNQgqiZgdi0QgcixAAi8QCUgnCxgdQC1gbDCAAIADAAQApCZAdC0QAcCxAAC8IAAAAQiTAnixAdQi1AbjDAAIgCAAg");
        this.shape_177.setTransform(156.5, 126);

        this.shape_178 = new cjs.Shape();
        this.shape_178.graphics.rf(["#3838FF", "#000000"], [0, 1], 1.4, 2.2, 0, 1.4, 2.2, 55.4).s().p("AkyGNQgnibgbi2QgciwAAi9QCWgmCygbQC1gaDDAAIADAAQAmCbAcC2QAcCwAAC9IAAAAQiWAmixAaQi2AbjDAAIgDAAg");
        this.shape_178.setTransform(152.1, 123.1);

        this.shape_179 = new cjs.Shape();
        this.shape_179.graphics.rf(["#3636FF", "#000000"], [0, 1], 1.4, 2.2, 0, 1.4, 2.2, 55.5).s().p("Ak2GMQglidgai2QgbixAAi9QCYgkCzgZQC2gaDDAAIACAAQAmCdAaC3QAbCxAAC9IAAABQiYAjiyAZQi3AZjDAAIgDAAg");
        this.shape_179.setTransform(147.7, 120.2);

        this.shape_180 = new cjs.Shape();
        this.shape_180.graphics.rf(["#3333FF", "#000000"], [0, 1], 1.4, 2.2, 0, 1.4, 2.2, 55.7).s().p("Ak5GNQgkiggZi2QgZiygBi+QCagiC0gZQC3gXDCAAIAEAAQAjCfAZC2QAaCzgBC9QiZAiizAYQi3AZjEAAIgCAAg");
        this.shape_180.setTransform(143.3, 117.3);

        this.shape_181 = new cjs.Shape();
        this.shape_181.graphics.rf(["#3030FF", "#000000"], [0, 1], 1.4, 2.3, 0, 1.4, 2.3, 55.9).s().p("Ak+GNQghihgYi5QgYiyAAi+QCcghC0gXQC3gXDEABIACAAQAiCgAYC4QAYC0AAC9QicAgizAXQi5AYjCAAIgEAAg");
        this.shape_181.setTransform(138.9, 114.4);

        this.shape_182 = new cjs.Shape();
        this.shape_182.graphics.rf(["#2E2EFF", "#000000"], [0, 1], 1.5, 2.3, 0, 1.5, 2.3, 56).s().p("AlBGMQghijgWi4QgWi0AAi+QCdgfC0gVQC6gWDCAAIADAAQAgCjAXC4QAWC0AAC+QidAei0AWQi5AWjEAAIgCAAg");
        this.shape_182.setTransform(134.5, 111.5);

        this.shape_183 = new cjs.Shape();
        this.shape_183.graphics.rf(["#2B2BFF", "#000000"], [0, 1], 1.5, 2.3, 0, 1.5, 2.3, 56.2).s().p("AlFGMQgfilgVi5QgVi0AAi/QCggdC1gVQC5gUDDAAIADAAQAeClAVC5QAXC1gBC+QifAdi1AVQi5AUjFAAIgCAAg");
        this.shape_183.setTransform(130.1, 108.6);

        this.shape_184 = new cjs.Shape();
        this.shape_184.graphics.rf(["#2929FF", "#000000"], [0, 1], 1.5, 2.3, 0, 1.5, 2.3, 56.3).s().p("AlJGMQgcingVi6QgUi1AAi/QCigbC2gUQC5gSDEAAIADAAQAdCmATC6QAVC1AAC/IAAAAQiiAbi1AUQi6ASjEABIgDAAg");
        this.shape_184.setTransform(125.7, 105.7);

        this.shape_185 = new cjs.Shape();
        this.shape_185.graphics.rf(["#2626FF", "#000000"], [0, 1], 1.5, 2.4, 0, 1.5, 2.4, 56.5).s().p("AlNGMQgaiqgUi6QgSi2gBi+QCkgbC2gRQC7gTDEABIADAAQAbCoASC7QAUC2AAC/IAAAAQikAZi2ATQi7ASjEAAIgDAAg");
        this.shape_185.setTransform(121.3, 102.8);

        this.shape_186 = new cjs.Shape();
        this.shape_186.graphics.rf(["#2424FF", "#000000"], [0, 1], 1.5, 2.4, 0, 1.5, 2.4, 56.6).s().p("AlRGLQgZiqgSi8QgRi3AAi+QClgYC4gSQC7gQDEAAIADAAQAYCqATC8QARC3AAC+QilAYi3ASQi8AQjEAAIgDAAg");
        this.shape_186.setTransform(116.9, 99.9);

        this.shape_187 = new cjs.Shape();
        this.shape_187.graphics.rf(["#2121FF", "#000000"], [0, 1], 1.5, 2.4, 0, 1.5, 2.4, 56.8).s().p("AlUGLQgYisgQi9QgRi4AAi/QCogVC3gRQC9gPDEAAIADAAQAXCtAQC8QARC4AAC+QioAXi3APQi8AQjFAAIgCAAg");
        this.shape_187.setTransform(112.5, 97);

        this.shape_188 = new cjs.Shape();
        this.shape_188.graphics.rf(["#1F1FFF", "#000000"], [0, 1], 1.6, 2.4, 0, 1.6, 2.4, 56.9).s().p("AlZGLQgVivgPi9QgPi4gBi/QCqgVC5gOQC8gPDFAAIACAAQAWCvAPC9QAQC4AAC/QipAVi5AOQi9APjFAAIgDAAg");
        this.shape_188.setTransform(108.1, 94.1);

        this.shape_189 = new cjs.Shape();
        this.shape_189.graphics.rf(["#1C1CFF", "#000000"], [0, 1], 1.6, 2.5, 0, 1.6, 2.5, 57.1).s().p("AlcGLQgUixgOi+QgOi5AAjAQCsgSC4gOQC+gNDEAAIAEAAQATCxAOC+QAOC5AADAQirASi5ANQi+AOjFAAIgCAAg");
        this.shape_189.setTransform(103.7, 91.2);

        this.shape_190 = new cjs.Shape();
        this.shape_190.graphics.rf(["#1A1AFF", "#000000"], [0, 1], 1.6, 2.5, 0, 1.6, 2.5, 57.2).s().p("AlgGKQgTiygMi/QgMi6AAi/QCtgRC6gNQC+gMDFABIACAAQASCyANC/QANC6AADAQiuAQi5AMQi/AMjEAAIgDAAg");
        this.shape_190.setTransform(99.3, 88.3);

        this.shape_191 = new cjs.Shape();
        this.shape_191.graphics.rf(["#1717FF", "#000000"], [0, 1], 1.6, 2.5, 0, 1.6, 2.5, 57.4).s().p("AllGKQgPi0gMjAQgLi7AAi/QCvgPC6gLQDAgLDEAAIAEAAQAQC0ALDAQALC6AADAIAAAAQiuAPi7ALQjAAMjEgBIgEAAg");
        this.shape_191.setTransform(94.9, 85.4);

        this.shape_192 = new cjs.Shape();
        this.shape_192.graphics.rf(["#1414FF", "#000000"], [0, 1], 1.6, 2.6, 0, 1.6, 2.6, 57.5).s().p("AloGKQgOi2gLjBQgKi7AAjAQCxgOC8gKQC/gJDGAAIACAAQAOC2ALDBQAKC7AADAQixAOi6AKQjBAJjGAAIgCAAg");
        this.shape_192.setTransform(90.5, 82.5);

        this.shape_193 = new cjs.Shape();
        this.shape_193.graphics.rf(["#1212FF", "#000000"], [0, 1], 1.7, 2.6, 0, 1.7, 2.6, 57.7).s().p("AlsGKQgMi5gKjBQgIi8gBjAQC0gMC8gJQDAgIDFAAIADAAQAMC5AJDBQAKC8gBDAQiyAMi8AJQjAAIjGAAIgDAAg");
        this.shape_193.setTransform(86.1, 79.6);

        this.shape_194 = new cjs.Shape();
        this.shape_194.graphics.rf(["#0F0FFF", "#000000"], [0, 1], 1.7, 2.6, 0, 1.7, 2.6, 57.8).s().p("AlwGJQgLi6gIjCQgHi9AAjAQC1gKC8gHQDCgIDFAAIADAAQALC7AHDBQAIC9AADBQi1AKi8AIQjBAGjGAAIgDAAg");
        this.shape_194.setTransform(81.7, 76.7);

        this.shape_195 = new cjs.Shape();
        this.shape_195.graphics.rf(["#0D0DFF", "#000000"], [0, 1], 1.7, 2.6, 0, 1.7, 2.6, 58).s().p("Al0GJQgJi8gGjCQgHi+ABjBQC2gIC+gHQDBgFDGAAIADAAQAJC8AGDCQAGC+AADBQi2AIi8AHQjDAFjGAAIgDAAg");
        this.shape_195.setTransform(77.3, 73.8);

        this.shape_196 = new cjs.Shape();
        this.shape_196.graphics.rf(["#0A0AFF", "#000000"], [0, 1], 1.7, 2.7, 0, 1.7, 2.7, 58.1).s().p("Al4GJQgHi+gFjEQgFi+AAjBQC5gHC9gFQDDgEDGAAIADAAQAHC/AFDCQAFC/AADBQi5AGi8AFQjEAGjGgBIgDAAg");
        this.shape_196.setTransform(72.9, 70.9);

        this.shape_197 = new cjs.Shape();
        this.shape_197.graphics.rf(["#0808FF", "#000000"], [0, 1], 1.7, 2.7, 0, 1.7, 2.7, 58.3).s().p("Al8GJIgJmEQgDjAAAjAQC6gGC/gDQDDgEDGAAIADAAIAJGFQAEC/gBDAIAAABQi5AFi+ADQjEAEjHAAIgDAAg");
        this.shape_197.setTransform(68.5, 68);

        this.shape_198 = new cjs.Shape();
        this.shape_198.graphics.rf(["#0505FF", "#000000"], [0, 1], 1.7, 2.7, 0, 1.7, 2.7, 58.4).s().p("AmAGJIgGmIQgCi/AAjCIF7gGQDEgCDGAAIADAAQAEDCADDGQACC/AADCIl7AFQjEADjHAAIgDAAg");
        this.shape_198.setTransform(64.1, 65.1);

        this.shape_199 = new cjs.Shape();
        this.shape_199.graphics.rf(["#0303FF", "#000000"], [0, 1], 1.8, 2.7, 0, 1.8, 2.7, 58.6).s().p("AmDGIIgEmJQgBjBAAjCIF/gDIGLgBIACAAIADGKQACDBAADCIl+ACImMABIgCAAg");
        this.shape_199.setTransform(59.7, 62.2);

        this.shape_200 = new cjs.Shape();
        this.shape_200.graphics.rf(["#0000FF", "#000000"], [0, 1], 1.8, 2.8, 0, 1.8, 2.8, 58.7).s().p("AmIGIIAAsPIMRAAIAAMPg");
        this.shape_200.setTransform(55.3, 59.3);

        this.timeline.addTween(cjs.Tween.get({}).to({ state: [{ t: this.shape }] }).to({ state: [{ t: this.shape_1 }] }, 1).to({ state: [{ t: this.shape_2 }] }, 1).to({ state: [{ t: this.shape_3 }] }, 1).to({ state: [{ t: this.shape_4 }] }, 1).to({ state: [{ t: this.shape_5 }] }, 1).to({ state: [{ t: this.shape_6 }] }, 1).to({ state: [{ t: this.shape_7 }] }, 1).to({ state: [{ t: this.shape_8 }] }, 1).to({ state: [{ t: this.shape_9 }] }, 1).to({ state: [{ t: this.shape_10 }] }, 1).to({ state: [{ t: this.shape_11 }] }, 1).to({ state: [{ t: this.shape_12 }] }, 1).to({ state: [{ t: this.shape_13 }] }, 1).to({ state: [{ t: this.shape_14 }] }, 1).to({ state: [{ t: this.shape_15 }] }, 1).to({ state: [{ t: this.shape_16 }] }, 1).to({ state: [{ t: this.shape_17 }] }, 1).to({ state: [{ t: this.shape_18 }] }, 1).to({ state: [{ t: this.shape_19 }] }, 1).to({ state: [{ t: this.shape_20 }] }, 1).to({ state: [{ t: this.shape_21 }] }, 1).to({ state: [{ t: this.shape_22 }] }, 1).to({ state: [{ t: this.shape_23 }] }, 1).to({ state: [{ t: this.shape_24 }] }, 1).to({ state: [{ t: this.shape_25 }] }, 1).to({ state: [{ t: this.shape_26 }] }, 1).to({ state: [{ t: this.shape_27 }] }, 1).to({ state: [{ t: this.shape_28 }] }, 1).to({ state: [{ t: this.shape_29 }] }, 1).to({ state: [{ t: this.shape_30 }] }, 1).to({ state: [{ t: this.shape_31 }] }, 1).to({ state: [{ t: this.shape_32 }] }, 1).to({ state: [{ t: this.shape_33 }] }, 1).to({ state: [{ t: this.shape_34 }] }, 1).to({ state: [{ t: this.shape_35 }] }, 1).to({ state: [{ t: this.shape_36 }] }, 1).to({ state: [{ t: this.shape_37 }] }, 1).to({ state: [{ t: this.shape_38 }] }, 1).to({ state: [{ t: this.shape_39 }] }, 1).to({ state: [{ t: this.shape_40 }] }, 1).to({ state: [{ t: this.shape_41 }] }, 1).to({ state: [{ t: this.shape_42 }] }, 1).to({ state: [{ t: this.shape_43 }] }, 1).to({ state: [{ t: this.shape_44 }] }, 1).to({ state: [{ t: this.shape_45 }] }, 1).to({ state: [{ t: this.shape_46 }] }, 1).to({ state: [{ t: this.shape_47 }] }, 1).to({ state: [{ t: this.shape_48 }] }, 1).to({ state: [{ t: this.shape_49 }] }, 1).to({ state: [{ t: this.shape_50 }] }, 1).to({ state: [{ t: this.shape_51 }] }, 1).to({ state: [{ t: this.shape_52 }] }, 1).to({ state: [{ t: this.shape_53 }] }, 1).to({ state: [{ t: this.shape_54 }] }, 1).to({ state: [{ t: this.shape_55 }] }, 1).to({ state: [{ t: this.shape_56 }] }, 1).to({ state: [{ t: this.shape_57 }] }, 1).to({ state: [{ t: this.shape_58 }] }, 1).to({ state: [{ t: this.shape_59 }] }, 1).to({ state: [{ t: this.shape_60 }] }, 1).to({ state: [{ t: this.shape_61 }] }, 1).to({ state: [{ t: this.shape_62 }] }, 1).to({ state: [{ t: this.shape_63 }] }, 1).to({ state: [{ t: this.shape_64 }] }, 1).to({ state: [{ t: this.shape_65 }] }, 1).to({ state: [{ t: this.shape_66 }] }, 1).to({ state: [{ t: this.shape_67 }] }, 1).to({ state: [{ t: this.shape_68 }] }, 1).to({ state: [{ t: this.shape_69 }] }, 1).to({ state: [{ t: this.shape_70 }] }, 1).to({ state: [{ t: this.shape_71 }] }, 1).to({ state: [{ t: this.shape_72 }] }, 1).to({ state: [{ t: this.shape_73 }] }, 1).to({ state: [{ t: this.shape_74 }] }, 1).to({ state: [{ t: this.shape_75 }] }, 1).to({ state: [{ t: this.shape_76 }] }, 1).to({ state: [{ t: this.shape_77 }] }, 1).to({ state: [{ t: this.shape_78 }] }, 1).to({ state: [{ t: this.shape_79 }] }, 1).to({ state: [{ t: this.shape_80 }] }, 1).to({ state: [{ t: this.shape_81 }] }, 1).to({ state: [{ t: this.shape_82 }] }, 1).to({ state: [{ t: this.shape_83 }] }, 1).to({ state: [{ t: this.shape_84 }] }, 1).to({ state: [{ t: this.shape_85 }] }, 1).to({ state: [{ t: this.shape_86 }] }, 1).to({ state: [{ t: this.shape_87 }] }, 1).to({ state: [{ t: this.shape_88 }] }, 1).to({ state: [{ t: this.shape_89 }] }, 1).to({ state: [{ t: this.shape_90 }] }, 1).to({ state: [{ t: this.shape_91 }] }, 1).to({ state: [{ t: this.shape_92 }] }, 1).to({ state: [{ t: this.shape_93 }] }, 1).to({ state: [{ t: this.shape_94 }] }, 1).to({ state: [{ t: this.shape_95 }] }, 1).to({ state: [{ t: this.shape_96 }] }, 1).to({ state: [{ t: this.shape_97 }] }, 1).to({ state: [{ t: this.shape_98 }] }, 1).to({ state: [{ t: this.shape_99 }] }, 1).to({ state: [{ t: this.shape_100 }] }, 1).to({ state: [{ t: this.shape_101 }] }, 1).to({ state: [{ t: this.shape_102 }] }, 1).to({ state: [{ t: this.shape_103 }] }, 1).to({ state: [{ t: this.shape_104 }] }, 1).to({ state: [{ t: this.shape_105 }] }, 1).to({ state: [{ t: this.shape_106 }] }, 1).to({ state: [{ t: this.shape_107 }] }, 1).to({ state: [{ t: this.shape_108 }] }, 1).to({ state: [{ t: this.shape_109 }] }, 1).to({ state: [{ t: this.shape_110 }] }, 1).to({ state: [{ t: this.shape_111 }] }, 1).to({ state: [{ t: this.shape_112 }] }, 1).to({ state: [{ t: this.shape_113 }] }, 1).to({ state: [{ t: this.shape_114 }] }, 1).to({ state: [{ t: this.shape_115 }] }, 1).to({ state: [{ t: this.shape_116 }] }, 1).to({ state: [{ t: this.shape_117 }] }, 1).to({ state: [{ t: this.shape_118 }] }, 1).to({ state: [{ t: this.shape_119 }] }, 1).to({ state: [{ t: this.shape_120 }] }, 1).to({ state: [{ t: this.shape_121 }] }, 1).to({ state: [{ t: this.shape_122 }] }, 1).to({ state: [{ t: this.shape_123 }] }, 1).to({ state: [{ t: this.shape_124 }] }, 1).to({ state: [{ t: this.shape_125 }] }, 1).to({ state: [{ t: this.shape_126 }] }, 1).to({ state: [{ t: this.shape_127 }] }, 1).to({ state: [{ t: this.shape_128 }] }, 1).to({ state: [{ t: this.shape_129 }] }, 1).to({ state: [{ t: this.shape_130 }] }, 1).to({ state: [{ t: this.shape_131 }] }, 1).to({ state: [{ t: this.shape_132 }] }, 1).to({ state: [{ t: this.shape_133 }] }, 1).to({ state: [{ t: this.shape_134 }] }, 1).to({ state: [{ t: this.shape_135 }] }, 1).to({ state: [{ t: this.shape_136 }] }, 1).to({ state: [{ t: this.shape_137 }] }, 1).to({ state: [{ t: this.shape_138 }] }, 1).to({ state: [{ t: this.shape_139 }] }, 1).to({ state: [{ t: this.shape_140 }] }, 1).to({ state: [{ t: this.shape_141 }] }, 1).to({ state: [{ t: this.shape_142 }] }, 1).to({ state: [{ t: this.shape_143 }] }, 1).to({ state: [{ t: this.shape_144 }] }, 1).to({ state: [{ t: this.shape_145 }] }, 1).to({ state: [{ t: this.shape_146 }] }, 1).to({ state: [{ t: this.shape_147 }] }, 1).to({ state: [{ t: this.shape_148 }] }, 1).to({ state: [{ t: this.shape_149 }] }, 1).to({ state: [{ t: this.shape_150 }] }, 1).to({ state: [{ t: this.shape_151 }] }, 1).to({ state: [{ t: this.shape_152 }] }, 1).to({ state: [{ t: this.shape_153 }] }, 1).to({ state: [{ t: this.shape_154 }] }, 1).to({ state: [{ t: this.shape_155 }] }, 1).to({ state: [{ t: this.shape_156 }] }, 1).to({ state: [{ t: this.shape_157 }] }, 1).to({ state: [{ t: this.shape_158 }] }, 1).to({ state: [{ t: this.shape_159 }] }, 1).to({ state: [{ t: this.shape_160 }] }, 1).to({ state: [{ t: this.shape_161 }] }, 1).to({ state: [{ t: this.shape_162 }] }, 1).to({ state: [{ t: this.shape_163 }] }, 1).to({ state: [{ t: this.shape_164 }] }, 1).to({ state: [{ t: this.shape_165 }] }, 1).to({ state: [{ t: this.shape_166 }] }, 1).to({ state: [{ t: this.shape_167 }] }, 1).to({ state: [{ t: this.shape_168 }] }, 1).to({ state: [{ t: this.shape_169 }] }, 1).to({ state: [{ t: this.shape_170 }] }, 1).to({ state: [{ t: this.shape_171 }] }, 1).to({ state: [{ t: this.shape_172 }] }, 1).to({ state: [{ t: this.shape_173 }] }, 1).to({ state: [{ t: this.shape_174 }] }, 1).to({ state: [{ t: this.shape_175 }] }, 1).to({ state: [{ t: this.shape_176 }] }, 1).to({ state: [{ t: this.shape_177 }] }, 1).to({ state: [{ t: this.shape_178 }] }, 1).to({ state: [{ t: this.shape_179 }] }, 1).to({ state: [{ t: this.shape_180 }] }, 1).to({ state: [{ t: this.shape_181 }] }, 1).to({ state: [{ t: this.shape_182 }] }, 1).to({ state: [{ t: this.shape_183 }] }, 1).to({ state: [{ t: this.shape_184 }] }, 1).to({ state: [{ t: this.shape_185 }] }, 1).to({ state: [{ t: this.shape_186 }] }, 1).to({ state: [{ t: this.shape_187 }] }, 1).to({ state: [{ t: this.shape_188 }] }, 1).to({ state: [{ t: this.shape_189 }] }, 1).to({ state: [{ t: this.shape_190 }] }, 1).to({ state: [{ t: this.shape_191 }] }, 1).to({ state: [{ t: this.shape_192 }] }, 1).to({ state: [{ t: this.shape_193 }] }, 1).to({ state: [{ t: this.shape_194 }] }, 1).to({ state: [{ t: this.shape_195 }] }, 1).to({ state: [{ t: this.shape_196 }] }, 1).to({ state: [{ t: this.shape_197 }] }, 1).to({ state: [{ t: this.shape_198 }] }, 1).to({ state: [{ t: this.shape_199 }] }, 1).to({ state: [{ t: this.shape_200 }] }, 1).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(285.1, 213.1, 74, 74);
    // library properties:
    lib.properties = {
        width: 550,
        height: 400,
        fps: 24,
        color: "#FFFFFF",
        opacity: 1.00,
        manifest: [],
        preloads: []
    };




})(lib = lib || {}, images = images || {}, createjs = createjs || {}, ss = ss || {}, AdobeAn = AdobeAn || {});
var lib, images, createjs, ss, AdobeAn;